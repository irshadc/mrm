package com.glassboxsoftware.mrmapp.gcm;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.activity.SelectionActivity;
import com.glassboxsoftware.mrmapp.activity.StartActivity;
import com.glassboxsoftware.mrmapp.dao.internal.InternalDao;
import com.glassboxsoftware.mrmapp.dao.internal.NotifyMsg;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * This {@code WakefulBroadcastReceiver} takes care of creating and managing a
 * partial wake lock for your app. It passes off the work of processing the GCM
 * message to an {@code IntentService}, while ensuring that the device does not
 * go back to sleep in the transition. The {@code IntentService} calls
 * {@code GcmBroadcastReceiver.completeWakefulIntent()} when it is ready to
 * release the wake lock.
 */

public class GcmBroadcastReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        JSONObject pushData = null;

        try {
            pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
        } catch (JSONException var7) {
            Log.e("GcmBroadcastReceiver", "Unexpected JSONException when receiving push data: ", var7);
        }

        String msg = null, title = null;
        if(pushData != null) {
            msg = pushData.optString("alert",null);
            title = pushData.optString("title","MRM - Mumbai Rail Map");
            NotifyMsg nMsg = new NotifyMsg();
            nMsg.setActive(true);
            nMsg.setHeader(pushData.optString("header", "Mega Block"));
            nMsg.setTitle(title);
            nMsg.setAlert(msg);
            nMsg.setGeneratedOn(System.currentTimeMillis());
            InternalDao.getDao().upsertNotifyMsg(nMsg);
            if(pushData.has("data")) {
                JSONArray dataArr = pushData.optJSONArray("data");
                if(dataArr!=null && dataArr.length()>0) {
                    for(int i=0;i<dataArr.length();i++){
                        try {
                            JSONObject dataObj = dataArr.getJSONObject(i);

                            nMsg = new NotifyMsg();
                            nMsg.setActive(true);
                            nMsg.setHeader(dataObj.optString("header", "MRM Information"));
                            nMsg.setTitle(dataObj.optString("title", "Mega Block"));
                            nMsg.setAlert(dataObj.optString("alert", null));
                            nMsg.setGeneratedOn(System.currentTimeMillis());
                            if(nMsg.getAlert()!=null) {
                                InternalDao.getDao().upsertNotifyMsg(nMsg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }

        if(msg != null) {
            showNotification(context,title,msg);
        }
    }

    private void showNotification(Context context, String title, String msg){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.local_train)
                        .setContentTitle(title)
                        .setContentText(msg);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, SelectionActivity.class);
        resultIntent.putExtra("show_notification",true);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(SelectionActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(1001, mBuilder.build());
    }
}