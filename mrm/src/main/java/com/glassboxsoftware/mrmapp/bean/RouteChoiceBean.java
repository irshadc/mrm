package com.glassboxsoftware.mrmapp.bean;

/**
 * Created by Irshad Chohan on 6/9/2014.
 */
public class RouteChoiceBean {

    public String stnIdList,stnNameList;
    public String lineIdList;
    public int timeOfJourney;
    public int noOfStops;
}
