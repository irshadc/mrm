package com.glassboxsoftware.mrmapp.bean;

/**
 * Created by Irshad Chohan on 6/24/2014.
 */
public class StationTimeBean {

    public int stnId;
    public String stationName;
    public int timeInMinute;
    public String platformNo;
    public String platformSide;

}
