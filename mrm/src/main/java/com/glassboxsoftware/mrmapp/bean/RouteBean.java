package com.glassboxsoftware.mrmapp.bean;

import com.glassboxsoftware.mrmapp.dao.Station;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Irshad Chohan on 6/9/2014.
 */
public class RouteBean {

    public Station srcStn = new Station();
    public Station dstStn = new Station();
    public List<RouteChoiceBean> rcbList = new ArrayList<RouteChoiceBean>();
    public float moneyF;
    public boolean isFavourite;
    public Calendar dateTime;
}
