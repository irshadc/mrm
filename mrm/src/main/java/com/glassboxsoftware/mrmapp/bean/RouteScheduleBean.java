package com.glassboxsoftware.mrmapp.bean;

import android.util.SparseArray;

import com.glassboxsoftware.mrmapp.dao.Line;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irshad Chohan on 6/9/2014.
 */
public class RouteScheduleBean {

    public TrainInfoBean trainInfoBean = new TrainInfoBean();
    public SparseArray<StationTimeBean> stbMap = new SparseArray<StationTimeBean>();
    public List<Integer> stnIdList = new ArrayList<Integer>();
    public Line line = new Line();
    public int timeToBoard, journeyTime;
    public int noOfStation,noOfStop;
}
