package com.glassboxsoftware.mrmapp.bean;

/**
 * Created by Irshad Chohan on 6/9/2014.
 */
public class StationHaltInfoBean {

    public int stnId;
    public String stnName;
    public int timeInMinute;
    public String platformNo, platformSide;
}
