package com.glassboxsoftware.mrmapp.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ChohI0155 on 6/15/2015.
 */
public class AdvSearchConfig {
    public String upStnName="", upStnCode="", downStnName="", downStnCode="";
    public String upStnIds="", downStnIds="", upFromStnIds="", downFromStnIds="", speedOpt="", platformOpt="", directionOpt="", carOpt="";

    public AdvSearchConfig(){

    }
}
