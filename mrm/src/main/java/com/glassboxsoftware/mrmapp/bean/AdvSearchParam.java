package com.glassboxsoftware.mrmapp.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ChohI0155 on 6/15/2015.
 */
public class AdvSearchParam implements Parcelable{
    public int stnId, lineId,timeNow;
    public int currentIndex;
    public String towardsFilter, fromFilter;
    public char direction, speed;

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(stnId);
        out.writeInt(lineId);
        out.writeInt(timeNow);
        out.writeInt(currentIndex);
        out.writeString(towardsFilter);
        out.writeString(fromFilter);
        out.writeInt((int) direction);
        out.writeInt((int)speed);
    }

    public static final Parcelable.Creator<AdvSearchParam> CREATOR
            = new Parcelable.Creator<AdvSearchParam>() {
        public AdvSearchParam createFromParcel(Parcel in) {
            return new AdvSearchParam(in);
        }

        public AdvSearchParam[] newArray(int size) {
            return new AdvSearchParam[size];
        }
    };

    public AdvSearchParam(){

    }

    private AdvSearchParam(Parcel in) {
        stnId = in.readInt();
        lineId = in.readInt();
        timeNow = in.readInt();
        currentIndex = in.readInt();
        towardsFilter = in.readString();
        fromFilter = in.readString();
        direction = (char)in.readInt();
        speed = (char)in.readInt();
    }
}
