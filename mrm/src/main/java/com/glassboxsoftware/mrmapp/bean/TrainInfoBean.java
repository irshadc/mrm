package com.glassboxsoftware.mrmapp.bean;

/**
 * Created by Irshad Chohan on 6/9/2014.
 */
public class TrainInfoBean {

    public int trainId;
    //public String trainCode;
    //public String trainNumber;
    //public String emuCode;
    public int noOfCar;
    //public String direction;
    public String speed;
    public String specialInfo;
    public boolean sundayOnly;
    public boolean holidayOnly;
    public boolean notOnSunday;
    public String firstStnName, lastStnName, lastStnCode;
    public int startStnId, endStnId, startStnTime, endStnTime;
    public String platformNo, platformSide;
}
