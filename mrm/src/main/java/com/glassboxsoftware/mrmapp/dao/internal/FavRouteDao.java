package com.glassboxsoftware.mrmapp.dao.internal;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.glassboxsoftware.mrmapp.dao.internal.FavRoute;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table FAV_ROUTE.
*/
public class FavRouteDao extends AbstractDao<FavRoute, Long> {

    public static final String TABLENAME = "FAV_ROUTE";

    /**
     * Properties of entity FavRoute.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property SrcId = new Property(1, int.class, "srcId", false, "SRC_ID");
        public final static Property DstId = new Property(2, int.class, "dstId", false, "DST_ID");
        public final static Property RouteId = new Property(3, int.class, "routeId", false, "ROUTE_ID");
        public final static Property JxIdLst = new Property(4, String.class, "jxIdLst", false, "JX_ID_LST");
        public final static Property RouteStr = new Property(5, String.class, "routeStr", false, "ROUTE_STR");
        public final static Property Favourite = new Property(6, boolean.class, "favourite", false, "FAVOURITE");
        public final static Property LastModified = new Property(7, Long.class, "lastModified", false, "LAST_MODIFIED");
        public final static Property SyncedOn = new Property(8, Long.class, "syncedOn", false, "SYNCED_ON");
        public final static Property UsageCount = new Property(9, Integer.class, "usageCount", false, "USAGE_COUNT");
        public final static Property Active = new Property(10, boolean.class, "active", false, "ACTIVE");
    };


    public FavRouteDao(DaoConfig config) {
        super(config);
    }
    
    public FavRouteDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'FAV_ROUTE' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'SRC_ID' INTEGER NOT NULL ," + // 1: srcId
                "'DST_ID' INTEGER NOT NULL ," + // 2: dstId
                "'ROUTE_ID' INTEGER NOT NULL ," + // 3: routeId
                "'JX_ID_LST' TEXT(120)," + // 4: jxIdLst
                "'ROUTE_STR' TEXT(120)," + // 5: routeStr
                "'FAVOURITE' INTEGER(1) NOT NULL ," + // 6: favourite
                "'LAST_MODIFIED' INTEGER," + // 7: lastModified
                "'SYNCED_ON' INTEGER," + // 8: syncedOn
                "'USAGE_COUNT' INTEGER," + // 9: usageCount
                "'ACTIVE' INTEGER(1) NOT NULL );"); // 10: active
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'FAV_ROUTE'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, FavRoute entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getSrcId());
        stmt.bindLong(3, entity.getDstId());
        stmt.bindLong(4, entity.getRouteId());
 
        String jxIdLst = entity.getJxIdLst();
        if (jxIdLst != null) {
            stmt.bindString(5, jxIdLst);
        }
 
        String routeStr = entity.getRouteStr();
        if (routeStr != null) {
            stmt.bindString(6, routeStr);
        }
        stmt.bindLong(7, entity.getFavourite() ? 1l: 0l);
 
        Long lastModified = entity.getLastModified();
        if (lastModified != null) {
            stmt.bindLong(8, lastModified);
        }
 
        Long syncedOn = entity.getSyncedOn();
        if (syncedOn != null) {
            stmt.bindLong(9, syncedOn);
        }
 
        Integer usageCount = entity.getUsageCount();
        if (usageCount != null) {
            stmt.bindLong(10, usageCount);
        }
        stmt.bindLong(11, entity.getActive() ? 1l: 0l);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public FavRoute readEntity(Cursor cursor, int offset) {
        FavRoute entity = new FavRoute( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getInt(offset + 1), // srcId
            cursor.getInt(offset + 2), // dstId
            cursor.getInt(offset + 3), // routeId
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // jxIdLst
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // routeStr
            cursor.getShort(offset + 6) != 0, // favourite
            cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7), // lastModified
            cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8), // syncedOn
            cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9), // usageCount
            cursor.getShort(offset + 10) != 0 // active
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, FavRoute entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setSrcId(cursor.getInt(offset + 1));
        entity.setDstId(cursor.getInt(offset + 2));
        entity.setRouteId(cursor.getInt(offset + 3));
        entity.setJxIdLst(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setRouteStr(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setFavourite(cursor.getShort(offset + 6) != 0);
        entity.setLastModified(cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7));
        entity.setSyncedOn(cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8));
        entity.setUsageCount(cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9));
        entity.setActive(cursor.getShort(offset + 10) != 0);
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(FavRoute entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(FavRoute entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
