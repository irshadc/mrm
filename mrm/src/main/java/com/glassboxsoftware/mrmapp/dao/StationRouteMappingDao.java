package com.glassboxsoftware.mrmapp.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table STATION_ROUTE_MAPPING.
*/
public class StationRouteMappingDao extends AbstractDao<StationRouteMapping, Long> {

    public static final String TABLENAME = "STATION_ROUTE_MAPPING";

    /**
     * Properties of entity StationRouteMapping.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property SourceStationId = new Property(1, int.class, "sourceStationId", false, "SOURCE_STATION_ID");
        public final static Property DestStationId = new Property(2, int.class, "destStationId", false, "DEST_STATION_ID");
        public final static Property Route = new Property(3, String.class, "route", false, "ROUTE");
        public final static Property Priority = new Property(4, Short.class, "priority", false, "PRIORITY");
        public final static Property Lines = new Property(5, String.class, "lines", false, "LINES");
        public final static Property Time = new Property(6, String.class, "time", false, "TIME");
        public final static Property Active = new Property(7, Boolean.class, "active", false, "ACTIVE");
    };


    public StationRouteMappingDao(DaoConfig config) {
        super(config);
    }
    
    public StationRouteMappingDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, StationRouteMapping entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getSourceStationId());
        stmt.bindLong(3, entity.getDestStationId());
 
        String route = entity.getRoute();
        if (route != null) {
            stmt.bindString(4, route);
        }
 
        Short priority = entity.getPriority();
        if (priority != null) {
            stmt.bindLong(5, priority);
        }
 
        String lines = entity.getLines();
        if (lines != null) {
            stmt.bindString(6, lines);
        }
 
        String time = entity.getTime();
        if (time != null) {
            stmt.bindString(7, time);
        }
 
        Boolean active = entity.getActive();
        if (active != null) {
            stmt.bindLong(8, active ? 1l: 0l);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public StationRouteMapping readEntity(Cursor cursor, int offset) {
        StationRouteMapping entity = new StationRouteMapping( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getInt(offset + 1), // sourceStationId
            cursor.getInt(offset + 2), // destStationId
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // route
            cursor.isNull(offset + 4) ? null : cursor.getShort(offset + 4), // priority
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // lines
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // time
            cursor.isNull(offset + 7) ? null : cursor.getShort(offset + 7) != 0 // active
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, StationRouteMapping entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setSourceStationId(cursor.getInt(offset + 1));
        entity.setDestStationId(cursor.getInt(offset + 2));
        entity.setRoute(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setPriority(cursor.isNull(offset + 4) ? null : cursor.getShort(offset + 4));
        entity.setLines(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setTime(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setActive(cursor.isNull(offset + 7) ? null : cursor.getShort(offset + 7) != 0);
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(StationRouteMapping entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(StationRouteMapping entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
