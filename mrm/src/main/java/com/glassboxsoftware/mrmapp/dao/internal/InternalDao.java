package com.glassboxsoftware.mrmapp.dao.internal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.glassboxsoftware.mrmapp.MRMApp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irshad Chohan on 12/31/2014.
 */
public class InternalDao {

    private SQLiteDatabase db;
    private DaoMaster daoMaster;
    private DaoSession daoSession;

    private static InternalDao internalDao;

    static {
        Log.d("InternalDao ","Counter 1");
        internalDao = new InternalDao(MRMApp.me().getApplicationContext());
    }


    public static InternalDao getDao() {
        if (internalDao == null) {
            Log.d("InternalDao ","Counter 1");
            internalDao = new InternalDao(MRMApp.me().getApplicationContext());
        }
        return internalDao;
    }

    private InternalDao(Context mContext) {

        DaoMaster.OpenHelper helper = new DaoMaster.OpenHelper(mContext, "mrm-internal", null);
        if(db!=null) {
            db.close();
        }
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();

    }

    public synchronized void close() {
        if(db != null){
            db.close();
        }
    }

    public List<FavRoute> getFavList() {
        List<FavRoute> mList = new ArrayList<>();
        boolean isFav = true, isRecent = true;
        if (MRMApp.loadingDone) {
            for (FavRoute fav : daoSession.getFavRouteDao().queryRaw(" WHERE T.FAVOURITE = 1 AND ACTIVE = 1 ORDER BY T.LAST_MODIFIED DESC")) {
                if (isFav) {
                    mList.add(new FavRoute(null, 0, 0, 0, null, "FAVOURITE JOURNEYS", false, 0L, null, 0, false));
                    isFav = false;
                }
                fav.fromStnName = MRMApp.me().getStation(fav.getSrcId()).getName();
                fav.toStnName = MRMApp.me().getStation(fav.getDstId()).getName();
                mList.add(fav);
            }

            for (FavRoute fav : daoSession.getFavRouteDao().queryRaw(" WHERE T.FAVOURITE = 0 AND ACTIVE = 1 ORDER BY T.LAST_MODIFIED DESC LIMIT 5")) {
                if (isRecent) {
                    mList.add(new FavRoute(null, 0, 0, 0, null, "RECENT JOURNEYS", false, 0L, null, 0, false));
                    isRecent = false;
                }
                fav.fromStnName = MRMApp.me().getStation(fav.getSrcId()).getName();
                fav.toStnName = MRMApp.me().getStation(fav.getDstId()).getName();
                mList.add(fav);
            }
        }
        if (mList.size() == 0) {
            mList.add(new FavRoute(null, 0, 0, 0, null, "YOUR FAVOURITE AND RECENT JOURNEY WILL APPEAR HERE", false, 0L, null, 0, false));
        }
        return mList;
    }

    public List<FavRoute> getSyncList() {
        List<FavRoute> mList = new ArrayList<>();
        boolean isFav = true, isRecent = true;
        if (MRMApp.loadingDone) {
            for (FavRoute fav : daoSession.getFavRouteDao().queryRaw(" WHERE SYNCED_ON IS NULL ORDER BY T.LAST_MODIFIED DESC LIMIT 10")) {
                 mList.add(fav);
            }
        }
        return mList;
    }

    public FavRoute getFavRoute(int srcId, int dstId, int routeId) {
        for (FavRoute fav : daoSession.getFavRouteDao().queryRaw(" WHERE T.SRC_ID = " + srcId + " AND T.DST_ID = " + dstId + " AND T.ROUTE_ID = " + routeId + " AND T.ACTIVE = 1 ORDER BY T.LAST_MODIFIED DESC LIMIT 1")) {
            fav.fromStnName = MRMApp.me().getStation(fav.getSrcId()).getName();
            fav.toStnName = MRMApp.me().getStation(fav.getDstId()).getName();
            return fav;
        }
        return null;
    }

    public void upsertFavRoute(FavRoute fav) {
        daoSession.getFavRouteDao().insertOrReplace(fav);
    }

    public void removeFavRoute(FavRoute fav) {
        if(fav.getSyncedOn()!=null) {
            fav.setActive(false);
            daoSession.getFavRouteDao().update(fav);
        } else {
            daoSession.getFavRouteDao().deleteByKey(fav.getId());
        }
    }

    public void setSync(FavRoute fav) {
        if(fav.getActive()) {
            fav.setSyncedOn(System.currentTimeMillis());
            daoSession.getFavRouteDao().update(fav);
        } else {
            daoSession.getFavRouteDao().deleteByKey(fav.getId());
        }
    }

    public void upsertNotifyMsg(NotifyMsg msg) {
        daoSession.getNotifyMsgDao().insertOrReplace(msg);
    }

    public void deactiveOldMsg(NotifyMsg msg) {
        daoSession.getNotifyMsgDao().insertOrReplace(msg);
    }

    public List<NotifyMsg> getNotificationList() {
        return daoSession.getNotifyMsgDao().queryRaw(" WHERE T.ACTIVE=1 ORDER BY T.GENERATED_ON DESC ");
    }

    public void clearNotifications() {
        daoSession.getNotifyMsgDao().deleteAll();
    }
}
