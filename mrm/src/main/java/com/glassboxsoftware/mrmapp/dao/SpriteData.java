package com.glassboxsoftware.mrmapp.dao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table SPRITE_DATA.
 */
public class SpriteData {

    private Long id;
    private Integer stationId;
    private Integer lineId;
    private String spriteType;
    private Integer spriteX;
    private Integer spriteY;
    private Integer mapX;
    private Integer mapY;
    private Integer mapW;
    private Integer mapH;
    private Integer mActive;
    private Integer mIdle;

    public SpriteData() {
    }

    public SpriteData(Long id) {
        this.id = id;
    }

    public SpriteData(Long id, Integer stationId, Integer lineId, String spriteType, Integer spriteX, Integer spriteY, Integer mapX, Integer mapY, Integer mapW, Integer mapH, Integer mActive, Integer mIdle) {
        this.id = id;
        this.stationId = stationId;
        this.lineId = lineId;
        this.spriteType = spriteType;
        this.spriteX = spriteX;
        this.spriteY = spriteY;
        this.mapX = mapX;
        this.mapY = mapY;
        this.mapW = mapW;
        this.mapH = mapH;
        this.mActive = mActive;
        this.mIdle = mIdle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public Integer getLineId() {
        return lineId;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public String getSpriteType() {
        return spriteType;
    }

    public void setSpriteType(String spriteType) {
        this.spriteType = spriteType;
    }

    public Integer getSpriteX() {
        return spriteX;
    }

    public void setSpriteX(Integer spriteX) {
        this.spriteX = spriteX;
    }

    public Integer getSpriteY() {
        return spriteY;
    }

    public void setSpriteY(Integer spriteY) {
        this.spriteY = spriteY;
    }

    public Integer getMapX() {
        return mapX;
    }

    public void setMapX(Integer mapX) {
        this.mapX = mapX;
    }

    public Integer getMapY() {
        return mapY;
    }

    public void setMapY(Integer mapY) {
        this.mapY = mapY;
    }

    public Integer getMapW() {
        return mapW;
    }

    public void setMapW(Integer mapW) {
        this.mapW = mapW;
    }

    public Integer getMapH() {
        return mapH;
    }

    public void setMapH(Integer mapH) {
        this.mapH = mapH;
    }

    public Integer getMActive() {
        return mActive;
    }

    public void setMActive(Integer mActive) {
        this.mActive = mActive;
    }

    public Integer getMIdle() {
        return mIdle;
    }

    public void setMIdle(Integer mIdle) {
        this.mIdle = mIdle;
    }

}
