package com.glassboxsoftware.mrmapp.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.activity.RouteActivity;
import com.irscomp.bitmapview.LargeImageView;

import java.util.List;


public class RouteMapFragment extends DialogFragment {

    private LargeImageView mapIV;
    private RouteActivity act;

    public static RouteMapFragment newInstance(RouteActivity routeActivity) {
        RouteMapFragment fragment = new RouteMapFragment();
        fragment.act = routeActivity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_MRM_WithNavDrawer);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View v = inflater.inflate(R.layout.f_route_map, container, false);

        mapIV = (LargeImageView) v.findViewById(R.id.mapIV);
        if(act.mRsbList!=null)
            mapIV.redrawMap(act.mRsbList, act.srcId, act.dstId);

        ((TextView)v.findViewById(R.id.startStnTV)).setText(MRMApp.me().getStation(act.srcId).getName());
        ((TextView)v.findViewById(R.id.endStnTV)).setText(MRMApp.me().getStation(act.dstId).getName());

        v.findViewById(R.id.backIV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        v.findViewById(R.id.mapViewFAB).setVisibility(View.GONE);
        v.findViewById(R.id.routeReverseIV).setVisibility(View.GONE);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogLeftRightAnimation;
    }
}