package com.glassboxsoftware.mrmapp.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.activity.RouteActivity;
import com.glassboxsoftware.mrmapp.bean.RouteBean;
import com.glassboxsoftware.mrmapp.bean.RouteChoiceBean;
import com.glassboxsoftware.mrmapp.bean.RouteScheduleBean;
import com.glassboxsoftware.mrmapp.bean.StationTimeBean;
import com.glassboxsoftware.mrmapp.bean.TrainInfoBean;
import com.glassboxsoftware.mrmapp.dao.LineRouteDetails;
import com.glassboxsoftware.mrmapp.dao.Station;
import com.glassboxsoftware.mrmapp.dao.TrainFare;
import com.glassboxsoftware.mrmapp.dao.TrainFareDao;
import com.glassboxsoftware.mrmapp.dao.internal.FavRoute;
import com.glassboxsoftware.mrmapp.dao.internal.InternalDao;
import com.glassboxsoftware.mrmapp.loader.RouteChoiceLoader;
import com.glassboxsoftware.mrmapp.ui.adapter.OnTapListener;
import com.glassboxsoftware.mrmapp.ui.adapter.ResultViewAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RouteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RouteFragment extends Fragment
        implements TrainListFragment.OnTrainListFragmentListener,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener,
        LoaderManager.LoaderCallbacks {
    private static final String TAG = "RouteFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private RouteBean mRouteBean;

    private TextView srcStnTV, dstStnTV, dateTV, timeTV;
    private Spinner routeOptionSpinner, fareSpinner;
    private ImageView routeReverseIV;

    private RouteActivity act;

    protected ListView mResultListView;
    protected ResultViewAdapter mResultAdapter;

    private RouteSpinnerAdapter rcAdapter;
    private RouteSpinnerAdapter fareAdapter;

    private FindRouteDetailsTask frdTask;

    private FavRoute favEntry;
    private ImageView addFavRouteIV;
    private TextView addFavRouteTV;

    private static final int TIME_NEED_TO_BOARD = 2; //2 minute to change the train


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RouteFragment.
     */
    // TODO: Rename and change types of parameters
    public static RouteFragment newInstance() {
        RouteFragment fragment = new RouteFragment();
        return fragment;
    }

    public RouteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        act = (RouteActivity) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.f_route, container, false);

        mRouteBean = new RouteBean();
        mRouteBean.srcStn = MRMApp.me().getStation(act.srcId);
        mRouteBean.dstStn = MRMApp.me().getStation(act.dstId);

        //TODO drawToMap(mRouteDetailList);
        srcStnTV = (TextView) v.findViewById(R.id.startStnTV);
        dstStnTV = (TextView) v.findViewById(R.id.endStnTV);
        routeReverseIV = (ImageView) v.findViewById(R.id.routeReverseIV);


        mResultListView = (ListView) v.findViewById(R.id.resultLV);

        final View headerView = getActivity().getLayoutInflater().inflate(R.layout.v_route_list_header, container);
        final View footerView = getActivity().getLayoutInflater().inflate(R.layout.v_route_footer, container);

        footerView.findViewById(R.id.mrmAppURLTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent linkIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.app_url)));
                startActivity(linkIntent);
            }
        });

        mResultListView.addHeaderView(headerView);
        mResultListView.addFooterView(footerView);

        routeOptionSpinner = (Spinner) headerView.findViewById(R.id.routeOptionSpinner);

        fareSpinner = (Spinner) footerView.findViewById(R.id.fareSpinner);

        dateTV = (TextView) headerView.findViewById(R.id.dateTV);
        timeTV = (TextView) headerView.findViewById(R.id.timeTV);

        dateTV.setTypeface(MRMApp.robotoMedium);
        timeTV.setTypeface(MRMApp.robotoMedium);


        addFavRouteIV = (ImageView) footerView.findViewById(R.id.addFavRouteIV);
        addFavRouteTV = (TextView) footerView.findViewById(R.id.addFavRouteTV);

        final RelativeLayout favouriteRL = (RelativeLayout) footerView.findViewById(R.id.favouriteRL);


        favouriteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favEntry = InternalDao.getDao().getFavRoute(mRouteBean.srcStn.getId().intValue(), mRouteBean.dstStn.getId().intValue(), act.routeId);
                if (favEntry != null && favEntry.getFavourite()) {
                    InternalDao.getDao().removeFavRoute(favEntry);
                    addFavRouteIV.setImageResource(R.drawable.star_off);
                    addFavRouteTV.setText("Add to Favourite");
                    Toast.makeText(getActivity().getApplicationContext(), "Removed from favourite.", Toast.LENGTH_SHORT).show();
                    updateLastUsedTime(favEntry);
                } else {
                    if (favEntry != null) {
                        favEntry.setFavourite(true);
                    } else {
                        addToFavorite(mRouteBean.srcStn.getId().intValue(), mRouteBean.dstStn.getId().intValue(), act.routeId);
                    }
                    updateLastUsedTime(favEntry);
                    addFavRouteIV.setImageResource(R.drawable.star_on);
                    addFavRouteTV.setText("Remove from Favourite");
                    Toast.makeText(getActivity().getApplicationContext(), "Added to favourite.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final RelativeLayout shareRL = (RelativeLayout) footerView.findViewById(R.id.shareRL);

        shareRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CharSequence colors[] = new CharSequence[]{"SMS / Text", "Image"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Share as...");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Intent share = new Intent(Intent.ACTION_SEND);
                            if (which == 0) {
                                share.setType("text/plain");
                                String msg = "";
                                String msgTitle = "";
                                for(RouteScheduleBean rsb: act.mRsbList) {
                                    msgTitle+=MRMApp.me().getStation(rsb.trainInfoBean.startStnId).getName()+" > ";
                                    msg += ((rsb.trainInfoBean.startStnId == act.srcId) ? "From " : "Board at ") +
                                            MRMApp.me().getStation(rsb.trainInfoBean.startStnId).getName() +
                                            (rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformNo.length()>0 ? " on PF-" + rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformNo : "") + "\n" +
                                            MRMApp.getTimeInFormatPM(rsb.trainInfoBean.startStnTime) + " [" + rsb.trainInfoBean.lastStnCode + "] " +
                                            rsb.trainInfoBean.lastStnName + " " + (rsb.trainInfoBean.noOfCar>0 ? rsb.trainInfoBean.noOfCar : 12) + "" + (rsb.trainInfoBean.speed!=null ? rsb.trainInfoBean.speed.charAt(0): "") + "\n\n";
                                    if(rsb.trainInfoBean.endStnId==act.dstId) {
                                        msg+= "Reaches "+MRMApp.me().getStation(rsb.trainInfoBean.endStnId).getName()+" at "+MRMApp.getTimeInFormatPM(rsb.trainInfoBean.endStnTime);
                                        msgTitle+=MRMApp.me().getStation(rsb.trainInfoBean.endStnId).getName()+"\n\n";
                                    }
                                }
                                msg = msgTitle + msg+"\n--sent via MRMApp";
                                share.putExtra(Intent.EXTRA_TEXT, msg);
                            } else {
                                share.setType("image/png");
                                View headerV = getActivity().findViewById(R.id.routeRow1RL);
                                Bitmap b = getWholeListViewItemsToBitmap(mResultListView, headerV);

                                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                b.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "mrm_route.jpg");

                                boolean fileCreated = f.createNewFile();
                                if(fileCreated) {
                                    FileOutputStream fo = new FileOutputStream(f);
                                    fo.write(bytes.toByteArray());
                                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                                }
                            }
                            startActivityForResult(Intent.createChooser(share, "Share via"), 101);
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.e("File Sharing Error", e.getMessage());
                        }
                    }
                });
                builder.show();


            }
        });

        final View feedbackRL = footerView.findViewById(R.id.feedbackRL);

        feedbackRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType(getActivity().getString(R.string.email_type_img));
                    View headerV = getActivity().findViewById(R.id.routeRow1RL);
                    Bitmap b = getWholeListViewItemsToBitmap(mResultListView, headerV);

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    b.compress(Bitmap.CompressFormat.JPEG, 95, bytes);
                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + getActivity().getString(R.string.default_image_name));

                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());

                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getActivity().getString(R.string.feedback_email)});
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getActivity().getString(R.string.feedback_email_subject) + MRMApp.versionStr);
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getActivity().getString(R.string.default_text) + MRMApp.me().regid);

                    emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                    startActivityForResult(Intent.createChooser(emailIntent, getActivity().getString(R.string.feedback_tag)),101);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("File Sharing Error", e.getMessage());
                }
            }
        });

        mResultAdapter = new ResultViewAdapter(act.mRsbList, getActivity(), this);
        // Set CustomAdapter as the adapter for RecyclerView.
        mResultAdapter.setOnTapListener(new OnTapListener() {
            @Override
            public void onTapView(int position) {

            }
        });
        mResultListView.setAdapter(mResultAdapter);

        findRoute();

        routeReverseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Station temp = mRouteBean.srcStn;
                mRouteBean.srcStn = mRouteBean.dstStn;
                mRouteBean.dstStn = temp;
                String tempStr = srcStnTV.getText().toString();
                srcStnTV.setText(dstStnTV.getText());
                dstStnTV.setText(tempStr);
                findRoute();
            }
        });

        dateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        timeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });

        ImageView backIV = (ImageView) v.findViewById(R.id.backIV);
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                act.finish();
            }
        });

        v.findViewById(R.id.mapViewFAB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                act.showRouteMapFragment();

//                if (sMapIV.getVisibility() == View.VISIBLE) {
//                    sMapIV.setVisibility(View.GONE);
//                    mResultListView.setVisibility(View.VISIBLE);
//                    mapViewIV.setImageResource(R.drawable.ic_map_view);
//                    routeReverseIV.setVisibility(View.VISIBLE);
//                } else {
//                    sMapIV.redrawMap(act.mRsbList);
//                    sMapIV.setVisibility(View.VISIBLE);
//                    sMapIV.invalidate();
//                    mResultListView.setVisibility(View.GONE);
//                    mapViewIV.setImageResource(R.drawable.ic_listview);
//                    routeReverseIV.setVisibility(View.GONE);
//                }

            }
        });

        return v;
    }

    public static Bitmap getWholeListViewItemsToBitmap(ListView mResultListView, View headerView) {

        ListView listview = mResultListView;
        ListAdapter adapter = listview.getAdapter();
        int itemscount = adapter.getCount();
        int allitemsheight = 0;
        List<Bitmap> bmps = new ArrayList<>();

        headerView.measure(View.MeasureSpec.makeMeasureSpec(listview.getWidth(), View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        headerView.layout(0, 0, headerView.getMeasuredWidth(), headerView.getMeasuredHeight());
        headerView.setDrawingCacheEnabled(true);
        headerView.buildDrawingCache();
        bmps.add(Bitmap.createBitmap(headerView.getDrawingCache()));
        allitemsheight += headerView.getMeasuredHeight();

        for (int i = 0; i < itemscount; i++) {

            View childView = adapter.getView(i, null, listview);
            childView.measure(View.MeasureSpec.makeMeasureSpec(listview.getWidth(), View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            childView.layout(0, 0, childView.getMeasuredWidth(), childView.getMeasuredHeight());
            childView.setDrawingCacheEnabled(true);
            childView.buildDrawingCache();
            bmps.add(Bitmap.createBitmap(childView.getDrawingCache()));
            allitemsheight += childView.getMeasuredHeight();
        }

        Bitmap bigbitmap = Bitmap.createBitmap(listview.getMeasuredWidth(), allitemsheight, Bitmap.Config.RGB_565);
        Canvas bigcanvas = new Canvas(bigbitmap);

        Paint paint = new Paint();
        int iHeight = 0;

        for (int i = 0; i < bmps.size(); i++) {
            Bitmap bmp = bmps.get(i);
            bigcanvas.drawBitmap(bmp, 0, iHeight, paint);
            iHeight += bmp.getHeight();
            if (bmp != null && !bmp.isRecycled()) {
                bmp.recycle();
            }
            bmp = null;
        }

        for (int i = 0; i < itemscount; i++) {
            View childView = adapter.getView(i, null, listview);
            childView.setDrawingCacheEnabled(false);
        }

        return bigbitmap;
    }


    public void findRoute() {
        Bundle args = new Bundle();
        args.putInt("src", mRouteBean.srcStn.getId().intValue());
        args.putInt("dst", mRouteBean.dstStn.getId().intValue());
        mRouteBean.dateTime = Calendar.getInstance();

        if(getActivity().getSupportLoaderManager().getLoader(1)==null) {
            getActivity().getSupportLoaderManager().initLoader(1, args, this);
        } else {
            getActivity().getSupportLoaderManager().restartLoader(1, args, this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        if (id == 1) {
            return new RouteChoiceLoader(getActivity(), args.getInt("src"), args.getInt("dst"));
        }
        return null;

    }

    @Override
    public void onLoaderReset(Loader loader) {
        if (loader.getId() == 1) {
            routeOptionSpinner.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if (loader.getId() == 1) {

            mRouteBean.rcbList = (List<RouteChoiceBean>) data;
            srcStnTV.setText(mRouteBean.srcStn.getName());
            dstStnTV.setText(mRouteBean.dstStn.getName());

            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_route_format), Locale.US);
            dateTV.setText(sdf.format(mRouteBean.dateTime.getTime()));

            sdf = new SimpleDateFormat(getResources().getString(R.string.time_route_format), Locale.US);
            timeTV.setText(sdf.format(mRouteBean.dateTime.getTime()));

            rcAdapter = new RouteSpinnerAdapter(getActivity(), mRouteBean.rcbList);
            routeOptionSpinner.setAdapter(rcAdapter);

            routeOptionSpinner.setSelection(act.routeId, true);

            fareAdapter = new RouteSpinnerAdapter(getActivity(), new ArrayList<RouteChoiceBean>());
            fareSpinner.setAdapter(fareAdapter);

            routeOptionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    act.routeId = position;
                    reloadResults();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            reloadResults();
            favEntry = InternalDao.getDao().getFavRoute(mRouteBean.srcStn.getId().intValue(), mRouteBean.dstStn.getId().intValue(), act.routeId);
            if (favEntry == null) { //add recent
                addToRecent(mRouteBean.srcStn.getId().intValue(), mRouteBean.dstStn.getId().intValue(), act.routeId);
                addFavRouteIV.setImageResource(R.drawable.star_off);
                addFavRouteTV.setText("Add to Favourite");
                Toast.makeText(getActivity(), "Added to Recent.", Toast.LENGTH_SHORT).show();
            } else if (favEntry.getFavourite()) {
                updateLastUsedTime(favEntry);
                addFavRouteIV.setImageResource(R.drawable.star_on);
                addFavRouteTV.setText("Remove from Favourite");
            } else {
                updateLastUsedTime(favEntry);
            }
        }
    }

    public void reloadResults() {
        int position = act.routeId;
        RouteChoiceBean rcb = mRouteBean.rcbList.get(position);
        act.mRsbList = new ArrayList<>();
        if (frdTask != null && frdTask.isRunning) {
            frdTask.cancel(true);
        }
        frdTask = new FindRouteDetailsTask();
        if (rcb != null && rcb.stnIdList != null && mRouteBean.srcStn != null && mRouteBean.dstStn != null) {
            String StnList = mRouteBean.srcStn.getId() + MRMApp.OR_SEPARATOR + rcb.stnIdList.trim() + MRMApp.OR_SEPARATOR + mRouteBean.dstStn.getId();

            int selectedTimeInMinute = mRouteBean.dateTime.get(Calendar.HOUR_OF_DAY) * 60 + mRouteBean.dateTime.get(Calendar.MINUTE);
            frdTask.execute(StnList, selectedTimeInMinute + "", rcb.lineIdList);
        }

        QueryBuilder<TrainFare> fareQb = MRMApp.me().getDaoSession().getTrainFareDao().queryBuilder();
        fareQb.where(TrainFareDao.Properties.FromId.eq(mRouteBean.srcStn.getId()),
                TrainFareDao.Properties.ToId.eq(mRouteBean.dstStn.getId()));


        for (TrainFare tf : fareQb.list()) {
            RouteChoiceBean fareRcb = new RouteChoiceBean();
            fareRcb.stnNameList = "2ND";
            fareRcb.timeOfJourney = tf.getFare_2D();
            fareAdapter.updateItem(fareRcb, 0);
            fareRcb = new RouteChoiceBean();
            fareRcb.stnNameList = "1ST";
            fareRcb.timeOfJourney = tf.getFare_1D();
            fareAdapter.updateItem(fareRcb, 1);
        }
    }


    public void addToRecent(int srcId, int dstId, int position) {
        FavRoute favRoute = new FavRoute(null, srcId, dstId, position, mRouteBean.rcbList.get(position).stnIdList, mRouteBean.rcbList.get(position).stnNameList, false, mRouteBean.dateTime.getTimeInMillis(), null, 0, true);
        InternalDao.getDao().upsertFavRoute(favRoute);
    }

    public void updateLastUsedTime(FavRoute favRoute) {
        if(mRouteBean!=null && mRouteBean.dateTime!=null) {
            favRoute.setLastModified(mRouteBean.dateTime.getTimeInMillis());
        } else {
            favRoute.setLastModified(Calendar.getInstance().getTimeInMillis());
        }
        InternalDao.getDao().upsertFavRoute(favRoute);
    }

    public void addToFavorite(int srcId, int dstId, int position) {
        FavRoute favRoute = new FavRoute(null, srcId, dstId, position, mRouteBean.rcbList.get(position).stnIdList, mRouteBean.rcbList.get(position).stnNameList, true, mRouteBean.dateTime.getTimeInMillis(), null, 0, true);
        InternalDao.getDao().upsertFavRoute(favRoute);
    }


    public void showDateDialog() {
        if (mRouteBean != null)
            new DatePickerDialog(getActivity(), this, mRouteBean.dateTime.get(Calendar.YEAR),
                    mRouteBean.dateTime.get(Calendar.MONTH), mRouteBean.dateTime.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void showTimeDialog() {
        if (mRouteBean != null)
            new TimePickerDialog(getActivity(), this, mRouteBean.dateTime.get(Calendar.HOUR_OF_DAY), mRouteBean.dateTime.get(Calendar.MINUTE),
                    DateFormat.is24HourFormat(getActivity())).show();
    }

    public void getNextTrain(RouteScheduleBean rsb, int position) {
        if (frdTask != null && frdTask.isRunning) {
            return;
        }
        if (rcAdapter != null) {
            RouteChoiceBean rcb = rcAdapter.getItem(act.routeId);
            if (rcb != null && rcb.stnIdList != null && mRouteBean.srcStn != null && mRouteBean.dstStn != null) {
                String StnList = mRouteBean.srcStn.getId() + MRMApp.OR_SEPARATOR + rcb.stnIdList.trim() + MRMApp.OR_SEPARATOR + mRouteBean.dstStn.getId();

                int selectedTimeInMinute = 0;
                frdTask = new RouteFragment.FindRouteDetailsTask();
                frdTask.position = position;
                frdTask.oldRsb = rsb;
                frdTask.isNext = true;
                frdTask.execute(StnList, selectedTimeInMinute + "", rcb.lineIdList);
            }
        }
    }

    public void getPrevTrain(RouteScheduleBean rsb, int position) {
        if (frdTask != null && frdTask.isRunning) {
            return;
        }
        if (rcAdapter != null) {
            RouteChoiceBean rcb = rcAdapter.getItem(act.routeId);
            if (rcb != null && rcb.stnIdList != null && mRouteBean.srcStn != null && mRouteBean.dstStn != null) {
                String StnList = mRouteBean.srcStn.getId() + MRMApp.OR_SEPARATOR + rcb.stnIdList.trim() + MRMApp.OR_SEPARATOR + mRouteBean.dstStn.getId();

                int selectedTimeInMinute = 0;
                frdTask = new RouteFragment.FindRouteDetailsTask();
                frdTask.position = position;
                frdTask.oldRsb = rsb;
                frdTask.isNext = false;
                frdTask.execute(StnList, selectedTimeInMinute + "", rcb.lineIdList);
            }
        }
    }


    @Override
    public void updateResultCard(TrainInfoBean tib, int position) {
        Log.d("mrm_logo", "TIB " + tib.trainId);
        if (rcAdapter != null) {
            RouteChoiceBean rcb = rcAdapter.getItem(act.routeId);
            if (rcb != null && rcb.stnIdList != null && mRouteBean.srcStn != null && mRouteBean.dstStn != null) {
                String StnList = mRouteBean.srcStn.getId() + MRMApp.OR_SEPARATOR + rcb.stnIdList.trim() + MRMApp.OR_SEPARATOR + mRouteBean.dstStn.getId();
                if (frdTask != null && frdTask.isRunning) {
                    frdTask.cancel(true);
                }
                int selectedTimeInMinute = 0;
                frdTask = new RouteFragment.FindRouteDetailsTask();
                frdTask.position = position;
                frdTask.oldRsb = act.mRsbList.get(position);
                frdTask.localTib = tib;
                frdTask.isNext = true;
                frdTask.execute(StnList, selectedTimeInMinute + "", rcb.lineIdList);
            }
        }
    }

    public class FindRouteDetailsTask extends AsyncTask<String, RouteScheduleBean, List<RouteScheduleBean>> {

        public int position;
        public RouteScheduleBean oldRsb;
        public boolean isNext, isRunning;
        public TrainInfoBean localTib;
        ;

        private int startJourneyTime, endJourneyTime;


        private ProgressDialog dialog = new ProgressDialog(act);

        public RouteScheduleBean getRouteScheduleBean(int startTimeInMinute, RouteScheduleBean rsb, Station fromSm, Station toSm, char directionStn) {

            if (rsb == null) {
                rsb = new RouteScheduleBean();
                List<LineRouteDetails> list = MRMApp.me().getDaoSession().getLineRouteDetailsDao().queryRaw(
                        " WHERE T.ROUTE_INFO like '%," + fromSm.getId() + ",%' " +
                                " AND T.ROUTE_INFO like '%," + toSm.getId() + ",%' ORDER BY ORDER_NO");

                if (list != null && list.size() > 0) {
                    rsb.line = MRMApp.me().getLine(list.get(0).getLineId().intValue());
                    String routeInfo = list.get(0).getRouteInfo();
                    int prevIndex = routeInfo.indexOf("," + fromSm.getId() + ",");
                    int smIndex = routeInfo.indexOf("," + toSm.getId() + ",");
                    if (prevIndex < smIndex) {
                        routeInfo = routeInfo.substring(prevIndex, smIndex + ("," + toSm.getId() + ",").length());
                        Log.d("Route got <", routeInfo);
                        String arr[] = routeInfo.split(",");

                        for (String stn : arr) {
                            if (stn.trim().length() > 0) {
                                Station tmpSm = MRMApp.me().getStation(Integer.parseInt(stn));
                                StationTimeBean stb = new StationTimeBean();
                                stb.stnId = tmpSm.getId().intValue();
                                stb.stationName = tmpSm.getName();
                                rsb.stbMap.put(stb.stnId, stb);
                                rsb.stnIdList.add(stb.stnId);
                            }
                        }
                    } else {
                        routeInfo = routeInfo.substring(smIndex, prevIndex + ("," + fromSm.getId() + ",").length());
                        Log.d("Route got > ", routeInfo);
                        String arr[] = routeInfo.split(",");
                        for (int x = arr.length - 1; x >= 0; x--) {
                            String stn = arr[x];

                            if (stn.trim().length() > 0) {
                                Station tmpSm = MRMApp.me().getStation(Integer.parseInt(stn));
                                StationTimeBean stb = new StationTimeBean();
                                stb.stnId = tmpSm.getId().intValue();
                                stb.stationName = tmpSm.getName();

                                rsb.stbMap.put(stb.stnId, stb);
                                rsb.stnIdList.add(stb.stnId);
                            }
                        }
                    }
                } else {
                    Log.e("Route", "Route not found");
                    return null;
                }
            }

            rsb.stbMap = new SparseArray<>();

            for (int stnId : rsb.stnIdList) {
                StationTimeBean stb = new StationTimeBean();
                stb.stnId = stnId;
                stb.stationName = MRMApp.me().getStation(stnId).getName();
                rsb.stbMap.put(stnId, stb);
            }

            String tableName = "train_schedule_details";
            switch (rsb.line.getId().intValue()) {
                case 1:
                    tableName = "tsd_l1";
                    break;
                case 2:
                    tableName = "tsd_l2";
                    break;
                case 3:
                    tableName = "tsd_l34";
                    break;
                case 4:
                    tableName = "tsd_l34";
                    break;
                case 5:
                    tableName = "tsd_l56";
                    break;
                case 6:
                    tableName = "tsd_l56";
                    break;
                case 7:
                    tableName = "tsd_l7";
                    break;
                case 8:
                    tableName = "tsd_l8";
                    break;
            }

            String sundayQuery = " and tas1.SUNDAY_ONLY = 0 ";
            if (mRouteBean.dateTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                sundayQuery = " and tas1.NOT_ON_SUNDAY = 0 ";
            }

            String tsdQuery = "SELECT * FROM " + tableName + " T " +
                    ", (select tas1.station_id as src_station_id, " +
                    " tas1.train_id as train_id, " +
                    " tas1.line_id as line_id, " +
                    " tas1.time_in_minutes as src_time_in_mins, " +
                    " tas2.time_in_minutes as dest_time_in_mins, " +
                    " tas2.station_id as dest_station_id " +
                    " from " + tableName + " tas1, " + tableName + " tas2 " +
                    " where tas1.train_id = tas2.train_id " +
                    " and tas1.station_id = " + fromSm.getId() +
                    " and tas2.station_id = " + toSm.getId() + sundayQuery +
                    " and tas1.time_in_minutes < tas2.time_in_minutes " +
                    " and tas1.time_in_minutes " + (directionStn) + " " + startTimeInMinute +
                    " and (tas2.time_in_minutes-tas1.time_in_minutes) < 180 " +
                    " and tas1.direction = tas2.direction " +
                    " order by tas1.time_in_minutes " + (directionStn == '>' ? " asc " : " desc ") +
                    " limit 1) Q2 " +
                    " WHERE T.TRAIN_ID = Q2.train_id" +
                    " AND T.TIME_IN_MINUTES >=  Q2.src_time_in_mins " +
                    " AND T.TIME_IN_MINUTES <= dest_time_in_mins " +
                    " ORDER BY T.TIME_IN_MINUTES ";

            if (localTib != null) {
                tsdQuery = "SELECT * FROM " + tableName + " tas1 " +
                        " WHERE tas1.TRAIN_ID = " + localTib.trainId + sundayQuery +
                        " AND tas1.TIME_IN_MINUTES >=  " + localTib.startStnTime +
                        " AND tas1.TIME_IN_MINUTES <=  " + localTib.endStnTime +
                        " ORDER BY tas1.TIME_IN_MINUTES ";
                localTib = null;
            }

            Cursor cursor = MRMApp.me().getDaoSession().getDatabase().rawQueryWithFactory(null, tsdQuery, null, null);

            boolean add24Hours = false;

            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.isClosed() || cursor.isAfterLast()) {
                    tsdQuery = "SELECT * FROM " + tableName + " T " +
                            ", (select tas1.station_id as src_station_id, " +
                            " tas1.train_id as train_id, " +
                            " tas1.line_id as line_id, " +
                            " tas1.time_in_minutes as src_time_in_mins, " +
                            " tas2.time_in_minutes as dest_time_in_mins, " +
                            " tas2.station_id as dest_station_id " +
                            " from " + tableName + " tas1, " + tableName + " tas2 " +
                            " where tas1.train_id = tas2.train_id " +
                            " and tas1.station_id = " + fromSm.getId() +
                            " and tas2.station_id = " + toSm.getId() + sundayQuery +
                            " and tas1.time_in_minutes < tas2.time_in_minutes " +
                            // " and tas1.time_in_minutes "+(directionStn)+" " + startTimeInMinute +
                            " and (tas2.time_in_minutes-tas1.time_in_minutes) < 180 " +
                            " and tas1.direction = tas2.direction " +
                            " order by tas1.time_in_minutes " + (directionStn == '>' ? " asc " : " desc ") +
                            " limit 1) Q2 " +
                            " WHERE T.TRAIN_ID = Q2.train_id" +
                            " AND T.TIME_IN_MINUTES >=  Q2.src_time_in_mins " +
                            " AND T.TIME_IN_MINUTES <= dest_time_in_mins " +
                            " ORDER BY T.TIME_IN_MINUTES ";

                    cursor = MRMApp.me().getDaoSession().getDatabase().rawQueryWithFactory(null, tsdQuery, null, null);
                    add24Hours = true;
                }
            }

            boolean isFirstRow = true;
            int i = 0;
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isClosed() && !cursor.isAfterLast()) {
                    Station stn = MRMApp.me().getStation(cursor.getInt(12));
                    if (isFirstRow) {
                        rsb.line = MRMApp.me().getLine(cursor.getInt(11));

                        rsb.trainInfoBean = new TrainInfoBean();
                        rsb.trainInfoBean.trainId = cursor.getInt(0);
                        //trainInfoBean.trainCode = tsd.getTrainCode();
                        //trainInfoBean.trainNumber = tsd.getTrainNumber();
                        //trainInfoBean.emuCode = tsd.getEmuCode();
                        rsb.trainInfoBean.noOfCar = cursor.getInt(4);
                        if (cursor.getString(6) != null && !cursor.getString(6).equalsIgnoreCase("S")) {
                            rsb.trainInfoBean.speed = "Fast";
                        } else {
                            rsb.trainInfoBean.speed = "Slow";
                        }
                        rsb.trainInfoBean.specialInfo = cursor.getString(7);
                        rsb.trainInfoBean.sundayOnly = cursor.getInt(8) == 1 ? true : false;
                        rsb.trainInfoBean.holidayOnly = cursor.getInt(9) == 1 ? true : false;
                        rsb.trainInfoBean.notOnSunday = cursor.getInt(10) == 1 ? true : false;
                        rsb.trainInfoBean.startStnId = stn.getId().intValue();
                        rsb.trainInfoBean.firstStnName = MRMApp.me().getStation(cursor.getInt(16)).getName();

                        rsb.trainInfoBean.endStnId = toSm.getId().intValue();

                        rsb.trainInfoBean.lastStnName = MRMApp.me().getStation(cursor.getInt(17)).getName();
                        rsb.trainInfoBean.lastStnCode = MRMApp.me().getStation(cursor.getInt(17)).getCode();

                        rsb.trainInfoBean.startStnTime = cursor.getInt(13) + (add24Hours ? 24 * 60 : 0);

                        isFirstRow = false;
                    }

                    StationTimeBean stb = rsb.stbMap.get(stn.getId().intValue());
                    if (stb == null) {
                        Log.e("STB", "wierd, could find STB for " + stn.getId() + " " + stn.getName());
                        stb = new StationTimeBean();
                        stb.stnId = stn.getId().intValue();
                        stb.stationName = stn.getName();
                    }
                    stb.timeInMinute = cursor.getInt(13) + (add24Hours ? 24 * 60 : 0);
                    stb.platformNo = cursor.getString(14);
                    stb.platformSide = cursor.getString(15);
                    rsb.stbMap.put(stn.getId().intValue(), stb);
                    cursor.moveToNext();
                    i++;
                }
                rsb.noOfStop = i;
                cursor.close();
            }
            if (i == 0) {
                return null;
            }

            rsb.noOfStation = rsb.stnIdList.size();
            rsb.trainInfoBean.endStnTime = rsb.stbMap.get(rsb.trainInfoBean.endStnId).timeInMinute;
            rsb.journeyTime = rsb.trainInfoBean.endStnTime - rsb.trainInfoBean.startStnTime;
            return rsb;
        }

        @Override
        protected List<RouteScheduleBean> doInBackground(String... param) {
            if (param != null && param.length >= 3) {
                List<RouteScheduleBean> rsbList = new ArrayList<>();
                Station prevSm = null;
                long timerMain = System.currentTimeMillis();
                int selectedTimeInMinute = Integer.parseInt(param[1]);
                int prevCardTime = mRouteBean.dateTime.get(Calendar.HOUR_OF_DAY) * 60 + mRouteBean.dateTime.get(Calendar.MINUTE);

                Log.i(TAG,"param: "+param[0]);

                int i = 0;
                for (String stnIdStr : param[0].trim().split(MRMApp.OR_SEARCH)) {
                    if (stnIdStr.trim().length() == 0 || stnIdStr.equals("") || stnIdStr.equals("0")) {
                        continue;
                    }
                    Station sm = MRMApp.me().getStation(Integer.parseInt(stnIdStr));
                    if (prevSm == null) {
                        prevSm = sm;
                        continue;
                    }

                    RouteScheduleBean myRsb;
                    if (oldRsb != null && (i < position || (!isNext && i > position))) { //for Next copy all prev. card till this card.  // for Prev. copy all card except selected one
                        myRsb = act.mRsbList.get(i);
                    } else {
                        if (oldRsb != null) {
                            if (selectedTimeInMinute == 0) {
                                selectedTimeInMinute = oldRsb.stbMap.get(oldRsb.trainInfoBean.startStnId).timeInMinute + TIME_NEED_TO_BOARD;
                            }
                            if (i == position && !isNext) {
                                selectedTimeInMinute = oldRsb.stbMap.get(oldRsb.trainInfoBean.startStnId).timeInMinute;
                                myRsb = getRouteScheduleBean(selectedTimeInMinute, act.mRsbList.get(i), prevSm, sm, '<');
                            } else {
                                myRsb = getRouteScheduleBean(selectedTimeInMinute, act.mRsbList.get(i), prevSm, sm, '>');
                            }
                        } else {
                            myRsb = getRouteScheduleBean(selectedTimeInMinute, null, prevSm, sm, '>');
                        }
                        myRsb.timeToBoard = myRsb.trainInfoBean.startStnTime - prevCardTime;
                        selectedTimeInMinute = myRsb.stbMap.get(myRsb.trainInfoBean.endStnId).timeInMinute + TIME_NEED_TO_BOARD;

                    }
                    prevCardTime = myRsb.stbMap.get(myRsb.trainInfoBean.endStnId).timeInMinute;
                    rsbList.add(myRsb);
                    if (prevSm.getId().intValue() == mRouteBean.srcStn.getId()) {
                        startJourneyTime = myRsb.trainInfoBean.startStnTime;
                    }
                    if (sm.getId().intValue() == mRouteBean.dstStn.getId()) {
                        endJourneyTime = myRsb.stbMap.get(myRsb.trainInfoBean.endStnId).timeInMinute;
                    }
                    prevSm = sm;
                    i++;
                }
                Log.d("Timing ", " Full route calculation : " + (System.currentTimeMillis() - timerMain));
                return rsbList;
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            isRunning = true;
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            isRunning = false;
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }


        @Override
        protected void onPostExecute(List<RouteScheduleBean> routeScheduleBeans) {
            super.onPostExecute(routeScheduleBeans);
            isRunning = false;
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            act.mRsbList = routeScheduleBeans;
            mResultAdapter.updateList(routeScheduleBeans);
            int position = act.routeId;
            if (position >= 0) {
                RouteChoiceBean rcb = mRouteBean.rcbList.get(position);
                rcb.timeOfJourney = (endJourneyTime - startJourneyTime);
                Log.d("MRMRoute", "timeOfJourney " + rcb.timeOfJourney + " for " + position);
                mRouteBean.rcbList.set(position, rcb);
                rcAdapter.updateItem(rcb, position);
                routeOptionSpinner.setSelection(position);
                rcAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static class RouteSpinnerAdapter extends ArrayAdapter<RouteChoiceBean> {

        private final Context context;
        private final List<RouteChoiceBean> values;

        public RouteSpinnerAdapter(Context context, List<RouteChoiceBean> mList) {
            super(context, R.layout.v_via_spinner_entry, mList);
            this.context = context;
            this.values = mList;
        }

        public void updateItem(RouteChoiceBean rcb, int position) {
            if (position < this.values.size()) {
                this.values.set(position, rcb);
            } else {
                this.values.add(position, rcb);
            }
            notifyDataSetChanged();
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (v == null) {
                v = inflater.inflate(R.layout.v_via_spinner_entry, parent, false);
            }
            TextView viaRouteTV = (TextView) v.findViewById(R.id.viaRouteTV);
            TextView timeDurationTV = (TextView) v.findViewById(R.id.timeDurationTV);
            TextView changeCountTV = (TextView) v.findViewById(R.id.changeCountTV);

            RouteChoiceBean rcb = values.get(position);

            viaRouteTV.setText("Via: " + rcb.stnNameList);
            viaRouteTV.setTypeface(MRMApp.robotoMedium);
            if (rcb.timeOfJourney != 0) {
                if (rcb.timeOfJourney < 60) {
                    timeDurationTV.setText(rcb.timeOfJourney + " min");
                } else {
                    timeDurationTV.setText(MRMApp.getTimeInFormat(rcb.timeOfJourney) + " H");
                }
            } else {
                timeDurationTV.setText("");
            }
            changeCountTV.setText((rcb.noOfStops < 1 ? "no" : rcb.noOfStops) + " change");

            return v;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (v == null) {
                v = inflater.inflate(R.layout.v_via_text_entry, parent, false);
            }
            TextView viaRouteTV = (TextView) v.findViewById(R.id.viaRouteTV);
            TextView timeDurationTV = (TextView) v.findViewById(R.id.timeDurationTV);
            TextView changeCountTV = (TextView) v.findViewById(R.id.changeCountTV);

            RouteChoiceBean rcb = values.get(position);

            viaRouteTV.setText("Via: " + rcb.stnNameList);
            viaRouteTV.setTypeface(MRMApp.robotoMedium);
            if (rcb.timeOfJourney != 0) {
                if (rcb.timeOfJourney < 60) {
                    timeDurationTV.setText(rcb.timeOfJourney + " min");
                } else {
                    timeDurationTV.setText(MRMApp.getTimeInFormat(rcb.timeOfJourney) + " H");
                }
            } else {
                timeDurationTV.setText("");
            }
            changeCountTV.setText((rcb.noOfStops < 1 ? "no" : rcb.noOfStops) + " change");

            return v;
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mRouteBean.dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mRouteBean.dateTime.set(Calendar.MINUTE, minute);
        SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.time_route_format), Locale.US);
        timeTV.setText(sdf.format(mRouteBean.dateTime.getTime()));
        reloadResults();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mRouteBean.dateTime.set(Calendar.YEAR, year);
        mRouteBean.dateTime.set(Calendar.MONTH, monthOfYear);
        mRouteBean.dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_route_format), Locale.US);
        dateTV.setText(sdf.format(mRouteBean.dateTime.getTime()));
        reloadResults();
    }
}
