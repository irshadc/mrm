package com.glassboxsoftware.mrmapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SectionIndexer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.dao.Line;
import com.glassboxsoftware.mrmapp.dao.Station;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@linkCallbacks}
 * interface.
 */
public class StationListFragment extends DialogFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String SEL_STATION = "SELECTED_STATION";
    private static final String TAG = "StationListFragment";
    private static final String ADV_SEARCH = "ADV_SEARCH";
    private static final String CURR_LOCATION = "CURR_LOCATION";

    private Long selectedStation;
    private boolean advSearch;

    private String [] lineArrSpinner, lineArrRBtn;

    private Station srcSM, dstSM;

    private ImageView clearSearchIV, closeSearchIV;

    private ListView mStationLV;
    private List<Station> smList;
    private StationListAdapter mAdapter;

    private EditText searchET;

    private Spinner localSpinner;

    public RadioGroup lineSelRG;

    private String favStnIds, removedStnIds;

    private SharedPreferences sharedPref;

    private OnFragmentInteractionListener mListener;

    protected Location mLastLocation;

    public static StationListFragment newInstance(int stnId, boolean advSearch, Location mLastLocation) {
        StationListFragment fragment = new StationListFragment();
        Bundle args = new Bundle();
        if(stnId > 0)
            args.putLong(SEL_STATION, stnId);
        args.putBoolean(ADV_SEARCH,advSearch);
        args.putParcelable(CURR_LOCATION,mLastLocation);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StationListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,R.style.Theme_MRM_WithNavDrawer);

        if (getArguments() != null) {
            selectedStation = getArguments().getLong(SEL_STATION);
            advSearch = getArguments().getBoolean(ADV_SEARCH);
            mLastLocation = getArguments().getParcelable(CURR_LOCATION);
        }

        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        favStnIds = sharedPref.getString("fav_stn_ids",getString(R.string.fav_stn_ids));

        lineArrSpinner = getResources().getStringArray(R.array.local_line_arr);
        lineArrRBtn = getResources().getStringArray(R.array.local_line_arr_index);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_station_list, container, false);
        assert v != null;

        mStationLV = (ListView) v.findViewById(android.R.id.list);
        smList = new ArrayList<>();

        mAdapter = new StationListAdapter(getActivity(),smList);
        mStationLV.setAdapter(mAdapter);
        mStationLV.setFastScrollEnabled(true);
        setupAdapter();

        localSpinner = (Spinner) v.findViewById(R.id.localSpinner);
        localSpinner.setAdapter(new LocalSpinnerAdapter(getActivity(), lineArrSpinner));


        searchET = (EditText) v.findViewById(R.id.searchET);

        clearSearchIV = (ImageView) v.findViewById(R.id.clearSearchIV);
        closeSearchIV = (ImageView) v.findViewById(R.id.closeSearchIV);

        lineSelRG = (RadioGroup) v.findViewById(R.id.lineSelRG);
        final Spinner localSpinner = (Spinner) v.findViewById(R.id.localSpinner);
        final RadioButton localRBtn = (RadioButton) v.findViewById(R.id.localRBtn);

//        mStationLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Station sm = smList.get(position);
//                if(sm.getName().trim().length()>1) {
//                    setStation(sm);
//                }
//            }
//        });


        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchText = s.toString().trim().toLowerCase();
                setupAdapter();
                if(lineSelRG.getVisibility()==View.VISIBLE) {
                    lineSelRG.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()>0) {
                    clearSearchIV.setVisibility(View.VISIBLE);
                } else {
                    clearSearchIV.setVisibility(View.GONE);
                    lineSelRG.setVisibility(View.VISIBLE);
                }
            }
        };

        searchET.addTextChangedListener(tw);

        localSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(lineSelRG.getCheckedRadioButtonId() == R.id.localRBtn) {
                    switch (position) {
                        case 0: lineIdStr = "1,2,3,4,5,6"; break;
                        case 1: lineIdStr = "1"; break;
                        case 2: lineIdStr = "2"; break;
                        case 3: lineIdStr = "3"; break;
                        case 4: lineIdStr = "4"; break;
                        case 5: lineIdStr = "5,6"; break;
                    }
                    localRBtn.setText(lineArrRBtn[position]);
                    setupAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        localRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localSpinner.performClick();
            }
        });

        lineSelRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.allRBtn : lineIdStr = ""; setupAdapter(); break;
                    case R.id.localRBtn : lineIdStr = "1,2,3,4,5,6"; break;
                    case R.id.monoRBtn : lineIdStr = "7"; setupAdapter(); break;
                    case R.id.metroRBtn : lineIdStr = "8"; setupAdapter(); break;
                }
            }


        });

        clearSearchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srcSM = null;
                searchET.setText("");
                clearSearchIV.setVisibility(View.INVISIBLE);
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchET.getWindowToken(), 0);
            }
        });



        closeSearchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        searchET.requestFocus();

        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        return v;
    }

    public void addToFavorite(int _id) {
        Toast.makeText(getActivity(),"Added to favourite.", Toast.LENGTH_SHORT).show();
        favStnIds = favStnIds+":"+_id+":";
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("fav_stn_ids", favStnIds);
        editor.commit();
        setupAdapter();
    }


    public void removeFromFavorite(int _id) {
        Toast.makeText(getActivity(), "Removed from favourite.", Toast.LENGTH_SHORT).show();
        favStnIds = favStnIds.replaceAll(":"+_id+":","");
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("fav_stn_ids", favStnIds);
        editor.commit();
        setupAdapter();
    }

    private String searchText, lineIdStr;
    private void setupAdapter() {
        smList = new ArrayList<>();
        List<Station> nearByList =  new ArrayList<>();
        char prevChar = '★';
        String sectionStr = "";
        boolean isFavorite = false;
        for (Station sm : MRMApp.me().masterStationList) {
            if(selectedStation!=null && selectedStation.intValue()!=0 && sm.getId().equals(selectedStation)) {
                continue;
            }
            if(sm.getLineIds()!=null && lineIdStr!=null && lineIdStr.trim().length()>0) {
                boolean foundOnLine = false;
                for(String linePart : lineIdStr.split(",")) {
                    if(sm.getLineIds().indexOf(linePart)>=0) {
                        foundOnLine = true;
                    }
                }
                if(!foundOnLine) {
                    continue;
                }
            }
            if(searchText!=null && !sm.getName().toLowerCase().startsWith(searchText)) {
                continue;
            }
            if(srcSM!=null && srcSM.getId()==sm.getId()) {
                continue;
            }
            if(dstSM!=null && dstSM.getId()==sm.getId()) {
                continue;
            }
            if (sm.getName().trim().toUpperCase().charAt(0) != prevChar) {
                prevChar = sm.getName().trim().toUpperCase().charAt(0);
                Station dummy = new Station();
                dummy.setName(prevChar + "");
                sectionStr += prevChar;
                smList.add(dummy);
            }
            if(mLastLocation!=null && sm.getLat()!=null && sm.getLng()!=null) {
                Location smLocation = new Location(sm.getName());
                smLocation.setLatitude(sm.getLat());
                smLocation.setLongitude(sm.getLng());
                float distance = mLastLocation.distanceTo(smLocation);
                if(distance < 2000) {
                    sm.nearby = true;
                    sm.distance = (int) distance;
                }
            }
            if(favStnIds.indexOf(":"+sm.getId()+":")>=0) {
                sm.favorite = true;
                smList.add(0,sm);
                isFavorite = true;
            } else {
                sm.favorite = false;
                if(sm.nearby) {
                    nearByList.add(sm);
                } else {
                    smList.add(sm);
                }
            }
        }
        if(isFavorite) {
            Station dummy = new Station();
            dummy.setName("★");
            sectionStr = "★"+sectionStr;
            if(nearByList.size()>0) {
                Collections.sort(nearByList, new Comparator<Station>() {
                    @Override
                    public int compare(Station  stn1, Station  stn2)
                    {
                        return  stn1.distance - stn2.distance;
                    }
                });

                smList.add(0, dummy);
                smList.addAll(0, nearByList);

                dummy = new Station();
                dummy.setName("⌖");
                sectionStr = "⌖" + sectionStr;
                smList.add(0, dummy);
            }
        }
        mAdapter.updateList(smList,sectionStr);
    }

    private class StationListAdapter extends ArrayAdapter<Station> implements SectionIndexer {

        private String mSections = "⌖★ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private final Context context;
        private List<Station> values = new ArrayList<>();
        private LayoutInflater mInflater;

        public StationListAdapter(Context context, List<Station> mList) {
            super(context, R.layout.v_station_entry,mList);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.context = context;
            this.values = mList;

        }

        public void updateList(List<Station> mList,String sections){
            this.values.clear();
            this.values.addAll(mList);
            this.mSections = sections;
            mAdapter.notifyDataSetChanged();
        }

        private class ViewHolder {
            public TextView stnNameTV,stnLineTV, distanceTV;
            public ImageView favStnIV,myLocationIV;
            public LinearLayout stationLL;
        }

        private View.OnClickListener setStnClickListener  = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Station mStn = (Station) v.getTag();
                final String arr[]=mStn.getLineIds().split(",");
                if(advSearch && arr.length>1) {
                    if(lineIdStr!=null && lineIdStr.trim().length()>0 && lineIdStr.indexOf(',')<0) {
                        setupStation(mStn, Integer.parseInt(lineIdStr));
                        dismiss();
                    } else {
                        CharSequence lineNames[] = new CharSequence[arr.length];
                        for (int i = 0; i < arr.length; i++) {
                            lineNames[i] = MRMApp.me().getLine(Integer.parseInt(arr[i])).getName();
                        }

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Choose Line");
                        builder.setItems(lineNames, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setupStation(mStn, Integer.parseInt(arr[which]));
                                dismiss();
                            }
                        });
                        builder.show();
                    }
                } else {
                    setupStation(mStn,Integer.parseInt(arr[0]));
                    dismiss();
                }
            }
        };

        private void setupStation(Station mStn, int lineId){
            mListener.setStation(mStn, lineId, advSearch);
        }

        private View.OnClickListener favClickListener  = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Station stnBean = (Station) v.getTag();
                if(stnBean.favorite) {
                    removeFromFavorite(stnBean.getId().intValue());
                } else {
                    addToFavorite(stnBean.getId().intValue());
                }
            }
        };

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final Station stnBean = values.get(position);
            ViewHolder holder = new ViewHolder();

           // if (convertView == null) {
//                holder = new ViewHolder();
                if(stnBean.getName().trim().length()>1) {
                    convertView = mInflater.inflate(R.layout.v_station_entry,  parent, false);
                    holder.stnNameTV = (TextView) convertView.findViewById(R.id.titleTV);
                    holder.stnLineTV = (TextView) convertView.findViewById(R.id.descTV);
                    holder.favStnIV = (ImageView) convertView.findViewById(R.id.clearSrcIV);
                    holder.myLocationIV = (ImageView) convertView.findViewById(R.id.myLocationIV);
                    holder.distanceTV = (TextView) convertView.findViewById(R.id.distanceTV);

                    holder.stationLL = (LinearLayout) convertView.findViewById(R.id.stationLL);
                } else {
                    convertView = mInflater.inflate(R.layout.v_station_header,  parent, false);
                    holder.stnNameTV = (TextView) convertView.findViewById(R.id.titleTV);
                    holder.stnNameTV.setText(stnBean.getName());
                }
                //convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
                if(stnBean.getName().trim().length()>1) {
                    holder.stnNameTV.setText(stnBean.getName());
                    String lineStr = "";
                    for(String lineIdS:stnBean.getLineIds().split(",")) {
                        Line line = MRMApp.me().getLine(Integer.parseInt(lineIdS));
                        lineStr = lineStr + ", " + line.getName();//"+line.getColor()+"
                    }
                    holder.stnLineTV.setText(lineStr.substring(2));
                    if(stnBean.favorite) {
                        holder.favStnIV.setImageResource(R.drawable.star_on);
                    } else {
                        holder.favStnIV.setImageResource(R.drawable.star_off);
                    }
                    if(stnBean.nearby){
                        holder.myLocationIV.setImageResource(R.drawable.my_location);
                       // holder.distanceTV.setText(stnBean.distance+" mtrs");//"+line.getColor()+"
                    }
                    holder.stationLL.setOnClickListener(setStnClickListener);
                    holder.stationLL.setTag(stnBean);
                    holder.favStnIV.setOnClickListener(favClickListener);
                    holder.favStnIV.setTag(stnBean);
                }
                return convertView;
            }


            @Override
            public int getPositionForSection(int section) {
                // If there is no item for current section, previous section will be selected
                for (int i = section; i >= 0; i--) {
                    for (int j = 0; j < getCount(); j++) {
                        if (mSections.length()>i && getItem(j).getName().charAt(0) == mSections.charAt(i))
                            return j;
                    }
                }
                return 0;
            }

            @Override
            public int getSectionForPosition(int position) {
                return 0;
            }

            @Override
            public Object[] getSections() {
                String[] sections = new String[mSections.length()];
                for (int i = 0; i < mSections.length(); i++)
                    sections[i] = String.valueOf(mSections.charAt(i));
                return sections;
            }
        }


    @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            try {
                mListener = (OnFragmentInteractionListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }


        /**
         * This interface must be implemented by activities that contain this
         * fragment to allow an interaction in this fragment to be communicated
         * to the activity and potentially other fragments contained in that
         * activity.
         * <p/>
         * See the Android Training lesson <a href=
         * "http://developer.android.com/training/basics/fragments/communicating.html"
         * >Communicating with Other Fragments</a> for more information.
         */
        public interface OnFragmentInteractionListener {
            // TODO: Update argument type and name
            public void setStation(Station Station,int lineId, boolean advSearch);
        }

    private static class LocalSpinnerAdapter extends ArrayAdapter<String> {

        private final Context context;
        private final String[] values;

        public LocalSpinnerAdapter(Context context, String[] mList) {
            super(context, R.layout.v_via_spinner_entry, mList);
            this.context = context;
            this.values = mList;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (v == null) {
                v = inflater.inflate(R.layout.v_line_spinner, parent, false);
            }
            TextView routeInfoTV = (TextView) v.findViewById(R.id.viaRouteTV);
            TextView timeDurationTV = (TextView) v.findViewById(R.id.timeDurationTV);

            String str[] = values[position].split("-");
            routeInfoTV.setText(str[0].trim());
            timeDurationTV.setText(str[1].trim());

            return v;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogUpDownAnimation;
    }
}


