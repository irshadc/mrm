package com.glassboxsoftware.mrmapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.dao.internal.InternalDao;
import com.glassboxsoftware.mrmapp.dao.internal.NotifyMsg;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@linkCallbacks}
 * interface.
 */
public class NotificationListFragment extends DialogFragment {

    private ImageView clearSearchIV, closeSearchIV;

    private ListView mStationLV;
    private List<NotifyMsg> smList;
    private NotificationListAdapter mAdapter;

    private SharedPreferences sharedPref;

    private OnFragmentInteractionListener mListener;

    public static NotificationListFragment newInstance() {
        NotificationListFragment fragment = new NotificationListFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NotificationListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_MRM_WithNavDrawer);

        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_notification_list, container, false);
        assert v != null;

        mStationLV = (ListView) v.findViewById(android.R.id.list);
        smList = new ArrayList<>();

        clearSearchIV = (ImageView) v.findViewById(R.id.clearSearchIV);
        closeSearchIV = (ImageView) v.findViewById(R.id.closeSearchIV);

        mAdapter = new NotificationListAdapter(getActivity(),smList);
        mStationLV.setAdapter(mAdapter);
        mAdapter.updateList(InternalDao.getDao().getNotificationList());

        clearSearchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InternalDao.getDao().clearNotifications();
                dismiss();
                Toast.makeText(MRMApp.me().getApplicationContext(), "Notification cleared...",Toast.LENGTH_LONG).show();
            }
        });


        closeSearchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return v;
    }


    private class NotificationListAdapter extends ArrayAdapter<NotifyMsg> {

        private final Context context;
        private List<NotifyMsg> values = new ArrayList<>();
        private LayoutInflater mInflater;
        private SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_notification_format), Locale.US);

        public NotificationListAdapter(Context context, List<NotifyMsg> mList) {
            super(context, R.layout.v_notification_entry,mList);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.context = context;
            this.values = mList;
        }

        public void updateList(List<NotifyMsg> mList){
            this.values.clear();
            this.values.addAll(mList);
            mAdapter.notifyDataSetChanged();
            if(clearSearchIV!=null && mList.size()>0) {
                clearSearchIV.setVisibility(View.VISIBLE);
            }
        }

        private class ViewHolder {
            public TextView msgDateTV,lineTV, titleTV, descTV;
        }

        private View.OnClickListener setStnClickListener  = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.setNotifyMsg((NotifyMsg) v.getTag());
                dismiss();
            }
        };

        private View.OnClickListener favClickListener  = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                NotifyMsg msgBean = (NotifyMsg) v.getTag();
            }
        };

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final NotifyMsg msgBean = values.get(position);
            ViewHolder holder = new ViewHolder();

           // if (convertView == null) {
//                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.v_notification_entry,  parent, false);
                holder.lineTV = (TextView) convertView.findViewById(R.id.lineTV);
                holder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
                holder.descTV = (TextView) convertView.findViewById(R.id.descTV);
                holder.msgDateTV = (TextView) convertView.findViewById(R.id.msgDateTV);

                //convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
                    holder.lineTV.setText(msgBean.getHeader());
                    holder.titleTV.setText(msgBean.getTitle());
                    holder.descTV.setText(msgBean.getAlert());

                    if(msgBean.getGeneratedOn()!=null) {
                        holder.msgDateTV.setText(sdf.format(new Date(msgBean.getGeneratedOn())));
                    } else {
                        holder.msgDateTV.setVisibility(View.GONE);
                    }

            return convertView;
            }
        }


    @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            try {
                mListener = (OnFragmentInteractionListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;

        }

        /**
         * This interface must be implemented by activities that contain this
         * fragment to allow an interaction in this fragment to be communicated
         * to the activity and potentially other fragments contained in that
         * activity.
         * <p/>
         * See the Android Training lesson <a href=
         * "http://developer.android.com/training/basics/fragments/communicating.html"
         * >Communicating with Other Fragments</a> for more information.
         */
        public interface OnFragmentInteractionListener {
            // TODO: Update argument type and name
            public void setNotifyMsg(NotifyMsg NotifyMsg);
        }
}


