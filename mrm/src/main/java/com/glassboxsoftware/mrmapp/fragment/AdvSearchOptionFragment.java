package com.glassboxsoftware.mrmapp.fragment;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.dao.Station;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AdvSearchOptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdvSearchOptionFragment extends DialogFragment {

    private static final String TAG = "AdvSearchOptionFragment";

    private String towardsStnIds,  fromStnIds;

    private String towardsFilter, fromFilter;

    private int lineId;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RouteFragment.
     */
    // TODO: Rename and change types of parameters
    public static AdvSearchOptionFragment newInstance(String towardsStnIds, String fromStnIds, String towardsFilter, String fromFilter, int lineId) {
        AdvSearchOptionFragment fragment = new AdvSearchOptionFragment();
        Bundle args = new Bundle();
        args.putString("towards", towardsStnIds);
        args.putString("from", fromStnIds);
        args.putString("towardsFilter", towardsFilter);
        args.putString("fromFilter", fromFilter);
        args.putInt("lineId",lineId);
        fragment.setArguments(args);
        return fragment;
    }

    public AdvSearchOptionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_MRM_WithNavDrawer);
        if (getArguments() != null) {
            towardsStnIds = getArguments().getString("towards");
            fromStnIds = getArguments().getString("from");

            towardsFilter = getArguments().getString("towardsFilter");
            fromFilter = getArguments().getString("fromFilter");

            lineId = getArguments().getInt("lineId");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.v_adv_option, container, false);

        final RadioGroup fromRG = (RadioGroup) v.findViewById(R.id.fromRG);
        RadioGroup towardRG = (RadioGroup) v.findViewById(R.id.towardRG);


        CompoundButton.OnCheckedChangeListener checkListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.i("Radio", isChecked + " :" + buttonView.getTag());
                if(isChecked) {
                    String arr[] = buttonView.getTag().toString().split(",");
                    if("T".equals(arr[0])) {
                        towardsFilter = towardsFilter + ","+arr[1];
                    } else {
                        fromFilter = fromFilter + ","+arr[1];
                    }
                } else {
                    String arr[] = buttonView.getTag().toString().split(",");
                    if("T".equals(arr[0])) {
                        towardsFilter = towardsFilter.replace(","+arr[1],"");
                    } else {
                        fromFilter = fromFilter.replace(","+arr[1],"");
                    }
                }
                mListener.updateListFromTo(towardsFilter, fromFilter);
            }
        };

        int i=0;
        for(String idStr : towardsStnIds.split(",")){
            if(idStr.trim().equals("")) {
                continue;
            }
            Station stn = MRMApp.me().getStation(Integer.parseInt(idStr.trim()));
            CheckBox x = new CheckBox(this.getActivity());
            x.setText(stn.getCode()+" - "+stn.getName());
            x.setTextColor(getResources().getColor(R.color.text_black_90));
            x.setChecked(towardsFilter!=null && !towardsFilter.trim().equals("") && towardsFilter.indexOf(","+idStr)>=0);
            x.setTag("T,"+idStr);
            x.setOnCheckedChangeListener(checkListener);
            towardRG.addView(x,i++);
        }
        i = 0;

        for(String idStr : fromStnIds.split(",")){
            if(idStr.trim().equals("")) {
                continue;
            }
            Station stn = MRMApp.me().getStation(Integer.parseInt(idStr.trim()));
            CheckBox x = new CheckBox(this.getActivity());
            x.setText(stn.getCode()+" - "+stn.getName());
            x.setTextColor(getResources().getColor(R.color.text_black_90));
            x.setChecked(fromFilter!=null && !fromFilter.trim().equals("") && fromFilter.indexOf(","+idStr)>=0);
            x.setTag("F,"+idStr);
            x.setOnCheckedChangeListener(checkListener);
            fromRG.addView(x,i++);
        }

        v.findViewById(R.id.preferenceFAB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        v.findViewById(R.id.dismissV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Toast.makeText(getActivity(),"Filter your train list with Towards & From.",Toast.LENGTH_LONG).show();
        return v;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = getDialog().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            float[] hsv = new float[3];
            int color = MRMApp.me().lineColorMap[lineId-1];
            Color.colorToHSV(color, hsv);
            hsv[2] *= 0.8f; // value component
            color = Color.HSVToColor(hsv);

            getDialog().getWindow().setStatusBarColor(color);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void updateListFromTo(String towards, String from);
    }

}
