package com.glassboxsoftware.mrmapp.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.RouteScheduleBean;
import com.glassboxsoftware.mrmapp.bean.TrainInfoBean;
import com.glassboxsoftware.mrmapp.ui.adapter.TrainRVAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link com.glassboxsoftware.mrmapp.fragment.TrainListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrainListFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    private OnTrainListFragmentListener mListener;


    protected RecyclerView trainRV;
    protected TrainRVAdapter mTrainRVAdapter;
    protected RecyclerView.LayoutManager mTrainLayoutManager;
    private RouteScheduleBean mRsb;
    private List<TrainInfoBean> mTibList = new ArrayList<>();
    private int currentIndex = 0;
    private int cardPosition;

    public void updateCard(TrainInfoBean tib) {
        dismiss();
        mListener.updateResultCard(tib,cardPosition);
    }


    public interface OnTrainListFragmentListener {

        public void updateResultCard(TrainInfoBean tib, int cardPosition);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RouteFragment.
     */
    // TODO: Rename and change types of parameters
    public static TrainListFragment newInstance(RouteFragment routeFragment, RouteScheduleBean rsb, int position) {
        TrainListFragment fragment = new TrainListFragment();
        fragment.mRsb = rsb;
        fragment.mTibList.add(rsb.trainInfoBean);
        fragment.cardPosition = position;
        fragment.mListener = routeFragment;
        return fragment;
    }

    public TrainListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_MRM_WithNavDrawer);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.v_route_train_list, container, false);
        ViewHolder vh = new ViewHolder(v);

        final int lineColor = getResources().getColor(MRMApp.lineColorMap[mRsb.line.getId().intValue()-1]);
        if(mRsb!=null && mRsb.stbMap.size()>1) {

            vh.stnNameTV.setText(mRsb.stbMap.get(mRsb.trainInfoBean.startStnId).stationName+" → "+mRsb.stbMap.get(mRsb.trainInfoBean.endStnId).stationName);

            trainRV = (RecyclerView) v.findViewById(R.id.stnTimeRV);
            mTrainLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
            trainRV.setLayoutManager(mTrainLayoutManager);

            trainRV.setHasFixedSize(true);
            mTrainRVAdapter = new TrainRVAdapter(mTibList, 0, mRsb.line.getId().intValue(),getActivity(),this);

            // Set CustomAdapter as the adapter for RecyclerView.
            trainRV.setAdapter(mTrainRVAdapter);

            TrainListTask tlt = new TrainListTask();
            tlt.execute(mRsb);
        }

        vh.lineNameTV.setText(mRsb.line.getName());

        vh.headerRL.setBackgroundColor(lineColor);

        vh.closePopupIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        vh.closeV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        v.setTag(vh);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogLeftRightAnimation;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
        mRsb = null;
    }

    private class TrainListTask extends AsyncTask<RouteScheduleBean, Void, List<TrainInfoBean>> {

        @Override
        protected List<TrainInfoBean> doInBackground(RouteScheduleBean... rsb) {

            if (rsb!=null && rsb.length!=0) {

                int fromStnId = rsb[0].trainInfoBean.startStnId;
                int toStnId = rsb[0].trainInfoBean.endStnId;
                int lineId = rsb[0].line.getId().intValue();
                int trainId = rsb[0].trainInfoBean.trainId;


                List<TrainInfoBean> tibList = new ArrayList<>();

                String tableName = "train_schedule_details";
                switch (lineId) {
                    case 1: tableName = "tsd_l1"; break;
                    case 2: tableName = "tsd_l2"; break;
                    case 3: tableName = "tsd_l34"; break;
                    case 4: tableName = "tsd_l34"; break;
                    case 5: tableName = "tsd_l56"; break;
                    case 6: tableName = "tsd_l56"; break;
                    case 7: tableName = "tsd_l7"; break;
                    case 8: tableName = "tsd_l8"; break;
                }

                String queryText =  " SELECT T.TRAIN_ID, T.START_STN_ID,T.TIME_IN_MINUTES, T.END_STN_ID,D.TIME_IN_MINUTES, T.NO_OF_CAR, T.SPEED, T.SPECIAL_INFO, T.SUNDAY_ONLY, T.HOLIDAY_ONLY, T.NOT_ON_SUNDAY, T.PLATFORM_NO, T.PLATFORM_SIDE " +
                        " FROM "+tableName+" T, "+tableName+" D " +
                        " where T.STATION_ID = " +fromStnId +
                        " and D.STATION_ID = " + toStnId +
                        " and T.LINE_ID = D.LINE_ID " +
                        " and T.LINE_ID = " + lineId +
                        " and D.TIME_IN_MINUTES > T.TIME_IN_MINUTES " +
                        " and T.TRAIN_ID = D.TRAIN_ID " +
                        " order by T.TIME_IN_MINUTES ";

                Cursor cursor = MRMApp.me().getDaoSession().getDatabase().rawQueryWithFactory(null,queryText,null,null);
                int i=0;
                if(cursor!=null) {
                    cursor.moveToFirst();
                    while(!cursor.isAfterLast() && !cursor.isClosed()) {
                        TrainInfoBean tib = new TrainInfoBean();
                        tib.trainId = cursor.getInt(0);
                        if(tib.trainId==trainId) {
                            currentIndex = i;
                        }
                        tib.firstStnName = MRMApp.me().getStation(cursor.getInt(1)).getName();
                        tib.startStnTime = cursor.getInt(2);
                        tib.lastStnCode = MRMApp.me().getStation(cursor.getInt(3)).getCode();
                        tib.lastStnName = MRMApp.me().getStation(cursor.getInt(3)).getName();
                        tib.endStnTime = cursor.getInt(4);
                        tib.noOfCar = cursor.getInt(5);
                        if (cursor.getString(6) != null && !cursor.getString(6).equalsIgnoreCase("S")) {
                            tib.speed = "Fast";
                        } else {
                            tib.speed = "Slow";
                        }
                        tib.specialInfo = cursor.getString(7);
                        tib.sundayOnly = cursor.getInt(8) == 1 ? true : false;
                        tib.holidayOnly = cursor.getInt(9) == 1 ? true : false;
                        tib.notOnSunday = cursor.getInt(10) == 1 ? true : false;
                        tib.platformNo = cursor.getString(11);
                        tib.platformSide = cursor.getString(12);
                        tibList.add(tib);
                        cursor.moveToNext();
                        i++;
                    }
                    cursor.close();
                }
                return tibList;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<TrainInfoBean> tibList) {
            super.onPostExecute(tibList);
            mTrainRVAdapter.updateList(tibList,currentIndex);
            if(currentIndex>15) {
                trainRV.scrollToPosition(currentIndex-15);
            }
            if(currentIndex<(tibList.size()+3)) {
                trainRV.smoothScrollToPosition(currentIndex+2);
            } else {
                trainRV.smoothScrollToPosition(currentIndex);
            }
        }
    }

    public final static class ViewHolder {

        public final TextView lineNameTV;
        public final TextView stnNameTV;

        public final RelativeLayout headerRL;

        public final ImageView closePopupIV;
        public final ImageView lineIV;

        public final View closeV;

        public ViewHolder(View v) {
            headerRL = (RelativeLayout) v.findViewById(R.id.headerRL);

            lineNameTV = (TextView) v.findViewById(R.id.lineNameTV);
            stnNameTV = (TextView) v.findViewById(R.id.trainStatusTV);

            closePopupIV = (ImageView) v.findViewById(R.id.closePopupIV);
            lineIV = (ImageView) v.findViewById(R.id.lineIV);

            closeV = (View) v.findViewById(R.id.closeV);
        }
    }
}
