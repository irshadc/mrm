package com.glassboxsoftware.mrmapp.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.bean.RouteChoiceBean;
import com.glassboxsoftware.mrmapp.dao.Station;
import com.glassboxsoftware.mrmapp.dao.StationRouteMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irshad Chohan on 1/21/2015.
 */
public class RouteChoiceLoader extends AsyncTaskLoader<List<RouteChoiceBean>> {

    private static final String TAG = "RouteChoiceLoader";
    List<RouteChoiceBean> mApps;

    private Context mContext;

    public int srcId, dstId;

    public RouteChoiceLoader(Context context, int srcId, int dstId) {
        super(context);
        mContext = context;
        this.srcId = srcId;
        this.dstId = dstId;
        Log.i(TAG,"param: "+srcId+";"+dstId);
    }

    /**
     * This is where the bulk of our work is done.  This function is
     * called in a background thread and should generate a new set of
     * data to be published by the loader.
     */
    @Override
    public List<RouteChoiceBean> loadInBackground() {
        List<RouteChoiceBean> rcbList = new ArrayList<>();

        for (StationRouteMapping srm : MRMApp.me().getDaoSession().getStationRouteMappingDao().queryRaw(" WHERE T.SOURCE_STATION_ID = "+srcId+" AND T.DEST_STATION_ID = "+dstId+" ORDER BY T.PRIORITY LIMIT 8 ")) {

            RouteChoiceBean rcb = new RouteChoiceBean();
            rcb.stnIdList = srm.getRoute();
            rcb.lineIdList = srm.getLines();
            rcb.stnNameList = "Direct Train → ";

            if (rcb.stnIdList != null && rcb.stnIdList.trim().length() > 0) {
                int noOfStops = 0;
                for (String stnIdStr : rcb.stnIdList.trim().split(MRMApp.OR_SEARCH)) {

                    if (stnIdStr.trim().length() == 0 || stnIdStr.equals("") || stnIdStr.equals("0")) {
                        continue;
                    }
                    Station st = MRMApp.me().getStation(Integer.parseInt(stnIdStr));
                    if (rcb.stnNameList.equals("Direct Train → ")) {
                        rcb.stnNameList = "";
                    }
                    rcb.stnNameList += st.getName() + " → ";
                    noOfStops++;
                }
                int timeTaken = 0;
                for (String timeStr : srm.getTime().trim().split(MRMApp.OR_SEARCH)) {
                    try {
                        timeTaken += Integer.parseInt(timeStr.trim()) + 3;  //boarding time
                    } catch (Exception e) {
                    }
                }
                rcb.stnNameList = rcb.stnNameList.substring(0, rcb.stnNameList.length() - 3);
                rcb.timeOfJourney = timeTaken;
                rcb.noOfStops = noOfStops;
            }
            rcbList.add(rcb);
        }
        return rcbList;
    }

    /**
     * Called when there is new data to deliver to the client.  The
     * super class will take care of delivering it; the implementation
     * here just adds a little more logic.
     */
    @Override
    public void deliverResult(List<RouteChoiceBean> apps) {
        if (isReset()) {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if (apps != null) {
                onReleaseResources(apps);
            }
        }
        List<RouteChoiceBean> oldApps = mApps;
        mApps = apps;

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(apps);
        }

        // At this point we can release the resources associated with
        // 'oldApps' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if (oldApps != null) {
            onReleaseResources(oldApps);
        }
    }


    /**
     * Handles a request to start the Loader.
     */
    @Override
    protected void onStartLoading() {

        if (mApps != null) {
            // If we currently have a result available, deliver it
            // immediately.
            deliverResult(mApps);
        }
        if (takeContentChanged() || mApps == null) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }

    /**
     * Handles a request to stop the Loader.
     */
    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(List<RouteChoiceBean> apps) {
        super.onCanceled(apps);

        // At this point we can release the resources associated with 'apps'
        // if needed.
        onReleaseResources(apps);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset() {
        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        // At this point we can release the resources associated with 'apps'
        // if needed.
        if (mApps != null) {
            onReleaseResources(mApps);
            mApps = null;
        }
    }

    /**
     * Helper function to take care of releasing resources associated
     * with an actively loaded data set.
     */
    protected void onReleaseResources(List<RouteChoiceBean> apps) {
        // For a simple List<> there is nothing to do.  For something
        // like a Cursor, we would close it here.
    }


}