package com.glassboxsoftware.mrmapp.loader;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.bean.AdvSearchConfig;
import com.glassboxsoftware.mrmapp.bean.AdvSearchParam;
import com.glassboxsoftware.mrmapp.bean.TrainInfoBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Irshad Chohan on 1/21/2015.
 */
public class TrainListLoader extends AsyncTaskLoader<List<TrainInfoBean>> {

    private static final String TAG = "TrainListLoader";
    List<TrainInfoBean> mApps;

    private Context mContext;

    private AdvSearchParam param = new AdvSearchParam();
    public int currentIndex;

    public TrainListLoader(Context context, AdvSearchParam param) {
        super(context);
        mContext = context;
        if(param!=null) {
            this.param = param;
            Log.i(TAG,"param: "+param.stnId+";"+param.lineId+param.direction+param.timeNow);
        }
    }

    /**
     * This is where the bulk of our work is done.  This function is
     * called in a background thread and should generate a new set of
     * data to be published by the loader.
     */
    @Override
    public List<TrainInfoBean> loadInBackground() {

        if (param.stnId!=0 && param.lineId!=0) {

            List<TrainInfoBean> tibList = new ArrayList<>();

            String tableName = "train_schedule_details";
            switch (param.lineId) {
                case 1: tableName = "tsd_l1"; break;
                case 2: tableName = "tsd_l2"; break;
                case 3: tableName = "tsd_l34"; break;
                case 4: tableName = "tsd_l34"; break;
                case 5: tableName = "tsd_l56"; break;
                case 6: tableName = "tsd_l56"; break;
                case 7: tableName = "tsd_l7"; break;
                case 8: tableName = "tsd_l8"; break;
            }
            String queryText = " SELECT T.TRAIN_ID, T.START_STN_ID,T.TIME_IN_MINUTES, T.END_STN_ID,0, T.NO_OF_CAR, T.SPEED, T.SPECIAL_INFO, T.SUNDAY_ONLY, T.HOLIDAY_ONLY, T.NOT_ON_SUNDAY, T.PLATFORM_NO, T.PLATFORM_SIDE " +
                    " FROM " + tableName + " T " +
                    " where T.STATION_ID = " + param.stnId +
                    " and T.DIRECTION = '" + param.direction + "'" +
                    " and T.End_STN_ID <> " + param.stnId +
                    " and T.LINE_ID = " + param.lineId;

            if(param.fromFilter!=null && param.fromFilter.trim().length()>1) {
                queryText = queryText +" and T.START_STN_ID IN (0" + param.fromFilter + ") ";
            }
            if(param.towardsFilter!=null && param.towardsFilter.trim().length()>1) {
                queryText = queryText +" and T.END_STN_ID IN (0" + param.towardsFilter + ") ";
            }
            if(param.speed=='F' || param.speed=='S') {
                queryText = queryText +" and T.SPEED = '" + param.speed + "'";
            }
            queryText = queryText + " order by T.TIME_IN_MINUTES ";

            Cursor cursor = MRMApp.me().getDaoSession().getDatabase().rawQueryWithFactory(null,queryText,null,null);
            int i=0;
            currentIndex = 0;
            if(cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();
                while(!cursor.isAfterLast() && !cursor.isClosed()) {
                    TrainInfoBean tib = new TrainInfoBean();
                    tib.trainId = cursor.getInt(0);
                    tib.firstStnName = MRMApp.me().getStation(cursor.getInt(1)).getName();
                    tib.startStnTime = cursor.getInt(2);
                    if(currentIndex == 0 && tib.startStnTime > param.timeNow) {
                        currentIndex = i;
                    }
                    tib.lastStnCode = MRMApp.me().getStation(cursor.getInt(3)).getCode();
                    tib.lastStnName = MRMApp.me().getStation(cursor.getInt(3)).getName();
                    tib.endStnTime = cursor.getInt(4);
                    tib.noOfCar = cursor.getInt(5);
                    if (cursor.getString(6) != null && !cursor.getString(6).equalsIgnoreCase("S")) {
                        tib.speed = "Fast";
                    } else {
                        tib.speed = "Slow";
                    }
                    tib.specialInfo = cursor.getString(7);
                    tib.sundayOnly = cursor.getInt(8) == 1 ? true : false;
                    tib.holidayOnly = cursor.getInt(9) == 1 ? true : false;
                    tib.notOnSunday = cursor.getInt(10) == 1 ? true : false;
                    tib.platformNo = cursor.getString(11);
                    tib.platformSide = cursor.getString(12);
                    tibList.add(tib);
                    cursor.moveToNext();
                    i++;
                }
            }
            if(cursor!=null) cursor.close();
            return tibList;
        }
        return null;
    }

    /**
     * Called when there is new data to deliver to the client.  The
     * super class will take care of delivering it; the implementation
     * here just adds a little more logic.
     */
    @Override
    public void deliverResult(List<TrainInfoBean> apps) {
        if (isReset()) {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if (apps != null) {
                onReleaseResources(apps);
            }
        }
        List<TrainInfoBean> oldApps = mApps;
        mApps = apps;

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(apps);
        }

        // At this point we can release the resources associated with
        // 'oldApps' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if (oldApps != null) {
            onReleaseResources(oldApps);
        }
    }


    /**
     * Handles a request to start the Loader.
     */
    @Override
    protected void onStartLoading() {

        if (mApps != null) {
            // If we currently have a result available, deliver it
            // immediately.
            deliverResult(mApps);
        }
        if (takeContentChanged() || mApps == null) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }

    /**
     * Handles a request to stop the Loader.
     */
    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(List<TrainInfoBean> apps) {
        super.onCanceled(apps);

        // At this point we can release the resources associated with 'apps'
        // if needed.
        onReleaseResources(apps);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset() {
        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        // At this point we can release the resources associated with 'apps'
        // if needed.
        if (mApps != null) {
            onReleaseResources(mApps);
            mApps = null;
        }
    }

    /**
     * Helper function to take care of releasing resources associated
     * with an actively loaded data set.
     */
    protected void onReleaseResources(List<TrainInfoBean> apps) {
        // For a simple List<> there is nothing to do.  For something
        // like a Cursor, we would close it here.
    }
}