package com.glassboxsoftware.mrmapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;

import com.glassboxsoftware.mrmapp.dao.DaoMaster;
import com.glassboxsoftware.mrmapp.dao.DaoSession;
import com.glassboxsoftware.mrmapp.dao.Line;
import com.glassboxsoftware.mrmapp.dao.LineDraw;
import com.glassboxsoftware.mrmapp.dao.Station;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Irshad Chohan on 5/30/2014.
 */
public class MRMApp extends Application {

    private static final String TAG = "MRMApp";
    private SQLiteDatabase db;
    private DaoMaster daoMaster;
    private DaoSession daoSession;

    public List<Station> masterStationList = new ArrayList<>();
    public List<LineDraw> lineDrawList = new ArrayList<>();

    private SparseArray<Station> masterStationMap = new SparseArray<>();
    private Map<Long,Line> masterLineMap = new HashMap<>();
    private Map<String,LineDraw> lineDrawMap = new HashMap<>();

    private static MRMApp mrm;
    public String regid = "";

    public static Typeface robotoMedium, normalTF;

    public static boolean loadingDone = false;

    public static final int[] lineColorMap = {
           R.color.wr_main,
            R.color.cr_main,
            R.color.hr_main,
            R.color.th_main,
            R.color.ind_rail,
            R.color.ind_rail,
            R.color.mono_main,
            R.color.metro_main
    };

    public static final String SEPARATOR = ";";
    public static final String OR_SEPARATOR = "|";
    public static final String OR_SEARCH = "\\|";

    public static int versionCode = 53;
    public static String versionStr = "";


    public static MRMApp me() {
        if(mrm == null) {
            mrm = new MRMApp();
        }
        return mrm;
    }

    public MRMApp(){
        mrm = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            versionStr = getPackageManager().getPackageInfo(getPackageName(),0).versionName;
        } catch (android.content.pm.PackageManager.NameNotFoundException ne) {
            Log.e(TAG,ne.getMessage());
        }
        Parse.initialize(this, "oIltp8ZP8uE1dAlmlRiHGe0yvTFiJSQyUX0c1U5j", "1gJrrgNcjYDsaqSSGEGtuB0LvkldrvEslfwfGeMG");
        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParsePush.subscribeInBackground("android", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");

                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });

        InitAppTask iat = new InitAppTask();
        iat.mContext = this;
        mrm = this;
        iat.execute();
    }



    public Station getStation(int id) {
        return masterStationMap.get(id);
    }

    public LineDraw getLineDraw(Long fromStnId, Long toStnId, Long lineId) {
        return lineDrawMap.get(fromStnId+":"+toStnId+":"+lineId);
    }

    public Line getLine(int id) {
        return masterLineMap.get(new Long(id));
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    private class InitAppTask extends AsyncTask<String, Void, String> {

        private static final String DB_NAME="mrmv3.png", TAG="MRMApp";
        public Context mContext;

        public InitAppTask() {
        }
        @Override
        protected String doInBackground(String... params) {

            //SQLiteDatabase.loadLibs(mContext);
            DaoMaster.MyDatabase helper = new DaoMaster.MyDatabase(mContext,mContext.getPackageName().substring(4,12),versionCode);
            db = helper.getReadableDatabase();
            daoMaster = new DaoMaster(db,versionCode);
            daoSession = daoMaster.newSession();

            List<Line> mLineList = daoSession.getLineDao().loadAll();
            for (Line l : mLineList) {
                masterLineMap.put(l.getId(), l);
            }
            List<Station> stationList = daoSession.getStationDao().queryRaw(" WHERE _ID < 143 ORDER BY T.NAME ASC ");

            for (Station sm : stationList) {
                masterStationList.add(sm);
                masterStationMap.put(sm.getId().intValue(), sm);
            }
            lineDrawList = daoSession.getLineDrawDao().loadAll();
            for (LineDraw ld : lineDrawList) {
                lineDrawMap.put(ld.getFromStnId() + ":" + ld.getToStnId() + ":" + ld.getLineId(), ld);
            }
            Log.d("MRM-Cache", "All Done !!!");
            return "SUCCESS";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if("SUCCESS".equals(s)) {
                loadingDone = true;
            }
        }
    }

    public static void setDefault(String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MRMApp.me());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDefault(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MRMApp.me());
        return preferences.getString(key, "");
    }

    public static String getTimeInFormat(long timeInM) {
        String time = "";
        long hr =timeInM/60;
        if(hr>24) {
            hr -=24;
        }
        if(hr > 12) {
            hr = hr-12;

            if(hr<10) {
                time = "0";
            }

            time += hr+ ((timeInM % 60) < 10 ? ":0"+(timeInM % 60) : ":"+(timeInM % 60));
        } else {
            if(hr<10) {
                time = "0";
            }
            time += hr+ ((timeInM % 60) < 10 ? ":0"+(timeInM % 60) : ":"+(timeInM % 60));
        }
        return time;
    }

    public static String getTimeInFormatPM(long timeInM) {
        String time = "";
        long hr =timeInM/60;
        if(hr>24) {
            hr -=24;
        }
        if(hr > 12) {
            hr = hr-12;

            if(hr<10) {
                time = "0";
            }
            if(hr==12) {
                time += hr+ ((timeInM % 60) < 10 ? ":0"+(timeInM % 60) : ":"+(timeInM % 60))+" AM";
            } else {
                time += hr+ ((timeInM % 60) < 10 ? ":0"+(timeInM % 60) : ":"+(timeInM % 60))+" PM";
            }
        } else {
            if(hr<10) {
                time = "0";
            }
            if(hr==12) {
                time += hr+ ((timeInM % 60) < 10 ? ":0"+(timeInM % 60) : ":"+(timeInM % 60))+" PM";
            } else {
                time += hr+ ((timeInM % 60) < 10 ? ":0"+(timeInM % 60) : ":"+(timeInM % 60))+" AM";
            }
        }
        return time;
    }

    public void updateDb(String msg) {
        if(msg!=null && msg.indexOf("query")>1) {
            try {
                String sql = "";
                db.execSQL(sql);
            }catch (Exception e) {}
        }
    }
}
