package com.glassboxsoftware.mrmapp.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.AdvSearchConfig;
import com.glassboxsoftware.mrmapp.bean.AdvSearchParam;
import com.glassboxsoftware.mrmapp.bean.TrainInfoBean;
import com.glassboxsoftware.mrmapp.dao.Station;
import com.glassboxsoftware.mrmapp.fragment.AdvSearchOptionFragment;
import com.glassboxsoftware.mrmapp.fragment.StationListFragment;
import com.glassboxsoftware.mrmapp.loader.TrainListLoader;
import com.glassboxsoftware.mrmapp.ui.adapter.TrainRVAdapter;
import com.glassboxsoftware.mrmapp.ui.listener.HidingScrollListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class AdvSearchActivity extends AppCompatActivity implements
        StationListFragment.OnFragmentInteractionListener,
AdvSearchOptionFragment.OnFragmentInteractionListener,
        LoaderManager.LoaderCallbacks {

    private static final String TAG = "AdvSearchActivity";
    private TextView searchET, lineTV, towardTV, upTowardCodeTV, fromTV, downTowardCodeTV;

    private RelativeLayout upSideRL, downSideRL;
    private LinearLayout headerOptionLL;

    private CheckBox fastCB, slowCB;

    protected RecyclerView trainRV;
    protected TrainRVAdapter mTrainRVAdapter;
    protected RecyclerView.LayoutManager mTrainLayoutManager;

    private List<TrainInfoBean> mTibList = new ArrayList<>();

    private int cardPosition;

    private AdvSearchParam advParam;

    private Location mLocation;
    private AdvSearchConfig advConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the message from the intent
        advParam = new AdvSearchParam();
        Intent intent = getIntent();
        advParam.stnId = intent.getIntExtra("stnId", 0);
        advParam.lineId = intent.getIntExtra("lineId", 0);

        Calendar cal = Calendar.getInstance();
        advParam.direction = (cal.get(Calendar.HOUR_OF_DAY)>=12) ? 'D': 'U';

        advParam.speed = 'B';
        mLocation = intent.getParcelableExtra("CURR_LOCATION");

        setContentView(R.layout.a_adv_search);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final int lineColor = getResources().getColor(MRMApp.lineColorMap[advParam.lineId - 1]);

            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            float[] hsv = new float[3];
            int color = lineColor;
            Color.colorToHSV(color, hsv);
            hsv[2] *= 0.8f; // value component
            color = Color.HSVToColor(hsv);

            window.setStatusBarColor(color);
        }

        ImageView closeSearchIV = (ImageView) findViewById(R.id.closeSearchIV);

        closeSearchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchET = (TextView) findViewById(R.id.searchET);

        towardTV = (TextView) findViewById(R.id.towardTV);
        upTowardCodeTV = (TextView) findViewById(R.id.upTowardCodeTV);
        fromTV = (TextView) findViewById(R.id.fromTV);
        downTowardCodeTV = (TextView) findViewById(R.id.downTowardCodeTV);

        upSideRL = (RelativeLayout) findViewById(R.id.upSideRL);
        downSideRL = (RelativeLayout) findViewById(R.id.downSideRL);

        headerOptionLL = (LinearLayout) findViewById(R.id.headerOptionLL);


        searchET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("station_list");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                // Create and show the dialog.
                DialogFragment erFragment = StationListFragment.newInstance(0, true, mLocation);
                erFragment.show(ft, "station_list");
            }
        });

        lineTV = (TextView) findViewById(R.id.lineTV);
        lineTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("station_list");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                // Create and show the dialog.
                DialogFragment erFragment = StationListFragment.newInstance(0, true, mLocation);
                erFragment.show(ft, "station_list");
            }
        });

        findViewById(R.id.upSideRL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(advParam.direction=='U') {
                    showAdvOption();
                } else {
                    advParam.direction = 'U';
                    advParam.towardsFilter = advConfig.upStnIds;
                    advParam.fromFilter = advConfig.upFromStnIds;
                    updateList(true);
                    findViewById(R.id.downSideRL).setBackgroundResource(R.color.body_text_160);
                    findViewById(R.id.upSideRL).setBackgroundResource(R.drawable.card_white_top);
                }
            }
        });

        findViewById(R.id.downSideRL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(advParam.direction=='D') {
                    showAdvOption();
                } else {
                    advParam.direction = 'D';
                    advParam.towardsFilter = advConfig.downStnIds;
                    advParam.fromFilter = advConfig.downFromStnIds;
                    updateList(true);
                    findViewById(R.id.upSideRL).setBackgroundResource(R.color.body_text_160);
                    findViewById(R.id.downSideRL).setBackgroundResource(R.drawable.card_white_top);
                }
            }
        });


        trainRV = (RecyclerView) findViewById(R.id.stnTimeRV);
        mTrainLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        trainRV.setLayoutManager(mTrainLayoutManager);

        trainRV.setHasFixedSize(true);
        mTrainRVAdapter = new TrainRVAdapter(mTibList, advParam.stnId, advParam.lineId, this, null);

        // Set CustomAdapter as the adapter for RecyclerView.
        trainRV.setAdapter(mTrainRVAdapter);

        setupConfigDisplay(MRMApp.me().getStation(advParam.stnId), advParam.lineId, false);


        findViewById(R.id.preferenceFAB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAdvOption();
            }
        });

        fastCB = (CheckBox) findViewById(R.id.fastCB);
        slowCB = (CheckBox) findViewById(R.id.slowCB);


        CompoundButton.OnCheckedChangeListener speedListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (slowCB.isChecked() && !fastCB.isChecked()) {
                    advParam.speed = 'S';
                } else if (fastCB.isChecked()) {
                    advParam.speed = 'F';
                } else {
                    advParam.speed = 'B';
                }
                updateList(true);
            }
        };

        fastCB.setOnCheckedChangeListener(speedListener);
        slowCB.setOnCheckedChangeListener(speedListener);

        //setting up our OnScrollListener
        trainRV.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideTopViews();
            }

            @Override
            public void onShow() {
                showTopViews();
            }
        });

        if(advParam.direction=='D') {
            findViewById(R.id.upSideRL).setBackgroundResource(R.color.body_text_160);
            findViewById(R.id.downSideRL).setBackgroundResource(R.drawable.card_white_top);
        }
        Toast.makeText(this, "Tap on Trains for complete list of Station's halt.", Toast.LENGTH_LONG).show();
    }

    private void hideTopViews() {
        if(mTrainRVAdapter.getItemCount()<=4) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            headerOptionLL.animate().translationY(-headerOptionLL.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
        } else {
            headerOptionLL.setVisibility(View.GONE);
        }
    }

    private void showTopViews() {
        if(mTrainRVAdapter.getItemCount()<=4) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            headerOptionLL.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
        } else {
            headerOptionLL.setVisibility(View.VISIBLE);
        }
    }


    public void showAdvOption() {
        if(advConfig!=null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("adv_option");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            if(advParam!=null && advParam.direction=='D') {
                AdvSearchOptionFragment erFragment = AdvSearchOptionFragment.newInstance(advConfig.downStnIds, advConfig.downFromStnIds, advParam.towardsFilter, advParam.fromFilter, advParam.lineId);
                erFragment.show(getSupportFragmentManager(), "adv_option");
            } else {
                AdvSearchOptionFragment erFragment = AdvSearchOptionFragment.newInstance(advConfig.upStnIds, advConfig.upFromStnIds, advParam.towardsFilter, advParam.fromFilter, advParam.lineId);
                erFragment.show(getSupportFragmentManager(), "adv_option");
            }
        }
    }


    private void setupConfigDisplay(Station stn,int lineId, final boolean again) {

        advParam.stnId = stn.getId().intValue();
        advParam.lineId = lineId;

        searchET.setText(stn.getName());
        lineTV.setText(MRMApp.me().getLine(advParam.lineId).getName());

        final int lineColor = getResources().getColor(MRMApp.lineColorMap[advParam.lineId-1]);
        findViewById(R.id.search_bar).setBackgroundColor(lineColor);

        new AsyncTask<Void, Void, AdvSearchConfig>() {
            @Override
            protected AdvSearchConfig doInBackground(Void... params) {
                AdvSearchConfig result = new AdvSearchConfig();
                List<TrainInfoBean> tibList = new ArrayList<>();

                String tableName = "train_schedule_details";
                switch (advParam.lineId) {
                    case 1: tableName = "tsd_l1"; break;
                    case 2: tableName = "tsd_l2"; break;
                    case 3: tableName = "tsd_l34"; break;
                    case 4: tableName = "tsd_l34"; break;
                    case 5: tableName = "tsd_l56"; break;
                    case 6: tableName = "tsd_l56"; break;
                    case 7: tableName = "tsd_l7"; break;
                    case 8: tableName = "tsd_l8"; break;
                }
                String queryText = " SELECT T.END_STN_ID, T.START_STN_ID, T.NO_OF_CAR, T.SPEED, T.PLATFORM_NO, T.DIRECTION " +
                        " FROM " + tableName + " T " +
                        " where T.STATION_ID = " + advParam.stnId +
                        " and T.END_STN_ID <> " + advParam.stnId +
                        " and T.LINE_ID = " + advParam.lineId +
                        " order by  T.END_STN_ID, T.NO_OF_CAR, T.SPEED, T.PLATFORM_NO, T.DIRECTION ";

                Cursor cursor = MRMApp.me().getDaoSession().getDatabase().rawQueryWithFactory(null,queryText,null,null);

                if(cursor!=null && cursor.getCount()>0) {
                    cursor.moveToFirst();
                    while(!cursor.isAfterLast() && !cursor.isClosed()) {
                        int endStnId = cursor.getInt(0);
                        int fromStnId = cursor.getInt(1);
                        String noOfCAr =  cursor.getString(2);
                        String spd = cursor.getString(3);
                        String pno = cursor.getString(4);
                        String dir = cursor.getString(5);

                        if(result.directionOpt.indexOf(dir)<0) {
                            result.directionOpt +=","+dir;
                        }
                        if(result.speedOpt.indexOf(spd)<0) {
                            result.speedOpt +=","+spd;
                        }
                        if(result.platformOpt.indexOf(pno)<0) {
                            result.platformOpt +=","+pno;
                        }
                        if(result.carOpt.indexOf(noOfCAr)<0) {
                            result.carOpt +=","+noOfCAr;
                        }

                        if("U".equalsIgnoreCase(dir)) {
                            if(result.upStnIds.indexOf(""+endStnId)<0) {
                                result.upStnIds +=","+endStnId;
                                result.upStnCode+="  "+MRMApp.me().getStation(endStnId).getCode();
                                if(result.upStnName.length()==0) {
                                    result.upStnName = MRMApp.me().getStation(endStnId).getName();
                                }
                            }
                            if(result.upFromStnIds.indexOf(""+fromStnId)<0) {
                                result.upFromStnIds +=","+fromStnId;
                            }
                        } else if("D".equalsIgnoreCase(dir)) {
                            if(result.downStnIds.indexOf(""+endStnId)<0) {
                                result.downStnIds +=","+endStnId;
                                result.downStnCode+="  "+MRMApp.me().getStation(endStnId).getCode();
                                if(result.downStnName.length()==0) {
                                    result.downStnName = MRMApp.me().getStation(endStnId).getName();
                                }
                            }
                            if(result.downFromStnIds.indexOf(""+fromStnId)<0) {
                                result.downFromStnIds +=","+fromStnId;
                            }
                        }
                        cursor.moveToNext();
                    }
                }
                if(cursor!=null) cursor.close();
                return result;
            }

            @Override
            protected void onPostExecute(AdvSearchConfig config) {
                advConfig = config;

                towardTV.setText(advConfig.upStnName);
                upTowardCodeTV.setText(advConfig.upStnCode.trim());
                fromTV.setText(advConfig.downStnName);
                downTowardCodeTV.setText(advConfig.downStnCode.trim());

                if(advConfig.directionOpt.indexOf("U")<0) {
                    upSideRL.setVisibility(View.GONE);
                    downSideRL.setBackgroundResource(R.drawable.card_white_top);
                    advParam.direction = 'D';
                }
                if(advConfig.directionOpt.indexOf("D")<0) {
                    downSideRL.setVisibility(View.GONE);
                    upSideRL.setBackgroundResource(R.drawable.card_white_top);
                    advParam.direction = 'U';
                }
                if(advConfig.speedOpt.indexOf("F")<0) {
                    fastCB.setEnabled(false);
                    slowCB.setEnabled(false);
                }

                if(advParam.direction == 'U') {
                    advParam.towardsFilter = advConfig.upStnIds;
                    advParam.fromFilter = advConfig.upFromStnIds;
                } else {
                    advParam.towardsFilter = advConfig.downStnIds;
                    advParam.fromFilter = advConfig.downFromStnIds;
                }
                updateList(again);
            }
        }.execute(null, null, null);
    }

    private void updateList(boolean again) {
        Calendar cal = Calendar.getInstance();
        advParam.timeNow = 60*cal.get(Calendar.HOUR_OF_DAY)+cal.get(Calendar.MINUTE);
        Bundle args = new Bundle();
        args.putParcelable("advParam", advParam);
        if(again) {
            getSupportLoaderManager().restartLoader(0, args, this);
        } else {
            getSupportLoaderManager().initLoader(0, args, this);
        }
    }


    @Override
    public void setStation(Station stn,int lineId, boolean advSearch) {
        setupConfigDisplay(stn, lineId, true);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new TrainListLoader(getApplicationContext(), (AdvSearchParam)args.getParcelable("advParam"));
    }


    @Override
    public void onLoaderReset(Loader loader) {
        mTrainRVAdapter.updateList(null,0);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        advParam.currentIndex = ((TrainListLoader)loader).currentIndex;

        Log.d(TAG,"currentIndex "+advParam.currentIndex);

        mTrainRVAdapter.updateList((List<TrainInfoBean>) data, advParam.currentIndex);
        int listSize = ((List<TrainInfoBean>) data).size();

//        if(advParam.currentIndex>15) {
//            trainRV.scrollToPosition(advParam.currentIndex-15);
//        }
//        if(advParam.currentIndex<(listSize+4)) {
//            trainRV.scrollToPosition(advParam.currentIndex + 2);
//        } else {
//            trainRV.scrollToPosition(advParam.currentIndex);
//        }
        trainRV.scrollToPosition(advParam.currentIndex);
    }

    @Override
    public void updateListFromTo(String towards, String from) {
        advParam.towardsFilter = towards;
        advParam.fromFilter = from;

        String stnCode = "";
        String stnName = "";
        for(String idStr : advParam.towardsFilter.split(",")) {
            if(idStr.trim().equals("")) {
                continue;
            }
            Station stn = MRMApp.me().getStation(Integer.parseInt(idStr));
            if(stn!=null && stn.getCode()!=null) {
                stnCode = stnCode + "  " + stn.getCode();
            }
            stnName = stn.getName();
        }
        if(advParam.direction=='U') {
            if(stnName.trim().length()>0) towardTV.setText(stnName);
            upTowardCodeTV.setText(stnCode.trim());
        } else {
            if(stnName.trim().length()>0) fromTV.setText(stnName);
            downTowardCodeTV.setText(stnCode.trim());
        }

        updateList(true);
    }
}