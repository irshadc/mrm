package com.glassboxsoftware.mrmapp.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.StationHaltInfoBean;
import com.glassboxsoftware.mrmapp.ui.adapter.TrainHaltRVAdapter;

import java.util.ArrayList;
import java.util.List;

public class TrainHaltActivity extends AppCompatActivity {

    protected RecyclerView trainRV;
    protected TrainHaltRVAdapter mTrainRVAdapter;
    protected RecyclerView.LayoutManager mTrainLayoutManager;
    private List<StationHaltInfoBean> mShiList = new ArrayList<>();
    private int stnId, trainId, lineId, lineColor;


    private TextView firstTitleTV, firstPltNumTV, firstTimeMinTV, lastTitleTV, lastPltNumTV, lastTimeMinTV;
    private ImageView firstStnStopIV,lastStnStopIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the message from the intent
        Intent intent = getIntent();
        stnId = intent.getIntExtra("stnId", 0);
        trainId = intent.getIntExtra("trainId", 0);
        lineId = intent.getIntExtra("lineId", 0);
        String details = intent.getStringExtra("details");
        String extraDetails = intent.getStringExtra("extraDetails");
        // Inflate the layout for this fragment

        lineColor = getResources().getColor(MRMApp.lineColorMap[lineId-1]);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            float[] hsv = new float[3];
            int color = lineColor;
            Color.colorToHSV(color, hsv);
            hsv[2] *= 0.8f; // value component
            color = Color.HSVToColor(hsv);

            window.setStatusBarColor(color);
        }

        setContentView(R.layout.a_train_halt);

        trainRV = (RecyclerView) findViewById(R.id.trainHaltRV);

        findViewById(R.id.lineHaltIV).setBackgroundColor(lineColor);
        findViewById(R.id.headerRL).setBackgroundColor(lineColor);

        findViewById(R.id.closePopupIV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        firstTitleTV = (TextView) findViewById(R.id.firstTitleTV);
        firstPltNumTV = (TextView) findViewById(R.id.firstPltNumTV);
        firstTimeMinTV = (TextView) findViewById(R.id.firstTimeMinTV);
        firstStnStopIV = (ImageView) findViewById(R.id.firstStnStopIV);

        lastTitleTV = (TextView) findViewById(R.id.lastTitleTV);
        lastPltNumTV = (TextView) findViewById(R.id.lastPltNumTV);
        lastTimeMinTV = (TextView) findViewById(R.id.lastTimeMinTV);
        lastStnStopIV = (ImageView) findViewById(R.id.lastStnStopIV);

        ((TextView) findViewById(R.id.lineNameTV)).setText(MRMApp.me().getLine(lineId).getName()+" - "+extraDetails);
        ((TextView) findViewById(R.id.trainStatusTV)).setText(details);

        mTrainLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        trainRV.setLayoutManager(mTrainLayoutManager);

        trainRV.setHasFixedSize(true);
        mTrainRVAdapter = new TrainHaltRVAdapter(mShiList, stnId, lineId, this);

        // Set CustomAdapter as the adapter for RecyclerView.
        trainRV.setAdapter(mTrainRVAdapter);

        TrainListTask tlt = new TrainListTask();
        tlt.execute();
    }

    private class TrainListTask extends AsyncTask<Void, Void, List<StationHaltInfoBean>> {
        private StationHaltInfoBean firstShi,lastShi;

        @Override
        protected List<StationHaltInfoBean> doInBackground(Void ...param) {

            List<StationHaltInfoBean> shiList = new ArrayList<>();

            String tableName = "train_schedule_details";
            switch (lineId) {
                case 1: tableName = "tsd_l1"; break;
                case 2: tableName = "tsd_l2"; break;
                case 3: tableName = "tsd_l34"; break;
                case 4: tableName = "tsd_l34"; break;
                case 5: tableName = "tsd_l56"; break;
                case 6: tableName = "tsd_l56"; break;
                case 7: tableName = "tsd_l7"; break;
                case 8: tableName = "tsd_l8"; break;
            }

            String queryText =  " SELECT T.STATION_ID, T.TIME_IN_MINUTES, T.PLATFORM_NO, T.PLATFORM_SIDE " +
                    " FROM "+tableName+" T " +
                    " WHERE T.TRAIN_ID = " + trainId +
                    " order by T.TIME_IN_MINUTES ";

            Cursor cursor = MRMApp.me().getDaoSession().getDatabase().rawQueryWithFactory(null,queryText,null,null);
            int i=0;
            if(cursor!=null) {
                cursor.moveToFirst();
                while (!cursor.isClosed() && !cursor.isAfterLast()) {
                    StationHaltInfoBean shi = new StationHaltInfoBean();
                    shi.stnId = cursor.getInt(0);
                    shi.stnName = MRMApp.me().getStation(shi.stnId).getName();
                    shi.timeInMinute = cursor.getInt(1);
                    shi.platformNo = cursor.getString(2);
                    shi.platformSide = cursor.getString(3);
                    if(i==0) {
                        firstShi = shi;
                    } else if (cursor.isLast()) {
                        lastShi = shi;
                    } else {
                        shiList.add(shi);
                    }
                    cursor.moveToNext();
                    i++;
                }
                cursor.close();
            }
            return shiList;
        }

        @Override
        protected void onPostExecute(List<StationHaltInfoBean> shiList) {
            if(shiList==null) {
                return;
            }

            mTrainRVAdapter.updateList(shiList);
            trainRV.smoothScrollToPosition(shiList.size()/2);

            firstTitleTV.setText(firstShi.stnName);
            if(firstShi.timeInMinute>0) {
                firstTimeMinTV.setText(MRMApp.getTimeInFormatPM(firstShi.timeInMinute));
                ((GradientDrawable)firstStnStopIV.getDrawable()).setStroke(getResources().getDimensionPixelSize(R.dimen.terminal_stroke_w),lineColor);
                if (firstShi.platformNo != null && firstShi.platformNo.length() > 0) {
                    firstPltNumTV.setText("PF " + firstShi.platformNo + " ");
                    firstPltNumTV.setBackgroundResource(R.drawable.side_none);
                    if (firstShi.platformSide != null && firstShi.platformSide.length() > 0) {
                        switch (firstShi.platformSide.charAt(0)) {
                            case 'L':
                                firstPltNumTV.setBackgroundResource(R.drawable.side_left);
                                break;
                            case 'R':
                                firstPltNumTV.setBackgroundResource(R.drawable.side_right);
                                break;
                            case 'B':
                                firstPltNumTV.setBackgroundResource(R.drawable.side_both);
                                break;
                        }
                    }
                } else {
                    firstPltNumTV.setText("");
                    firstPltNumTV.setBackgroundResource(0);
                }
            } else {
                firstStnStopIV.setVisibility(View.INVISIBLE);
            }

            lastTitleTV.setText(lastShi.stnName);
            if(lastShi.timeInMinute>0) {
                lastTimeMinTV.setText(MRMApp.getTimeInFormatPM(lastShi.timeInMinute));
                ((GradientDrawable)lastStnStopIV.getDrawable()).setStroke(getResources().getDimensionPixelSize(R.dimen.terminal_stroke_w), lineColor);
                if (lastShi.platformNo != null && lastShi.platformNo.length() > 0) {
                    lastPltNumTV.setText("PF " + lastShi.platformNo + " ");
                    lastPltNumTV.setBackgroundResource(R.drawable.side_none);
                    if (lastShi.platformSide != null && lastShi.platformSide.length() > 0) {
                        switch (lastShi.platformSide.charAt(0)) {
                            case 'L':
                                lastPltNumTV.setBackgroundResource(R.drawable.side_left);
                                break;
                            case 'R':
                                lastPltNumTV.setBackgroundResource(R.drawable.side_right);
                                break;
                            case 'B':
                                lastPltNumTV.setBackgroundResource(R.drawable.side_both);
                                break;
                        }
                    }
                } else {
                    lastPltNumTV.setText("");
                    lastPltNumTV.setBackgroundResource(0);
                }
            } else {
                lastStnStopIV.setVisibility(View.INVISIBLE);
            }
        }
    }
}
