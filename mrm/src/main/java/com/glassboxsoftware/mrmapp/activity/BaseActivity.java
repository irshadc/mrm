package com.glassboxsoftware.mrmapp.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.fragment.NavDrawFragment;

import java.util.Locale;



public abstract class BaseActivity extends AppCompatActivity
        implements
        NavDrawFragment.NavigationDrawerCallbacks {
    protected NavDrawFragment navFragment;
    private DrawerLayout mDrawerLayout;

    private boolean shouldShowActionBar = true;

    private RelativeLayout browseMapRL;


    // Primary toolbar and drawer toggle
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    protected void initDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        navFragment = (NavDrawFragment) getSupportFragmentManager().findFragmentById(R.id.navDrawFragment);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {

            }

            @Override
            public void onDrawerClosed(View view) {

            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

        browseMapRL = (RelativeLayout) findViewById(R.id.browseMapRL);
        browseMapRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browseMapActivity();
            }
        });

        findViewById(R.id.aboutMRMTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutActivity("About MRM", "about");
            }
        });
        findViewById(R.id.lineRL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutActivity("Mumbai Suburban lines", "line");
            }
        });
        findViewById(R.id.ticketRL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutActivity("Rail ticketing system", "ticket");
            }
        });

        findViewById(R.id.helpTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MRMApp.setDefault("help_read", "no");
                Intent intent = new Intent(getApplicationContext(), StartActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.languageTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CharSequence lang[] = new CharSequence[]{"English", "हिन्दी","मराठी","اُردُو"};

                AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
                builder.setTitle("Choose you language...");
                builder.setItems(lang, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Locale locale = null;
                        if (which == 0) {
                            MRMApp.setDefault("LANGUAGE-PREF", "English");
                            locale = new Locale("en");
                            Locale.setDefault(locale);
                        } else if (which == 1) {
                            MRMApp.setDefault("LANGUAGE-PREF", "hindi");
                            locale = new Locale("hi");
                            Locale.setDefault(locale);
                        } else if (which == 2) {
                            MRMApp.setDefault("LANGUAGE-PREF", "marathi");
                            locale = new Locale("mr");
                            Locale.setDefault(locale);
                        } else if (which == 3) {
                            MRMApp.setDefault("LANGUAGE-PREF", "urdu");
                            locale = new Locale("ur");
                            Locale.setDefault(locale);
                        }
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config, null);
                        finish();
                    }
                });
                builder.show();
            }
        });

        findViewById(R.id.feedbackTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/html");

                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.feedback_email)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,getString(R.string.feedback_email_subject) + MRMApp.versionStr);
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.default_text));

                startActivity(Intent.createChooser(emailIntent, "Send Feedback"));
            }
        });

        findViewById(R.id.mrmAppURLTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent linkIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.app_url)));
                startActivity(linkIntent);
            }
        });
    }

    protected void browseMapActivity() {
        Intent intent = new Intent(this, BrowseMapActivity.class);
        intent.putExtra("srcStn", 0);
        intent.putExtra("dstStn", 0);
        intent.putExtra("routeOpt", 0);
        startActivity(intent);
    }

    protected void aboutActivity(String title, String fileName) {
        Intent intent = new Intent(this, AboutActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("fileName", fileName);
        startActivity(intent);
    }

//    protected Toolbar getActionBarToolbar() {
//        if (mToolbar == null) {
//            mToolbar = (Toolbar) findViewById(R.id.toolbar);
//            if (mToolbar != null) {
//                setSupportActionBar(mToolbar);
//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                mToolbar.setNavigationIcon(R.drawable.ic_hamburger);
//                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        mDrawerLayout.openDrawer(GravityCompat.START);
//                    }
//                });
//            }
//
//        }
//        return mToolbar;
//    }

    protected void openDrawer() {
        if (mDrawerLayout != null)
            mDrawerLayout.openDrawer(GravityCompat.START);
    }
}