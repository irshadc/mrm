package com.glassboxsoftware.mrmapp.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.R;


public class AboutActivity extends AppCompatActivity {

    private WebView webView;
    private TextView titleTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the message from the intent
        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String fileName = intent.getStringExtra("fileName");

        setContentView(R.layout.a_about);

        titleTV = (TextView) findViewById(R.id.titleTV);

        titleTV.setText(title);

        webView = (WebView) findViewById(R.id.webView);

        //wv.getSettings().setPluginsEnabled(true);
        //WebSettings webSettings = wv.getSettings();
        //webSettings.setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/"+fileName+".html");

        webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && url.startsWith("http://")) {
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    return false;
                }
            }
        });

        findViewById(R.id.closeV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}