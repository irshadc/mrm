package com.glassboxsoftware.mrmapp.activity;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.dao.SpriteData;
import com.glassboxsoftware.mrmapp.dao.Station;
import com.glassboxsoftware.mrmapp.fragment.StationListFragment;
import com.irscomp.bitmapview.LargeImageView;

import java.util.List;


public class BrowseMapActivity extends AppCompatActivity implements StationListFragment.OnFragmentInteractionListener{

    private LargeImageView mapIV;

    private TextView searchET;

    private ImageView clearSearchIV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the message from the intent
        Intent intent = getIntent();
        int srcId = intent.getIntExtra("srcStn", 0);
        int dstId = intent.getIntExtra("dstStn", 0);
        int routeOpt = intent.getIntExtra("routeOpt", 0);

        setContentView(R.layout.a_browse_map);

        mapIV = (LargeImageView) findViewById(R.id.mapIV);

        ImageView closeSearchIV = (ImageView) findViewById(R.id.closeSearchIV);

        closeSearchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchET = (TextView) findViewById(R.id.searchET);
        searchET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("station_list");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                // Create and show the dialog.
                DialogFragment erFragment = StationListFragment.newInstance(0,false, null);
                erFragment.show(ft, "station_list");
            }
        });


        clearSearchIV = (ImageView) findViewById(R.id.clearSearchIV);

        clearSearchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchET.setText("");
                clearSearchIV.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void setStation(Station stn,int lineId, boolean advSearch) {
        searchET.setText(stn.getName());
        clearSearchIV.setVisibility(View.VISIBLE);
        List<SpriteData> spriteDataList = MRMApp.me().getDaoSession().getSpriteDataDao().queryRaw(" WHERE T.STATION_ID = "+stn.getId()+" AND T.SPRITE_TYPE='Marker' LIMIT 1");

        for (SpriteData sd : spriteDataList) {
            int x = (sd.getSpriteX() + sd.getMapW() / 2) / 4;
            int y = (sd.getSpriteY() + sd.getMapH() / 2) / 4;
            mapIV.pointMarkerF = new PointF(x, y);
            mapIV.animateScaleAndCenter(1.5f, new PointF(x, y)).start();
            break;
        }
    }
}