package com.glassboxsoftware.mrmapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;


public class TSActivity extends AppCompatActivity {

    private WebView webView;
    private TextView approveTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the message from the intent
        Intent intent = getIntent();

        setContentView(R.layout.a_tos);

        approveTV = (TextView) findViewById(R.id.approveTV);

        approveTV.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 MRMApp.setDefault("tos_read", "yes");
                 Intent intent = new Intent(getApplicationContext(), StartActivity.class);
                 startActivity(intent);
             }
         }
        );

        webView = (WebView) findViewById(R.id.webView);

        //wv.getSettings().setPluginsEnabled(true);
        //WebSettings webSettings = wv.getSettings();
        //webSettings.setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/tos.html");

    }
}