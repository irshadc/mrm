package com.glassboxsoftware.mrmapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;

import java.util.Locale;

public class StartActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_start);

        TextView versionTV = (TextView) findViewById(R.id.versionTV);

        versionTV.setText("version " + MRMApp.versionStr);

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (MRMApp.loadingDone) {
                    openApp();
                } else {
                    handler.postDelayed(this, 500);
                }
            }
        };
        handler.postDelayed(runnable, 500);
    }

    private void openApp() {
        if ("yes".equals(MRMApp.getDefault("tos_read"))) {
            if ("no".equals(MRMApp.getDefault("help_read"))) {
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
                // Set up the ViewPager with the sections adapter.
                mViewPager = (ViewPager) findViewById(R.id.pager);
                mViewPager.setAdapter(mSectionsPagerAdapter);
                mViewPager.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mViewPager.getCurrentItem() < 5) {
                            mViewPager.setCurrentItem(mViewPager.getCurrentItem(), true);
                        }
                    }
                });
            } else {
                Intent intent = new Intent(this, SelectionActivity.class);
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(this, TSActivity.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 6;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.text_mrm_app).toUpperCase(l);
                case 1:
                    return getString(R.string.text_mrm_app).toUpperCase(l);
                case 2:
                    return getString(R.string.text_mrm_app).toUpperCase(l);
                case 3:
                    return getString(R.string.text_mrm_app).toUpperCase(l);
                case 4:
                    return getString(R.string.text_mrm_app).toUpperCase(l);
                case 5:
                    return getString(R.string.text_mrm_app).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */

        private int section;

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            fragment.section = sectionNumber;
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.f_first_run, container, false);

            if (section != 0) {
                ImageView help = (ImageView) rootView.findViewById(R.id.help_screen);
                switch (section) {
                    case 1:
                        help.setImageResource(R.drawable.intro_1);
                        break;
                    case 2:
                        help.setImageResource(R.drawable.intro_2);
                        break;
                    case 3:
                        help.setImageResource(R.drawable.intro_3);
                        break;
                    case 4:
                        help.setImageResource(R.drawable.intro_4);
                        break;
                    case 5:
                        help.setImageResource(R.drawable.intro_5);
                        break;
                    case 6:
                        help.setImageResource(R.drawable.intro_6);
                        Button btn = (Button) rootView.findViewById(R.id.button);
                        btn.setText("Start");
                        btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MRMApp.setDefault("help_read", "yes");
                                Intent intent = new Intent(getActivity(), SelectionActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });
                        help.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MRMApp.setDefault("help_read", "yes");
                                Intent intent = new Intent(getActivity(), SelectionActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });
                }
            }
            return rootView;
        }
    }

}
