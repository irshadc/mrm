package com.glassboxsoftware.mrmapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.RouteScheduleBean;
import com.glassboxsoftware.mrmapp.fragment.NavDrawFragment;
import com.glassboxsoftware.mrmapp.fragment.RouteFragment;
import com.glassboxsoftware.mrmapp.fragment.RouteMapFragment;
import com.glassboxsoftware.mrmapp.fragment.TrainListFragment;
import com.glassboxsoftware.mrmapp.ui.adapter.ResultViewAdapter;

import java.util.ArrayList;
import java.util.List;


public class RouteActivity extends AppCompatActivity implements
        ResultViewAdapter.OpenExpandedRouteListener,
        NavDrawFragment.NavigationDrawerCallbacks
{

    public int srcId,dstId,routeId;
    public List<RouteScheduleBean> mRsbList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the message from the intent
        Intent intent = getIntent();
        srcId = intent.getIntExtra("srcStn", 0);
        dstId = intent.getIntExtra("dstStn", 0);
        routeId = intent.getIntExtra("routeOpt", 0);

        if(srcId==0 || dstId==0) {
            finish();
            return;
        }
        setContentView(R.layout.a_route);
    }

    @Override
    public void showTrainListFragment(RouteFragment routeFragment, RouteScheduleBean rsb, int position) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("train_list");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DialogFragment erFragment = TrainListFragment.newInstance(routeFragment,rsb,position);
        erFragment.show(getSupportFragmentManager(),"train_list");
    }

    public void showRouteMapFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("route_map");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        RouteMapFragment erFragment = RouteMapFragment.newInstance(this);
        erFragment.show(getSupportFragmentManager(),"route_map");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==101) {
            findViewById(R.id.exploreMoreTV).performClick();
        }
    }
}