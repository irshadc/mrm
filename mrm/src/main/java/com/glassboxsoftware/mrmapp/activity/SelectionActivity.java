package com.glassboxsoftware.mrmapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.RouteChoiceBean;
import com.glassboxsoftware.mrmapp.dao.Station;
import com.glassboxsoftware.mrmapp.dao.internal.FavRoute;
import com.glassboxsoftware.mrmapp.dao.internal.InternalDao;
import com.glassboxsoftware.mrmapp.dao.internal.NotifyMsg;
import com.glassboxsoftware.mrmapp.fragment.NotificationListFragment;
import com.glassboxsoftware.mrmapp.fragment.RouteFragment;
import com.glassboxsoftware.mrmapp.fragment.StationListFragment;
import com.glassboxsoftware.mrmapp.loader.FavListLoader;
import com.glassboxsoftware.mrmapp.loader.RouteChoiceLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class SelectionActivity extends BaseActivity
        implements StationListFragment.OnFragmentInteractionListener,
NotificationListFragment.OnFragmentInteractionListener,
        LoaderManager.LoaderCallbacks,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private TextView srcStnTV, dstStnTV;
    private ImageView routeReverseIV;
    private Button getRouteBtn;
    private boolean srcActive = true;
    private int srcId, dstId;
    private int routeId;

    private FavListAdapter mFavAdapter;
    private RouteFragment.RouteSpinnerAdapter rcAdapter;

    private Spinner routeOptionSpinner;

    private ImageView openDrawerIV,notificationIV;

    private List<FavRoute> mFavList;

    private ListView favListView;
    private static final String TAG = "SelectionActivity";
    //private String regId="";

    //private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    //private String SENDER_ID = "";
    //private GoogleCloudMessaging gcm;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_station_sel);
        initDrawer();
        buildGoogleApiClient();

        favListView = (ListView) findViewById(R.id.favListView);

        openDrawerIV = (ImageView) findViewById(R.id.openDrawerIV);
        notificationIV = (ImageView) findViewById(R.id.notificationIV);

        View headerView = getLayoutInflater().inflate(R.layout.v_search_header, null);
        View footerView = getLayoutInflater().inflate(R.layout.v_selection_footer, null);

        srcStnTV = (TextView) headerView.findViewById(R.id.srcStnTV);
        dstStnTV = (TextView) headerView.findViewById(R.id.dstStnTV);
        getRouteBtn = (Button) headerView.findViewById(R.id.getRouteBtn);
        routeReverseIV = (ImageView) headerView.findViewById(R.id.routeReverseIV);

        routeOptionSpinner = (Spinner) headerView.findViewById(R.id.routeOptionSpinner);

        footerView.findViewById(R.id.mrmAppURLTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent linkIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.app_url)));
                startActivity(linkIntent);
            }
        });

        favListView.addHeaderView(headerView);
        favListView.addFooterView(footerView);

        mFavList = new ArrayList<>();

        mFavAdapter = new FavListAdapter(this, mFavList);
        favListView.setAdapter(mFavAdapter);
        routeOptionSpinner.setVisibility(View.GONE);

        srcStnTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("station_list");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.
                DialogFragment erFragment = StationListFragment.newInstance(dstId, false, mLastLocation);
                erFragment.show(ft, "station_list");

                srcActive = true;
            }
        });

        dstStnTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("station_list");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.
                DialogFragment erFragment = StationListFragment.newInstance(srcId, false, mLastLocation);
                erFragment.show(ft, "station_list");
                srcActive = false;
            }
        });


        routeReverseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int temp = srcId;
                srcId = dstId;
                dstId = temp;
                String tempStr = srcStnTV.getText().toString();
                srcStnTV.setText(dstStnTV.getText());
                dstStnTV.setText(tempStr);
                if (srcStnTV.getText().length()>0 && dstStnTV.getText().length()>0 && srcId > 0 && dstId > 0) {
                    findRoute();
                }
            }
        });

        getRouteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (srcId > 0 && dstId > 0) {
                    routeId = 0;
                    if (srcId == dstId || routeOptionSpinner.getCount() >= 1) {
                        routeId = routeOptionSpinner.getSelectedItemPosition();
                        openRouteActivity();
                    } else {
                        Log.e("Route Find", "No Route Found " + srcId + " > " + dstId);
                        Toast.makeText(getApplicationContext(), "Sorry No Route found! Please write feedback if this seems as an error.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        headerView.findViewById(R.id.advSearchRL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("station_list");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.
                DialogFragment erFragment = StationListFragment.newInstance(0, true, mLastLocation);
                erFragment.show(ft, "station_list");
            }
        });

        openDrawerIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer();
            }
        });
        notificationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNotification();
            }
        });

        Intent intent = getIntent();

        if(intent!=null && intent.getBooleanExtra("show_notification", false)){
            showNotification();
        }

        getSupportLoaderManager().initLoader(0, null, this);
//        // Check device for Play Services APK.
//        if (checkPlayServices() && isConnected()) {
//            gcm = GoogleCloudMessaging.getInstance(this);
//            regId = getRegistrationId(getApplicationContext());
//            if (regId.isEmpty()) {
//                registerInBackground();
//            }
//        } else {
//            Log.i(TAG, "No valid Google Play Services APK found.");
//        }
        sendUsage();
    }

    private void showNotification(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("notification_list");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment erFragment = NotificationListFragment.newInstance();
        erFragment.show(ft, "notification_list");
    }

    public void reloadFavList() {
        if (MRMApp.loadingDone) {
            getSupportLoaderManager().restartLoader(0, null, this);
        }
    }

    public void launchFromFavorite(FavRoute fav) {
        if (fav.getSrcId() != 0 && fav.getDstId() != 0) {
            srcId = fav.getSrcId();
            dstId = fav.getDstId();
            routeId = fav.getRouteId();
            openRouteActivity();
        } else {
            reloadFavList();
        }
    }

    private void openRouteActivity() {
        Intent intent = new Intent(this, RouteActivity.class);
        intent.putExtra("srcStn", srcId);
        intent.putExtra("dstStn", dstId);
        intent.putExtra("routeOpt", routeId);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadFavList();
        checkPlayServices();
    }

    @Override
    public void setStation(Station sm, int lineId, boolean advSearch) {
        if (sm != null) {
            if(advSearch) {
                Intent intent = new Intent(this, AdvSearchActivity.class);
                intent.putExtra("stnId", sm.getId().intValue());
                intent.putExtra("lineId", lineId);
                intent.putExtra("routeOpt", routeId);
                intent.putExtra("CURR_LOCATION",mLastLocation);
                startActivity(intent);

            } else {
                if (srcActive) {
                    srcId = sm.getId().intValue();
                    srcStnTV.setText(sm.getName());
                    srcStnTV.setTypeface(MRMApp.robotoMedium);
                } else {
                    dstId = sm.getId().intValue();
                    dstStnTV.setText(sm.getName());
                    dstStnTV.setTypeface(MRMApp.robotoMedium);
                }
                if (srcId > 0 && dstId > 0) {
                    findRoute();
                }
            }
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        if (id == 1) {
            return new RouteChoiceLoader(getApplicationContext(), args.getInt("src"), args.getInt("dst"));
        } else {
            return new FavListLoader(getApplicationContext());
        }
    }


    @Override
    public void onLoaderReset(Loader loader) {
        if (loader.getId() == 1) {
            routeOptionSpinner.setVisibility(View.GONE);
        } else {
            mFavAdapter.updateAdapter(null);
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if (loader.getId() == 1) {
            List<RouteChoiceBean> rcbList = (List<RouteChoiceBean>) data;
            if (rcbList != null && rcbList.size() >= 1) {
                routeOptionSpinner.setVisibility(View.VISIBLE);
                rcAdapter = new RouteFragment.RouteSpinnerAdapter(getApplicationContext(), rcbList);
                routeOptionSpinner.setAdapter(rcAdapter);
            } else {
                routeOptionSpinner.setVisibility(View.GONE);
            }
        } else {
            mFavAdapter.updateAdapter((List<FavRoute>) data);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void setNotifyMsg(NotifyMsg NotifyMsg) {

    }


    public class FavListAdapter extends ArrayAdapter<FavRoute> {

        private final Context context;
        private List<FavRoute> values = new ArrayList<>();
        private LayoutInflater mInflater;

        public FavListAdapter(Context context, List<FavRoute> mList) {
            super(context, R.layout.v_fav_entry, mList);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.context = context;
            this.values = mList;
        }

        public void updateAdapter(List<FavRoute> data) {

            this.values.clear();
            if (data != null) {
                this.values.addAll(data);
            }
            notifyDataSetChanged();
        }

        private class ViewHolder {
            public TextView stnNameTV, viaTV;
            public ImageView favStnIV;
            public LinearLayout stationLL;
        }

        private View.OnClickListener setStnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyDataSetChanged();
                launchFromFavorite((FavRoute) v.getTag());
            }
        };

        private View.OnClickListener favClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavRoute favBean = (FavRoute) v.getTag();
                if (favBean.getFavourite()) {
                    InternalDao.getDao().removeFavRoute(favBean);
                    reloadFavList();
                } else {
                    favBean.setFavourite(true);
                    InternalDao.getDao().upsertFavRoute(favBean);
                    reloadFavList();
                }
            }
        };

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final FavRoute fav = values.get(position);
            ViewHolder holder = new ViewHolder();

            if (fav == null || fav.getSrcId() == 0) {
                convertView = mInflater.inflate(R.layout.v_navdrawer_header, parent, false);
                if (fav != null && fav.getRouteStr() != null && fav.getRouteStr().length() > 1) {
                    holder.stnNameTV = (TextView) convertView.findViewById(R.id.titleTV);
                    holder.stnNameTV.setTypeface(MRMApp.normalTF, Typeface.NORMAL);
                    holder.stnNameTV.setText(fav.getRouteStr());
                }
            } else if (fav.getSrcId() > 0) {

                if (fav.getSrcId() > 0 && fav.getDstId() > 0) {
                    convertView = mInflater.inflate(R.layout.v_fav_entry, parent, false);
                    holder.stnNameTV = (TextView) convertView.findViewById(R.id.trainStatusTV);
                    holder.stnNameTV.setTypeface(MRMApp.normalTF, Typeface.NORMAL);
                    holder.favStnIV = (ImageView) convertView.findViewById(R.id.iconTV);
                    holder.viaTV = (TextView) convertView.findViewById(R.id.viaTV);
                    holder.viaTV.setTypeface(MRMApp.normalTF, Typeface.NORMAL);

                    holder.stnNameTV.setText(fav.fromStnName + " → " + fav.toStnName);
                    holder.viaTV.setText("Via: " + fav.getRouteStr());
                    if (fav.getFavourite()) {
                        holder.favStnIV.setImageResource(R.drawable.star_on);
                    } else {
                        holder.favStnIV.setImageResource(R.drawable.star_off);
                        RelativeLayout stnVLayout = (RelativeLayout) convertView.findViewById(R.id.stnVLayout);
                        stnVLayout.setBackgroundResource(R.drawable.recent_item_bg);
                    }

                    convertView.setOnClickListener(setStnClickListener);
                    convertView.setTag(fav);
                    holder.favStnIV.setOnClickListener(favClickListener);
                    holder.favStnIV.setTag(fav);
                }

            }
            //convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }

            return convertView;
        }
    }

    private void findRoute() {
        Bundle args = new Bundle();
        args.putInt("src", srcId);
        args.putInt("dst", dstId);
        getSupportLoaderManager().restartLoader(1, args, this);
    }

    private void sendUsage() {
        long lastTime = Long.parseLong("0" + MRMApp.getDefault("last_sync"));
        int dayToLastRefresh = (int) ((System.currentTimeMillis() - lastTime) / ( 24 * 60 * 60 * 1000));

        if (dayToLastRefresh > 2 && isConnected()) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String result = null;
                    try {
                        List<FavRoute> syncList = InternalDao.getDao().getSyncList();
                        if (syncList != null && syncList.size() > 0) {
                            JSONArray jsArr = new JSONArray();
                            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.timestamp_route_format), Locale.US);
                            for(FavRoute fav: syncList) {
                                JSONObject jsObj = new JSONObject();
                                jsObj.put("srcId",fav.getSrcId());
                                jsObj.put("dstId",fav.getDstId());
                                jsObj.put("jxIdLst",fav.getJxIdLst());
                                jsObj.put("jxStrLst",fav.getRouteStr());
                                jsObj.put("osIdentifier","Android "+Build.VERSION.RELEASE);
                                jsObj.put("appVer",MRMApp.versionStr);
                                jsObj.put("fav",fav.getFavourite());
                                jsObj.put("active",fav.getActive());
                                jsObj.put("lastModifiedTS", sdf.format(new Date(fav.getLastModified())));
                                jsArr.put(jsObj);
                            }


                            URL url = new URL("http://stats.mrmapp.in/stats.php");
                            HttpURLConnection hc = (HttpURLConnection) url.openConnection();

                            hc.setDoOutput(true);
                            hc.setChunkedStreamingMode(0);

                            hc.setRequestProperty("App-Version-X", "cpsess6885068682");
                            hc.setRequestProperty("Accept", "application/json");
                            hc.setRequestProperty("Content-type", "application/json");


                            OutputStream out = new BufferedOutputStream(hc.getOutputStream());
                            out.write(jsArr.toString().getBytes());
                            out.flush();
                            out.close();

                            result = convertInputStreamToString(hc.getInputStream());
                            if (result != null && result.indexOf("SUCCESS")>=0) {
                                for(FavRoute fav : syncList){
                                    InternalDao.getDao().setSync(fav);
                                }
                                MRMApp.setDefault("last_sync",System.currentTimeMillis()+"");
                            }

                            hc.disconnect();
                        }
                    } catch (IOException ex) {
                        result = "Error :" + ex.getMessage();
                    } catch (JSONException e) {
                        result = "Error :" + e.getMessage();
                    }
                    return result;
                }

                @Override
                protected void onPostExecute(String msg) {
                    //Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                }
            }.execute(null, null, null);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        if(inputStream==null) return null;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                //        PLAY_SERVICES_RESOLUTION_REQUEST).show();
                Log.e(TAG,"GooglePlayServicesUtil Error:"+resultCode);
            } else {
                Log.i(TAG, "This device is not supported.");
                //finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        String registrationId = MRMApp.getDefault("PROPERTY_REG_ID");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        String registeredVersion = MRMApp.getDefault("PROPERTY_APP_VERSION");
        String currentVersion = MRMApp.versionCode+"";
        if (!registeredVersion.equals(currentVersion)) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     *
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regId = gcm.register(SENDER_ID);

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the registration ID - no need to register again.
                    storeRegistrationId(regId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if(!msg.equals(""))
                    Log.e(TAG,msg);
            }
        }.execute(null, null, null);
    }*/

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     *
    private void sendRegistrationIdToBackend() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = "";
                try {
                    List<FavRoute> syncList = InternalDao.getDao().getSyncList();
                    if (syncList != null && syncList.size() > 0) {

                        JSONObject jsObj = new JSONObject();
                        SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.timestamp_route_format), Locale.US);
                        jsObj.put("regId", regId);
                        jsObj.put("appVer", MRMApp.versionStr);
                        jsObj.put("osIdentifier", "Android " + Build.VERSION.RELEASE);
                        jsObj.put("sentOnTS", sdf.format(new Date(System.currentTimeMillis())));

                        URL url = new URL("http://stats.mrmapp.in/gcmreg.php");
                        HttpURLConnection hc = (HttpURLConnection) url.openConnection();

                        hc.setDoOutput(true);
                        hc.setChunkedStreamingMode(0);

                        OutputStream out = new BufferedOutputStream(hc.getOutputStream());
                        out.write(jsObj.toString().getBytes());
                        out.flush();
                        out.close();

                        result = convertInputStreamToString(hc.getInputStream());
                        if (result != null && result.indexOf("SUCCESS")>=0) {
                            result="Successfully registered for GCM";
                        }

                        hc.disconnect();
                    }
                } catch (IOException ex) {
                    result = "Error :" + ex.getMessage();
                } catch (JSONException e) {
                    result = "Error :" + e.getMessage();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(TAG,msg);
            }
        }.execute(null, null, null);
    }*/

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @paramregId registration ID
     *
    private void storeRegistrationId(String regId) {
        Log.i(TAG, "Saving regId on app version " + MRMApp.versionCode);
        MRMApp.setDefault("PROPERTY_REG_ID",regId);
        MRMApp.setDefault("PROPERTY_APP_VERSION",MRMApp.versionCode+"");
    }*/

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d(TAG,"Last Location: "+mLastLocation.toString());
        } else {
            Log.d(TAG,getString(R.string.no_location_detected));
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }
}