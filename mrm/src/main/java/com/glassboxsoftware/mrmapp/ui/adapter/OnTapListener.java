package com.glassboxsoftware.mrmapp.ui.adapter;

/**
 * Created by Irshad Chohan on 11/27/2014.
 */
public interface OnTapListener {
    public void onTapView(int position);
}
