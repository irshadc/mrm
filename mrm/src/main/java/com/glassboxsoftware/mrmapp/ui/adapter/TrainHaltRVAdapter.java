package com.glassboxsoftware.mrmapp.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.StationHaltInfoBean;

import java.util.List;

/**
 * Created by Irshad Chohan on 10/24/2014.
 */
public class TrainHaltRVAdapter extends RecyclerView.Adapter<TrainHaltRVAdapter.ViewHolder> {

    private final List<StationHaltInfoBean> values;

    private final int lineId, stnId;
    private Context mContext;


   public TrainHaltRVAdapter(List<StationHaltInfoBean> mTibList, int stnId, int lineId, Context mContext) {
        this.values = mTibList;
        this.stnId = stnId;
       this.lineId = lineId;
       this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.v_stn_halt_item, viewGroup, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        final StationHaltInfoBean shi = values.get(position);


        final int lineColor = mContext.getResources().getColor(MRMApp.lineColorMap[lineId-1]);

        GradientDrawable gd = (GradientDrawable)mContext.getResources().getDrawable(R.drawable.line_stop);
        gd.setColor(lineColor);

        if(stnId==shi.stnId) {
            vh.timeMinTV.setTypeface(null, Typeface.BOLD);
            vh.titleTV.setTypeface(null, Typeface.BOLD);
            vh.timeMinTV.setTextColor(mContext.getResources().getColor(R.color.text_black_90));
            vh.titleTV.setTextColor(mContext.getResources().getColor(R.color.text_black_90));
        } else {
            vh.timeMinTV.setTypeface(null, Typeface.NORMAL);
            vh.titleTV.setTypeface(null, Typeface.NORMAL);
            vh.timeMinTV.setTextColor(mContext.getResources().getColor(R.color.text_black_70));
            vh.titleTV.setTextColor(mContext.getResources().getColor(R.color.text_black_70));
        }

        vh.titleTV.setText(shi.stnName);
        if(shi.timeInMinute>0) {
            vh.timeMinTV.setText(MRMApp.getTimeInFormatPM(shi.timeInMinute));
            vh.stnStopIV.setImageDrawable(gd);
            if (shi.platformNo != null && shi.platformNo.length() > 0) {
                vh.pltNumTV.setText("PF " + shi.platformNo + " ");
                vh.pltNumTV.setBackgroundResource(R.drawable.side_none);
                if (shi.platformSide != null && shi.platformSide.length() > 0) {
                    switch (shi.platformSide.charAt(0)) {
                        case 'L':
                            vh.pltNumTV.setBackgroundResource(R.drawable.side_left);
                            break;
                        case 'R':
                            vh.pltNumTV.setBackgroundResource(R.drawable.side_right);
                            break;
                        case 'B':
                            vh.pltNumTV.setBackgroundResource(R.drawable.side_both);
                            break;
                    }
                }
            } else {
                vh.pltNumTV.setText("");
                vh.pltNumTV.setBackgroundResource(0);
            }
        } else {
            vh.stnStopIV.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return values!=null ? values.size(): 0;
    }

    public void updateList(List<StationHaltInfoBean> shiList) {
        values.clear();
        if(shiList!=null) values.addAll(shiList);
        notifyDataSetChanged();
    }

    public final static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView timeMinTV;
        public final TextView titleTV;
        public final ImageView stnStopIV;
        public final TextView pltNumTV;
        public final RelativeLayout stnCardLL;

        public ViewHolder(View v) {
            super(v);
            timeMinTV = (TextView) v.findViewById(R.id.timeMinTV);
            titleTV = (TextView) v.findViewById(R.id.titleTV);
            pltNumTV = (TextView) v.findViewById(R.id.pltNumTV);
            stnStopIV = (ImageView) v.findViewById(R.id.stnStopIV);
            stnCardLL = (RelativeLayout) v.findViewById(R.id.stnCardLL);
        }
    }

}
