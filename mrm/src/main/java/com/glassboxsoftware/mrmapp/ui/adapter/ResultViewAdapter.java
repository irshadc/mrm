package com.glassboxsoftware.mrmapp.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.RouteScheduleBean;
import com.glassboxsoftware.mrmapp.bean.StationTimeBean;
import com.glassboxsoftware.mrmapp.fragment.RouteFragment;

import java.util.List;

/**
 * Created by Irshad Chohan on 10/24/2014.
 */
public class ResultViewAdapter extends ArrayAdapter<RouteScheduleBean> {

    private final List<RouteScheduleBean> values;
    private OnTapListener onTapListener;

    public interface OpenExpandedRouteListener {
        // TODO: Update argument type and name
        public void showTrainListFragment(RouteFragment routeFragment, RouteScheduleBean rsb, int position);
    }

    private OpenExpandedRouteListener mListener;

    private final Context mContext;
    private LayoutInflater mInflater;

    RouteFragment routeFragment;

    public ResultViewAdapter(List<RouteScheduleBean> values, Context context, RouteFragment routeFragment) {
        super(context, R.layout.v_route_list_item, values);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.values = values;
        this.mContext = context;
        mListener = (OpenExpandedRouteListener) context;
        this.routeFragment = routeFragment;
    }

    public final void updateList(List<RouteScheduleBean> values) {
        this.values.clear();
        this.values.addAll(values);
        notifyDataSetChanged();
    }

    public final void updateItem(RouteScheduleBean rsb, int position) {
        this.values.remove(position);
        this.values.add(position, rsb);
        //notifyItemChanged(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final RouteScheduleBean rsb = values.get(position);

        if(convertView==null) {
            convertView = mInflater.inflate(R.layout.v_route_list_item,parent,false);
            ViewHolder vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        }
        final ViewHolder vh = (ViewHolder)convertView.getTag();
        final int lineColor = mContext.getResources().getColor(MRMApp.lineColorMap[rsb.line.getId().intValue() - 1]);


        if (rsb.stbMap != null && rsb.stbMap.size() > 1) {

            vh.startStnTV.setText(rsb.stbMap.get(rsb.trainInfoBean.startStnId).stationName);
            vh.timeLabelTV.setText(MRMApp.getTimeInFormat(rsb.stbMap.get(rsb.trainInfoBean.startStnId).timeInMinute));

            if(rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformNo!=null && rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformNo.length()>0) {
                vh.startPltTV.setText("PF "+rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformNo);
                vh.startPltTV.setBackgroundResource(R.drawable.side_none);
                if(rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformSide!=null && rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformSide.length()>0) {
                    switch(rsb.stbMap.get(rsb.trainInfoBean.startStnId).platformSide.charAt(0)) {
                        case 'L':vh.startPltTV.setBackgroundResource(R.drawable.side_left); break;
                        case 'R':vh.startPltTV.setBackgroundResource(R.drawable.side_right); break;
                        case 'B':vh.startPltTV.setBackgroundResource(R.drawable.side_both); break;
                    }
                }
            } else {
                vh.startPltTV.setText("");
                vh.startPltTV.setBackgroundResource(0);
            }

            ((GradientDrawable)vh.startStnIV.getDrawable()).setStroke(mContext.getResources().getDimensionPixelSize(R.dimen.terminal_stroke_w),lineColor);
            ((GradientDrawable)vh.endStnIV.getDrawable()).setStroke(mContext.getResources().getDimensionPixelSize(R.dimen.terminal_stroke_w),lineColor);
            //vh.startStnIV.setImageDrawable(gd);
            //vh.endStnIV.setImageDrawable(gd);

            if (vh.stopCountTV != null)
                vh.stopCountTV.setText((rsb.noOfStop -2) + " stops. " + (rsb.noOfStation -2) + " stations. "+rsb.journeyTime +" mins.");

            vh.endTimeTV.setText(MRMApp.getTimeInFormat(rsb.stbMap.get(rsb.trainInfoBean.endStnId).timeInMinute));
            vh.endStnTV.setText(rsb.stbMap.get(rsb.trainInfoBean.endStnId).stationName);

            if(rsb.stbMap.get(rsb.trainInfoBean.endStnId).platformNo!=null && rsb.stbMap.get(rsb.trainInfoBean.endStnId).platformNo.length()>0) {
                vh.endPltTV.setText("PF "+rsb.stbMap.get(rsb.trainInfoBean.endStnId).platformNo);
                vh.endPltTV.setBackgroundResource(R.drawable.side_none);
                if(rsb.stbMap.get(rsb.trainInfoBean.endStnId).platformSide!=null && rsb.stbMap.get(rsb.trainInfoBean.endStnId).platformSide.length()>0) {
                    switch(rsb.stbMap.get(rsb.trainInfoBean.endStnId).platformSide.charAt(0)) {
                        case 'L':vh.endPltTV.setBackgroundResource(R.drawable.side_left); break;
                        case 'R':vh.endPltTV.setBackgroundResource(R.drawable.side_right); break;
                        case 'B':vh.endPltTV.setBackgroundResource(R.drawable.side_both); break;
                    }
                }
            } else {
                vh.endPltTV.setText("");
                vh.endPltTV.setBackgroundResource(0);
            }

            if (position == 0) {
                vh.startContIV.setVisibility(View.INVISIBLE);
            } else {
                vh.startContIV.setVisibility(View.VISIBLE);
            }
            if (position == (values.size() - 1)) {
                vh.endContIV.setVisibility(View.INVISIBLE);
            } else {
                vh.endContIV.setVisibility(View.VISIBLE);
            }

        }

        String timeBoarding = "";
        if(rsb.timeToBoard>0) {
            if(rsb.timeToBoard>59) {
                int h = (int)(rsb.timeToBoard / 60);
                timeBoarding = " IN <b>"+h + " H "+ (rsb.timeToBoard - h*60)+"</b> MINUTES";
            } else {
                timeBoarding = " IN <b>"+rsb.timeToBoard +"</b> MINUTES";
            }
        }

        vh.lineNameTV.setText(Html.fromHtml((position + 1) + ". ON " + rsb.line.getName().toUpperCase() + timeBoarding));

        vh.trainDstCodeTV.setText(rsb.trainInfoBean.lastStnCode);

        vh.trainDstNameTV.setText(rsb.trainInfoBean.lastStnName + " - " + rsb.trainInfoBean.speed);
        vh.trainInfoTV.setText("from " + rsb.trainInfoBean.firstStnName);

        String detailStr = "";
        if(rsb.trainInfoBean.noOfCar == -1) {
            rsb.trainInfoBean.noOfCar  = 12;
        }
        detailStr = rsb.trainInfoBean.noOfCar + " car"+(rsb.trainInfoBean.sundayOnly ? " | Sunday Only":"") + (rsb.trainInfoBean.notOnSunday ? " | Not on Sunday":"") +(rsb.trainInfoBean.holidayOnly ? " | Holiday Only":"");
        if("LS".equals(rsb.trainInfoBean.specialInfo)){
            detailStr+=" | Ladies Special";
        } else if (rsb.trainInfoBean.specialInfo!=null && rsb.trainInfoBean.specialInfo.trim().length() > 1) {
            detailStr+=" | "+rsb.trainInfoBean.specialInfo.trim();
        }
        vh.trainCarCountTV.setText(detailStr);


        vh.trainDstCodeTV.setTextColor(lineColor);
        vh.lineNameTV.setTextColor(lineColor);

        vh.lineIV.setBackgroundColor(lineColor);

        vh.expandedStationLL.setVisibility(View.VISIBLE);
        vh.expandedStationLL.removeAllViews();

        GradientDrawable gd = (GradientDrawable)mContext.getResources().getDrawable(R.drawable.line_stop);
        gd.setColor(lineColor);

        for (int stnId : rsb.stnIdList) {
            if(stnId == rsb.trainInfoBean.startStnId || stnId == rsb.trainInfoBean.endStnId) {
                continue;
            }
            StationTimeBean stb = rsb.stbMap.get(stnId);
            View childRoute = View.inflate(mContext, R.layout.v_stn_linear_item, null);
            TextView name = (TextView) childRoute.findViewById(R.id.titleTV);
            name.setText(stb.stationName);
            name.setTypeface(null, Typeface.NORMAL);

            TextView time = (TextView) childRoute.findViewById(R.id.timeMinTV);
            TextView pltNumTV = (TextView) childRoute.findViewById(R.id.pltNumTV);
            ImageView lineIcon = (ImageView) childRoute.findViewById(R.id.stnStopIV);
            time.setTypeface(null, Typeface.NORMAL);
            if(stb.timeInMinute > 0) {
                time.setText(MRMApp.getTimeInFormat(stb.timeInMinute));
                if(stb.platformNo!=null && stb.platformNo.length()>0) {
                    pltNumTV.setText("PF " + stb.platformNo+" ");
                    pltNumTV.setBackgroundResource(R.drawable.side_none);
                    if(stb.platformSide!=null && stb.platformSide.length()>0) {
                        switch(stb.platformSide.charAt(0)) {
                            case 'L':pltNumTV.setBackgroundResource(R.drawable.side_left); break;
                            case 'R':pltNumTV.setBackgroundResource(R.drawable.side_right); break;
                            case 'B':pltNumTV.setBackgroundResource(R.drawable.side_both); break;
                        }
                    }
                } else {
                    pltNumTV.setText("");
                    pltNumTV.setBackgroundResource(0);
                }
                lineIcon.setImageDrawable(gd);
            } else {
                name.setTextColor(mContext.getResources().getColor(R.color.text_black_4c));
                time.setTextColor(mContext.getResources().getColor(R.color.transparent));
                pltNumTV.setText("");
                lineIcon.setVisibility(View.INVISIBLE);
            }
            vh.expandedStationLL.addView(childRoute);
        }

        vh.expandedStationLL.setVisibility(View.GONE);
        vh.stopCountTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTapListener != null)
                    onTapListener.onTapView(position);

                if(vh.expandedStationLL.getVisibility() == View.VISIBLE) {
                    vh.expandedStationLL.setVisibility(View.GONE);
                    vh.expandIV.setImageResource(R.drawable.list_expand);
                } else {
                    vh.expandedStationLL.setVisibility(View.VISIBLE);
                    vh.expandIV.setImageResource(R.drawable.list_collapse);
                }
            }
        });

        vh.includeTrainRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTapListener != null)
                    onTapListener.onTapView(position);

                if (mListener != null)
                    mListener.showTrainListFragment(routeFragment, rsb, position);

            }
        });

        return convertView;
    }

    public final static class ViewHolder {

        public final TextView lineNameTV;
        public final TextView startStnTV;
        public final TextView startPltTV;
        public final TextView trainDstCodeTV;
        public final TextView startTimeTV;
        public final TextView timeLabelTV;

        public final TextView trainDstNameTV;
        public final TextView trainInfoTV;
        public final TextView trainCarCountTV;

        public final TextView stopCountTV;

        private final RelativeLayout includeTrainRL;

        public final TextView endTimeTV;
        public final TextView endStnTV;
        public final TextView endPltTV;

        public ImageView startContIV;
        public ImageView startStnIV;
        public ImageView lineIV;
        public ImageView endStnIV ;
        public ImageView endContIV ;
        public ImageView expandIV ;

        public LinearLayout expandedStationLL;

        public ViewHolder(View v) {

            lineNameTV = (TextView) v.findViewById(R.id.lineNameTV);
            startStnTV = (TextView) v.findViewById(R.id.startStnTV);
            startPltTV = (TextView) v.findViewById(R.id.startPltTV);

            trainDstCodeTV = (TextView) v.findViewById(R.id.trainDstCodeTV);
            startTimeTV = (TextView) v.findViewById(R.id.startTimeTV);
            timeLabelTV = (TextView) v.findViewById(R.id.timeLabelTV);
            trainDstNameTV = (TextView) v.findViewById(R.id.trainDstNameTV);
            trainInfoTV = (TextView) v.findViewById(R.id.trainInfoTV);
            trainCarCountTV = (TextView) v.findViewById(R.id.trainCarCountTV);

            stopCountTV = (TextView) v.findViewById(R.id.stopCountTV);

            endTimeTV = (TextView) v.findViewById(R.id.endTimeTV);
            endStnTV = (TextView) v.findViewById(R.id.endStnTV);
            endPltTV = (TextView) v.findViewById(R.id.endPltTV);

            startContIV = (ImageView) v.findViewById(R.id.startContIV);
            startStnIV = (ImageView) v.findViewById(R.id.startStnIV);
            lineIV = (ImageView) v.findViewById(R.id.lineIV);
            endStnIV = (ImageView) v.findViewById(R.id.endStnIV);
            endContIV = (ImageView) v.findViewById(R.id.endContIV);
            expandIV = (ImageView) v.findViewById(R.id.expandIV);


            includeTrainRL = (RelativeLayout) v.findViewById(R.id.includeTrainRL);
            expandedStationLL = (LinearLayout) v.findViewById(R.id.expandedStationLL);
        }
    }

    public void setOnTapListener(OnTapListener onTapListener) {
        this.onTapListener = onTapListener;
    }
}
