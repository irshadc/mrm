package com.glassboxsoftware.mrmapp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.bean.StationTimeBean;

import java.util.List;

/**
 * Created by Irshad Chohan on 10/24/2014.
 */
public class StationTimeViewAdapter extends RecyclerView.Adapter<StationTimeViewAdapter.ViewHolder> {

    private final List<StationTimeBean> values;
    private final int  lineColor;
    private final Context mContext;

   public StationTimeViewAdapter(List<StationTimeBean> mStbList, Context context, int lineColor) {
       this.values = mStbList;
       this.lineColor = lineColor;
       this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.v_route_list_item, viewGroup, false);
        final StationTimeBean stb = values.get(i);
        ViewHolder vh = new ViewHolder(v);
        vh.stnMarkerIV.setBackgroundColor(lineColor);
//        ViewGroup.LayoutParams lp = vh.timeMinTV.getLayoutParams();
//        lp.width = mContext.getResources().getDimensionPixelSize(R.dimen.result_card_right);
//        vh.timeMinTV.setLayoutParams(lp);
//        lp =vh.pltNumTV.getLayoutParams();
//        lp.width = mContext.getResources().getDimensionPixelSize(R.dimen.result_card_right);
//        vh.pltNumTV.setLayoutParams(lp);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        final StationTimeBean stb = values.get(position);
        if(stb.timeInMinute > 0) {
            vh.pltNumTV.setText("P"+stb.platformNo);
            vh.timeMinTV.setText(MRMApp.getTimeInFormat(stb.timeInMinute));
            vh.stnMarkerIV.setImageResource(R.drawable.line_stop);
            //vh.cardView.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            vh.pltNumTV.setText("");
            vh.timeMinTV.setText("");
            vh.stnMarkerIV.setImageResource(R.drawable.line_stop);
            //vh.cardView.setCardBackgroundColor(mContext.getResources().getColor(R.color.text_white_40));
        }
        vh.stnNameTV.setText(stb.stationName);

    }

    @Override
    public int getItemCount() {
        return values!=null ? values.size(): 0;
    }

    public final static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView stnNameTV;
        public final TextView pltNumTV;
        public final TextView timeMinTV;
        public final ImageView stnMarkerIV;
        //public final CardView cardView;

        public ViewHolder(View v) {
            super(v);
            stnNameTV = (TextView) v.findViewById(R.id.titleTV);
            pltNumTV = (TextView) v.findViewById(R.id.pltNumTV);
            timeMinTV = (TextView) v.findViewById(R.id.timeMinTV);
            stnMarkerIV = (ImageView) v.findViewById(R.id.stnStopIV);
            //cardView = (CardView) v.findViewById(R.id.card_view);
        }
    }
}
