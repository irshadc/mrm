package com.glassboxsoftware.mrmapp.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glassboxsoftware.mrmapp.MRMApp;
import com.glassboxsoftware.mrmapp.R;
import com.glassboxsoftware.mrmapp.activity.TSActivity;
import com.glassboxsoftware.mrmapp.activity.TrainHaltActivity;
import com.glassboxsoftware.mrmapp.bean.TrainInfoBean;
import com.glassboxsoftware.mrmapp.fragment.TrainListFragment;

import java.util.List;

/**
 * Created by Irshad Chohan on 10/24/2014.
 */
public class TrainRVAdapter extends RecyclerView.Adapter<TrainRVAdapter.ViewHolder> {

    private final List<TrainInfoBean> values;

    private final int lineId,stnId;
    private int currentIndex;
    private Context mContext;

    private TrainListFragment mParent;

   public TrainRVAdapter(List<TrainInfoBean> mTibList, int stnId, int lineId, Context mContext, TrainListFragment parent) {
        this.values = mTibList;
       this.stnId = stnId;
       this.lineId = lineId;
       this.mContext = mContext;
       this.mParent = parent;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.v_train_list_item, viewGroup, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder vh, int position) {
        final TrainInfoBean tib = values.get(position);
        int minusLineColor = 0,
                cardElevation = mContext.getResources().getDimensionPixelSize(R.dimen.headerbar_elevation),
                textColor = mContext.getResources().getColor(R.color.text_black_99),
                cardBgColor = mContext.getResources().getColor(R.color.white);

        if(position < currentIndex) {
            minusLineColor = 0x80000000;
            textColor = mContext.getResources().getColor(R.color.text_black_4c);
            cardBgColor = mContext.getResources().getColor(R.color.text_white_40);
        } else if(position == currentIndex) {
            Log.d("Train",currentIndex+" :: "+tib.trainId);
            cardElevation = mContext.getResources().getDimensionPixelSize(R.dimen.text_padding_8);
            cardBgColor = mContext.getResources().getColor(R.color.white);
        }
        final int lineColor = mContext.getResources().getColor(MRMApp.lineColorMap[lineId-1]) - minusLineColor;

        //vh.card_view.setCardElevation(cardElevation);
        vh.trainDstNameTV.setTextColor(textColor);
        vh.trainInfoTV.setTextColor(textColor);
        vh.startTimeTV.setTextColor(textColor);
        vh.trainInfoRL.setBackgroundColor(cardBgColor);

        vh.trainDstCodeTV.setText(tib.lastStnCode);
        vh.startTimeTV.setText(MRMApp.getTimeInFormatPM(tib.startStnTime));
        vh.trainDstNameTV.setText(tib.lastStnName+" - "+tib.speed);
        vh.trainInfoTV.setText("from "+tib.firstStnName);
        vh.trainCarCountTV.setText(tib.noOfCar+"");

        String detailStr = "";
        if(tib.noOfCar == -1) {
            tib.noOfCar  = 12;
        }
        detailStr = tib.noOfCar + " car"+(tib.sundayOnly ? " | Sunday Only":"") + (tib.notOnSunday ? " | Not on Sunday":"") +(tib.holidayOnly ? " | Holiday Only":"");
        if("LS".equals(tib.specialInfo)){
            detailStr+=" | Ladies Special";
        } else if (tib.specialInfo!=null && tib.specialInfo.trim().length() > 1) {
            detailStr+=" | "+tib.specialInfo.trim();
        }
        if(tib.platformNo!=null && tib.platformNo.length()>0) {
            vh.pltNumTV.setText("PF " + tib.platformNo+" ");
            vh.pltNumTV.setBackgroundResource(R.drawable.side_none);
//            if(tib.platformSide!=null && tib.platformSide.length()>0) {
//                switch(tib.platformSide.charAt(0)) {
//                    case 'L':vh.pltNumTV.setBackgroundResource(R.drawable.side_left); break;
//                    case 'R':vh.pltNumTV.setBackgroundResource(R.drawable.side_right); break;
//                    case 'B':vh.pltNumTV.setBackgroundResource(R.drawable.side_both); break;
//                }
//            }
        } else {
            vh.pltNumTV.setText("");
            vh.pltNumTV.setBackgroundResource(0);
        }
        vh.trainCarCountTV.setText(detailStr);

        vh.trainDstCodeTV.setTextColor(lineColor);
        final String extraDetails = tib.speed;

       vh.trainInfoRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mParent!=null) {
                    mParent.updateCard(tib);
                } else {
                    Intent intent = new Intent(mContext, TrainHaltActivity.class);
                    intent.putExtra("stnId",stnId);
                    intent.putExtra("trainId",tib.trainId);
                    intent.putExtra("lineId",lineId);
                    intent.putExtra("details", vh.trainCarCountTV.getText());
                    intent.putExtra("extraDetails", extraDetails);
                    mContext.startActivity(intent);
                }
            }
        });
    }




    @Override
    public int getItemCount() {
        return values!=null ? values.size(): 0;
    }

    public void updateList(List<TrainInfoBean> tibList, int currentIndex) {
        this.currentIndex = currentIndex;
        values.clear();
        if(tibList!=null) values.addAll(tibList);
        notifyDataSetChanged();
    }

    public final static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView trainDstCodeTV;
        public final TextView startTimeTV;
        public final TextView trainDstNameTV;
        public final TextView trainInfoTV;
        public final TextView trainCarCountTV;
        public final TextView pltNumTV;
        public final RelativeLayout trainInfoRL;

        public ViewHolder(View v) {
            super(v);
            trainDstCodeTV = (TextView) v.findViewById(R.id.trainDstCodeTV);
            startTimeTV = (TextView) v.findViewById(R.id.startTimeTV);
            trainDstNameTV = (TextView) v.findViewById(R.id.trainDstNameTV);
            trainInfoTV = (TextView) v.findViewById(R.id.trainInfoTV);
            trainCarCountTV = (TextView) v.findViewById(R.id.trainCarCountTV);
            pltNumTV = (TextView) v.findViewById(R.id.pltNumTV);
            trainInfoRL = (RelativeLayout) v.findViewById(R.id.trainInfoRL);
        }
    }

}
