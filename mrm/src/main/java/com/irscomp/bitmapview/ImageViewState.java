package com.irscomp.bitmapview;

import android.graphics.PointF;

import java.io.Serializable;

/**
 * Created by Irshad Chohan on 6/30/2014.
 */
public class ImageViewState implements Serializable {
    private float scale;

    private float centerX;

    private float centerY;

    private int orientation;

    public ImageViewState(float scale, PointF center, int orientation) {
        this.scale = scale;
        this.centerX = center.x;
        this.centerY = center.y;
        this.orientation = orientation;
    }

    public float getScale() {
        return scale;
    }

    public PointF getCenter() {
        return new PointF(centerX, centerY);
    }

    public int getOrientation() {
        return orientation;
    }
}
