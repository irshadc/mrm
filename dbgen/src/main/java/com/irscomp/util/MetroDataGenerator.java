package com.irscomp.util;

/**
 * Created by chohi0155 on 9/15/2015.
 */
public class MetroDataGenerator {

    public static void main(String irs[]) {
        for(int i=335;i<1392;i+=8) { // versova
            printVersova(i, 0);
            if(i>=8*60 && (i+4)<=11*60) {
                printVersova(i+4, 1);
            }
            if(i>=17*60 && (i+4)<=20*60) {
                printVersova(i+4, 1);
            }
        }
        printVersova(1392, 0);

        for(int i=331;i<1419;i+=8) { // ghatkopar
            printGhatkopar(i, 0);
            if(i>=8*60 && (i+4)<=11*60) {
                printGhatkopar(i+4, 1);
            }
            if(i>=17*60 && (i+4)<=20*60) {
                printGhatkopar(i+4,1);
            }
        }
        printGhatkopar(1419, 0);
    }
    private static int counter = 2300, upCounter=1, downCounter=1;
    public static void printVersova(int i,int notOnSunday) {
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,142,"+(i)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,141,"+(i+2)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,140,"+(i+3)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,16,"+(i+6)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,139,"+(i+8)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,138," +(i+11)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,137," +(i+12)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,136," +(i+14)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,135," +(i+16)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,134," +(i+18)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,133," +(i+19)+",2,L,142,40");
        System.out.println(counter + ",METRO,V" + upCounter + ",,4,U,S,,0,0," + notOnSunday + ",8,40," +(i+21)+",2,L,142,40");
        counter++;
        upCounter++;
    }

    public static void printGhatkopar(int i,int notOnSunday) {
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,40,"+(i)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,133,"+(i+2)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,134,"+(i+3)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,135,"+(i+5)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,136,"+(i+7)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,137,"+(i+9)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,138," +(i+10)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,139," +(i+13)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,16," +(i+15)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,140," +(i+18)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,141," +(i+19)+",1,R,40,142");
        System.out.println(counter + ",METRO,G" + downCounter + ",,4,D,S,,0,0," + notOnSunday + ",8,142," +(i+21)+",1,R,40,142");
        counter++;
        downCounter++;
    }

}
