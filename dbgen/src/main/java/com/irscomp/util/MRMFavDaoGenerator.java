package com.irscomp.util;

/**
 * Created by ChohI0155 on 5/30/2014.
 */

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;


public class MRMFavDaoGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(12, "com.glassboxsoftware.mrmapp.dao.internal");
        generateDatabase(schema);

        new DaoGenerator().generateAll(schema, "mrm/src/main/java");
    }

    private static void generateDatabase(Schema schema) {
        Entity favRoute = schema.addEntity("FavRoute");
        favRoute.setSkipTableCreation(false);

        favRoute.addIdProperty().columnType("INTEGER").autoincrement();
        favRoute.addIntProperty("srcId").columnType("INTEGER").notNull();
        favRoute.addIntProperty("dstId").columnType("INTEGER").notNull();
        favRoute.addIntProperty("routeId").columnType("INTEGER").notNull();
        favRoute.addStringProperty("jxIdLst").columnType("TEXT(120)");
        favRoute.addStringProperty("routeStr").columnType("TEXT(120)");
        favRoute.addBooleanProperty("favourite").columnType("INTEGER(1)").notNull();
        favRoute.addLongProperty("lastModified").columnType("INTEGER");
        favRoute.addLongProperty("syncedOn").columnType("INTEGER");
        favRoute.addIntProperty("usageCount").columnType("INTEGER");
        favRoute.addBooleanProperty("active").columnType("INTEGER(1)").notNull();

        Entity notifyMsg = schema.addEntity("NotifyMsg");
        notifyMsg.setSkipTableCreation(false);
        notifyMsg.addIdProperty().columnType("INTEGER").autoincrement();
        notifyMsg.addStringProperty("title").columnType("TEXT(120)");
        notifyMsg.addStringProperty("header").columnType("TEXT(10)");
        notifyMsg.addStringProperty("alert").columnType("TEXT(240)");
        notifyMsg.addStringProperty("jsonData").columnType("TEXT(4000)");
        notifyMsg.addLongProperty("generatedOn").columnType("INTEGER");
        notifyMsg.addBooleanProperty("active").columnType("INTEGER(1)").notNull();

    }
}