package com.irscomp.util;

/**
 * Created by ChohI0155 on 5/30/2014.
 */

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;


public class MRMDaoGeneratorV3 {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(26, "com.glassboxsoftware.mrmapp.dao");
        generateDatabase(schema);

        new DaoGenerator().generateAll(schema, "mrm/src/main/java");
    }

    private static void generateDatabase(Schema schema) {
        Entity line = schema.addEntity("Line");
        line.setSkipTableCreation(true);

        line.addIdProperty().columnType("INTEGER");
        line.addStringProperty("code").columnType("TEXT(8)").notNull();
        line.addStringProperty("name").columnType("TEXT(80)");
        line.addStringProperty("color").columnType("TEXT(8)");

        Entity station = schema.addEntity("Station");
        station.setSkipTableCreation(true);
        station.setTableName("STATION_MASTER");

        station.addIdProperty().columnType("INTEGER");
        station.addStringProperty("name").columnType("TEXT(120)").notNull();
        station.addStringProperty("code").columnType("TEXT(8)");
        station.addFloatProperty("lat").columnType("REAL");
        station.addFloatProperty("lng").columnType("REAL");
        station.addStringProperty("lineIds").columnType("TEXT(20)");

        Entity stationMapping = schema.addEntity("StationRouteMapping");
        stationMapping.setSkipTableCreation(true);

        stationMapping.addIdProperty().columnType("INTEGER");
        stationMapping.addIntProperty("sourceStationId").columnType("INTEGER").notNull();
        stationMapping.addIntProperty("destStationId").columnType("INTEGER").notNull();
        stationMapping.addStringProperty("route").columnType("TEXT(50)");
        stationMapping.addShortProperty("priority").columnType("INTEGER");
        stationMapping.addStringProperty("lines").columnType("TEXT(50)");
        stationMapping.addStringProperty("time").columnType("TEXT(50)");
        stationMapping.addBooleanProperty("active").columnType("INTEGER(1)");

       // generateTrainScheduleTables(schema);



        Entity lineDraw = schema.addEntity("LineDraw");
        lineDraw.setSkipTableCreation(true);

        lineDraw.addIdProperty().columnType("INTEGER");
        lineDraw.addIntProperty("lineId").columnType("INTEGER");
        lineDraw.addIntProperty("fromStnId").columnType("INTEGER");
        lineDraw.addIntProperty("toStnId").columnType("INTEGER");
        lineDraw.addStringProperty("drawScript").columnType("TEXT(450)").notNull();
        lineDraw.addStringProperty("strokeColor").columnType("TEXT(15)");
        lineDraw.addIntProperty("strokeWidth").columnType("INTEGER");

        Entity lineRouteDetails = schema.addEntity("LineRouteDetails");
        lineRouteDetails.setSkipTableCreation(true);

        lineRouteDetails.addIdProperty().columnType("INTEGER");
        lineRouteDetails.addIntProperty("lineId").columnType("INTEGER");
        lineRouteDetails.addStringProperty("routeInfo").columnType("TEXT(500)").notNull();
        lineRouteDetails.addIntProperty("orderNo").columnType("INTEGER");

        Entity spriteData = schema.addEntity("SpriteData");
        spriteData.setSkipTableCreation(true);

        spriteData.addIdProperty().columnType("INTEGER");
        spriteData.addIntProperty("stationId").columnType("INTEGER");
        spriteData.addIntProperty("lineId").columnType("INTEGER");
        spriteData.addStringProperty("spriteType").columnType("TEXT(20)");
        spriteData.addIntProperty("spriteX").columnType("INTEGER");
        spriteData.addIntProperty("spriteY").columnType("INTEGER");
        spriteData.addIntProperty("mapX").columnType("INTEGER");
        spriteData.addIntProperty("mapY").columnType("INTEGER");
        spriteData.addIntProperty("mapW").columnType("INTEGER");
        spriteData.addIntProperty("mapH").columnType("INTEGER");
        spriteData.addIntProperty("mActive").columnType("INTEGER");
        spriteData.addIntProperty("mIdle").columnType("INTEGER");

        Entity trainFare = schema.addEntity("TrainFare");
        trainFare.setSkipTableCreation(true);

        trainFare.addIdProperty().columnType("INTEGER");
        trainFare.addIntProperty("fromId").columnType("INTEGER");
        trainFare.addIntProperty("toId").columnType("INTEGER");
        trainFare.addIntProperty("lineId").columnType("INTEGER");
        trainFare.addStringProperty("via").columnType("TEXT");
        trainFare.addIntProperty("kms").columnType("INTEGER");
        trainFare.addIntProperty("fare_2D").columnType("INTEGER");
        trainFare.addIntProperty("fare_1D").columnType("INTEGER");
        trainFare.addIntProperty("fare_2_1m").columnType("INTEGER");
        trainFare.addIntProperty("fare_2_3m").columnType("INTEGER");
        trainFare.addIntProperty("fare_1_1m").columnType("INTEGER");
        trainFare.addIntProperty("fare_1_3m").columnType("INTEGER");

    }

    public static void generateTrainScheduleTables(Schema schema) {

        Entity tsdL1 = schema.addEntity("TsdL1");
        tsdL1.setSkipTableCreation(true);

        tsdL1.setSkipTableCreation(true);

        tsdL1.addIntProperty("trainId").columnType("INTEGER");
        tsdL1.addStringProperty("trainCode").columnType("TEXT(25)");
        tsdL1.addStringProperty("trainNumber").columnType("TEXT(25)");
        tsdL1.addStringProperty("emuCode").columnType("TEXT(25)");
        tsdL1.addStringProperty("noOfCar").columnType("TEXT(25)");
        tsdL1.addStringProperty("direction").columnType("TEXT(5)");
        tsdL1.addStringProperty("speed").columnType("TEXT(5)");
        tsdL1.addStringProperty("specialInfo").columnType("TEXT(25)");
        tsdL1.addBooleanProperty("sundayOnly").columnType("INTEGER(1)");
        tsdL1.addBooleanProperty("holidayOnly").columnType("INTEGER(1)");
        tsdL1.addBooleanProperty("notOnSunday").columnType("INTEGER(1)");

        tsdL1.addLongProperty("lineId").columnType("INTEGER");
        tsdL1.addLongProperty("stationId").columnType("INTEGER");

        tsdL1.addLongProperty("timeInMinutes").columnType("INTEGER");
        tsdL1.addStringProperty("platformNo").columnType("TEXT(5)");
        tsdL1.addStringProperty("platformSide").columnType("TEXT(5)");
        tsdL1.addLongProperty("startStnId").columnType("INTEGER");
        tsdL1.addLongProperty("endStnId").columnType("INTEGER");

        Entity tsdL2 = schema.addEntity("TsdL2");
        tsdL2.setSkipTableCreation(true);

        tsdL2.setSkipTableCreation(true);

        tsdL2.addIntProperty("trainId").columnType("INTEGER");
        tsdL2.addStringProperty("trainCode").columnType("TEXT(25)");
        tsdL2.addStringProperty("trainNumber").columnType("TEXT(25)");
        tsdL2.addStringProperty("emuCode").columnType("TEXT(25)");
        tsdL2.addStringProperty("noOfCar").columnType("TEXT(25)");
        tsdL2.addStringProperty("direction").columnType("TEXT(5)");
        tsdL2.addStringProperty("speed").columnType("TEXT(5)");
        tsdL2.addStringProperty("specialInfo").columnType("TEXT(25)");
        tsdL2.addBooleanProperty("sundayOnly").columnType("INTEGER(1)");
        tsdL2.addBooleanProperty("holidayOnly").columnType("INTEGER(1)");
        tsdL2.addBooleanProperty("notOnSunday").columnType("INTEGER(1)");

        tsdL2.addLongProperty("lineId").columnType("INTEGER");
        tsdL2.addLongProperty("stationId").columnType("INTEGER");

        tsdL2.addLongProperty("timeInMinutes").columnType("INTEGER");
        tsdL2.addStringProperty("platformNo").columnType("TEXT(5)");
        tsdL2.addStringProperty("platformSide").columnType("TEXT(5)");
        tsdL2.addLongProperty("startStnId").columnType("INTEGER");
        tsdL2.addLongProperty("endStnId").columnType("INTEGER");

        Entity tsdL3 = schema.addEntity("TsdL3");
        tsdL3.setSkipTableCreation(true);

        tsdL3.setSkipTableCreation(true);

        tsdL3.addIntProperty("trainId").columnType("INTEGER");
        tsdL3.addStringProperty("trainCode").columnType("TEXT(25)");
        tsdL3.addStringProperty("trainNumber").columnType("TEXT(25)");
        tsdL3.addStringProperty("emuCode").columnType("TEXT(25)");
        tsdL3.addStringProperty("noOfCar").columnType("TEXT(25)");
        tsdL3.addStringProperty("direction").columnType("TEXT(5)");
        tsdL3.addStringProperty("speed").columnType("TEXT(5)");
        tsdL3.addStringProperty("specialInfo").columnType("TEXT(25)");
        tsdL3.addBooleanProperty("sundayOnly").columnType("INTEGER(1)");
        tsdL3.addBooleanProperty("holidayOnly").columnType("INTEGER(1)");
        tsdL3.addBooleanProperty("notOnSunday").columnType("INTEGER(1)");

        tsdL3.addLongProperty("lineId").columnType("INTEGER");
        tsdL3.addLongProperty("stationId").columnType("INTEGER");

        tsdL3.addLongProperty("timeInMinutes").columnType("INTEGER");
        tsdL3.addStringProperty("platformNo").columnType("TEXT(5)");
        tsdL3.addStringProperty("platformSide").columnType("TEXT(5)");
        tsdL3.addLongProperty("startStnId").columnType("INTEGER");
        tsdL3.addLongProperty("endStnId").columnType("INTEGER");

        Entity tsdL4 = schema.addEntity("TsdL4");
        tsdL4.setSkipTableCreation(true);

        tsdL4.setSkipTableCreation(true);

        tsdL4.addIntProperty("trainId").columnType("INTEGER");
        tsdL4.addStringProperty("trainCode").columnType("TEXT(25)");
        tsdL4.addStringProperty("trainNumber").columnType("TEXT(25)");
        tsdL4.addStringProperty("emuCode").columnType("TEXT(25)");
        tsdL4.addStringProperty("noOfCar").columnType("TEXT(25)");
        tsdL4.addStringProperty("direction").columnType("TEXT(5)");
        tsdL4.addStringProperty("speed").columnType("TEXT(5)");
        tsdL4.addStringProperty("specialInfo").columnType("TEXT(25)");
        tsdL4.addBooleanProperty("sundayOnly").columnType("INTEGER(1)");
        tsdL4.addBooleanProperty("holidayOnly").columnType("INTEGER(1)");
        tsdL4.addBooleanProperty("notOnSunday").columnType("INTEGER(1)");

        tsdL4.addLongProperty("lineId").columnType("INTEGER");
        tsdL4.addLongProperty("stationId").columnType("INTEGER");

        tsdL4.addLongProperty("timeInMinutes").columnType("INTEGER");
        tsdL4.addStringProperty("platformNo").columnType("TEXT(5)");
        tsdL4.addStringProperty("platformSide").columnType("TEXT(5)");
        tsdL4.addLongProperty("startStnId").columnType("INTEGER");
        tsdL4.addLongProperty("endStnId").columnType("INTEGER");

        Entity tsdL56 = schema.addEntity("TsdL56");
        tsdL56.setSkipTableCreation(true);

        tsdL56.setSkipTableCreation(true);

        tsdL56.addIntProperty("trainId").columnType("INTEGER");
        tsdL56.addStringProperty("trainCode").columnType("TEXT(25)");
        tsdL56.addStringProperty("trainNumber").columnType("TEXT(25)");
        tsdL56.addStringProperty("emuCode").columnType("TEXT(25)");
        tsdL56.addStringProperty("noOfCar").columnType("TEXT(25)");
        tsdL56.addStringProperty("direction").columnType("TEXT(5)");
        tsdL56.addStringProperty("speed").columnType("TEXT(5)");
        tsdL56.addStringProperty("specialInfo").columnType("TEXT(25)");
        tsdL56.addBooleanProperty("sundayOnly").columnType("INTEGER(1)");
        tsdL56.addBooleanProperty("holidayOnly").columnType("INTEGER(1)");
        tsdL56.addBooleanProperty("notOnSunday").columnType("INTEGER(1)");

        tsdL56.addLongProperty("lineId").columnType("INTEGER");
        tsdL56.addLongProperty("stationId").columnType("INTEGER");

        tsdL56.addLongProperty("timeInMinutes").columnType("INTEGER");
        tsdL56.addStringProperty("platformNo").columnType("TEXT(5)");
        tsdL56.addStringProperty("platformSide").columnType("TEXT(5)");
        tsdL56.addLongProperty("startStnId").columnType("INTEGER");
        tsdL56.addLongProperty("endStnId").columnType("INTEGER");

        Entity tsdL7 = schema.addEntity("TsdL7");
        tsdL7.setSkipTableCreation(true);

        tsdL7.setSkipTableCreation(true);

        tsdL7.addIntProperty("trainId").columnType("INTEGER");
        tsdL7.addStringProperty("trainCode").columnType("TEXT(25)");
        tsdL7.addStringProperty("trainNumber").columnType("TEXT(25)");
        tsdL7.addStringProperty("emuCode").columnType("TEXT(25)");
        tsdL7.addStringProperty("noOfCar").columnType("TEXT(25)");
        tsdL7.addStringProperty("direction").columnType("TEXT(5)");
        tsdL7.addStringProperty("speed").columnType("TEXT(5)");
        tsdL7.addStringProperty("specialInfo").columnType("TEXT(25)");
        tsdL7.addBooleanProperty("sundayOnly").columnType("INTEGER(1)");
        tsdL7.addBooleanProperty("holidayOnly").columnType("INTEGER(1)");
        tsdL7.addBooleanProperty("notOnSunday").columnType("INTEGER(1)");

        tsdL7.addLongProperty("lineId").columnType("INTEGER");
        tsdL7.addLongProperty("stationId").columnType("INTEGER");

        tsdL7.addLongProperty("timeInMinutes").columnType("INTEGER");
        tsdL7.addStringProperty("platformNo").columnType("TEXT(5)");
        tsdL7.addStringProperty("platformSide").columnType("TEXT(5)");
        tsdL7.addLongProperty("startStnId").columnType("INTEGER");
        tsdL7.addLongProperty("endStnId").columnType("INTEGER");

        Entity tsdL8 = schema.addEntity("TsdL8");
        tsdL8.setSkipTableCreation(true);

        tsdL8.setSkipTableCreation(true);

        tsdL8.addIntProperty("trainId").columnType("INTEGER");
        tsdL8.addStringProperty("trainCode").columnType("TEXT(25)");
        tsdL8.addStringProperty("trainNumber").columnType("TEXT(25)");
        tsdL8.addStringProperty("emuCode").columnType("TEXT(25)");
        tsdL8.addStringProperty("noOfCar").columnType("TEXT(25)");
        tsdL8.addStringProperty("direction").columnType("TEXT(5)");
        tsdL8.addStringProperty("speed").columnType("TEXT(5)");
        tsdL8.addStringProperty("specialInfo").columnType("TEXT(25)");
        tsdL8.addBooleanProperty("sundayOnly").columnType("INTEGER(1)");
        tsdL8.addBooleanProperty("holidayOnly").columnType("INTEGER(1)");
        tsdL8.addBooleanProperty("notOnSunday").columnType("INTEGER(1)");

        tsdL8.addLongProperty("lineId").columnType("INTEGER");
        tsdL8.addLongProperty("stationId").columnType("INTEGER");

        tsdL8.addLongProperty("timeInMinutes").columnType("INTEGER");
        tsdL8.addStringProperty("platformNo").columnType("TEXT(5)");
        tsdL8.addStringProperty("platformSide").columnType("TEXT(5)");
        tsdL8.addLongProperty("startStnId").columnType("INTEGER");
        tsdL8.addLongProperty("endStnId").columnType("INTEGER");
    }

}